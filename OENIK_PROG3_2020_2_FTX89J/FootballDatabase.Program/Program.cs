﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace FootballDatabase.Program
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using ConsoleTools;
    using FootballDatabase.Data;
    using FootballDatabase.Logic;
    using FootballDatabase.Repository;

    /// <summary>
    /// The main class of the program.
    /// </summary>
    internal class Program
    {
        private static void Main()
        {
            FootballContext ctx = new FootballContext();

            StadiumRepository stadiumRepo = new StadiumRepository(ctx);
            LeagueRepository leagueRepo = new LeagueRepository(ctx);
            TeamRepository teamRepo = new TeamRepository(ctx);
            PlayerRepository playerRepo = new PlayerRepository(ctx);

            FinancialLogic financialLogic = new FinancialLogic(playerRepo, teamRepo, leagueRepo);
            InfrastructuralLogic infrastructuralLogic = new InfrastructuralLogic(stadiumRepo, leagueRepo, teamRepo);
            StatisticalLogic statisticalLogic = new StatisticalLogic(playerRepo, leagueRepo, stadiumRepo, teamRepo);
            TransferLogic transferLogic = new TransferLogic(playerRepo, teamRepo, leagueRepo);

            FixLeaguesAverageMarketValues(financialLogic, statisticalLogic);

            var lists = new ConsoleMenu()
                .Add("List leagues", () => ListLeagues(statisticalLogic))
                .Add("List teams", () => ListTeams(statisticalLogic))
                .Add("List stadiums", () => ListStadiums(statisticalLogic))
                .Add("List players", () => ListPlayers(statisticalLogic))
                .Add("Back", ConsoleMenu.Close);

            var insert = new ConsoleMenu()
                .Add("Add new League", () => AddLeague(statisticalLogic))
                .Add("Add new Stadium", () => AddStadium(infrastructuralLogic))
                .Add("Add new Team", () => AddTeam(statisticalLogic))
                .Add("Add new Player", () => AddPlayer(transferLogic, statisticalLogic, financialLogic))
                .Add("Back", ConsoleMenu.Close);

            var modifyStadium = new ConsoleMenu()
                .Add("Change Stadium's capacity", () => ModifyStadiumCapacity(infrastructuralLogic, statisticalLogic))
                .Add("Change Stadium's name", () => ModifyStadiumName(infrastructuralLogic, statisticalLogic))
                .Add("Change Stadium's phone", () => ModifyStadiumPhone(infrastructuralLogic, statisticalLogic))
                .Add("Back", ConsoleMenu.Close);

            var modifyTeam = new ConsoleMenu()
                .Add("Change Team's name", () => ModifyTeamName(statisticalLogic))
                .Add("Back", ConsoleMenu.Close);

            var modifyPlayer = new ConsoleMenu()
                .Add("Change Player's matches", () => ModifyPlayerMatches(statisticalLogic))
                .Add("Change Player's goals", () => ModifyPlayerGoals(statisticalLogic))
                .Add("Change Player's assists", () => ModifyPlayerAssists(statisticalLogic))
                .Add("Change Player's number", () => ModifyPlayerNumber(statisticalLogic))
                .Add("Change Player' market value", () => ModifyPlayerMarketValue(statisticalLogic, financialLogic))
                .Add("Change Player's team", () => ModifyPlayerTeam(statisticalLogic, transferLogic))
                .Add("Back", ConsoleMenu.Close);

            var modify = new ConsoleMenu()
                .Add("Modify a Stadium", () => modifyStadium.Show())
                .Add("Modify a Team", () => modifyTeam.Show())
                .Add("Modify a Player", () => modifyPlayer.Show())
                .Add("Back", ConsoleMenu.Close);

            var delete = new ConsoleMenu()
                .Add("Delete a League", () => DeleteLeague(statisticalLogic))
                .Add("Delete a Stadium", () => DeleteStadium(infrastructuralLogic, statisticalLogic))
                .Add("Delete a Team", () => DeleteTeam(statisticalLogic))
                .Add("Delete a Player", () => DeletePlayer(transferLogic, statisticalLogic, financialLogic))
                .Add("Back", ConsoleMenu.Close);

            var special = new ConsoleMenu()
                .Add("Transfer highest value player to one of the richest clubs in that league", () => TransferHighest(transferLogic, financialLogic, statisticalLogic))
                .Add("List teams with lower average player value than the league's average", () => ListTeamsBelowLeagueAverageValue(financialLogic))
                .Add("List stadiums with the biggest capacity from each league", () => ListHighestCapacityStadiums(infrastructuralLogic))
                .Add("Transfer highest value player to one of the richest clubs in that league (async version)", () => TransferHighestAsync(transferLogic, financialLogic, statisticalLogic))
                .Add("List teams with lower average player value than the league's average (async version)", () => ListTeamsBelowLeagueAverageValueAsync(financialLogic))
                .Add("List stadiums with the biggest capacity from each league (async version)", () => ListHighestCapacityStadiumsAsync(infrastructuralLogic))
                .Add("Back", ConsoleMenu.Close);

            var menu = new ConsoleMenu()
                .Add("List database", () => lists.Show())
                .Add("Add new record", () => insert.Show())
                .Add("Modifiy existing record", () => modify.Show())
                .Add("Delete record", () => delete.Show())
                .Add("Special functions", () => special.Show())
                .Add("Close", ConsoleMenu.Close);
            menu.Show();
        }

        private static void FixLeaguesAverageMarketValues(FinancialLogic financialLogic, StatisticalLogic statisticalLogic)
        {
            foreach (var item in statisticalLogic.GetAllLeagues())
            {
                financialLogic.FixLeagueAverageMarketValue(item.Id);
            }
        }

        private static void ListLeagues(StatisticalLogic statisticalLogic)
        {
            Console.WriteLine("-------------------------------------------------------------------------------------------------------------");
            Console.WriteLine("ID".PadRight(4) + "|" + "League's name".PadRight(30) + "|" + "Season".PadRight(9) + "|" + "Country".PadRight(30) + "|Division|Players' average value");
            Console.WriteLine("-------------------------------------------------------------------------------------------------------------");
            foreach (var item in statisticalLogic.GetAllLeagues())
            {
                Console.WriteLine("-------------------------------------------------------------------------------------------------------------");
                Console.WriteLine(item.ToString());
            }

            Console.WriteLine("Press any key to continue.");
            Console.ReadLine();
        }

        private static void ListTeams(StatisticalLogic statisticalLogic)
        {
            Console.WriteLine("---------------------------------------------------------------------------------------------------------------------------------------------------");
            Console.WriteLine("ID".PadRight(4) + "|" + "Team name".PadRight(30) + "|" + "Home shirt".PadRight(30) + "|" + "Away shirt".PadRight(30) + "|Established|Leagues won|Stadium's name");
            Console.WriteLine("---------------------------------------------------------------------------------------------------------------------------------------------------");
            foreach (var item in statisticalLogic.GetAllTeams())
            {
                Console.WriteLine("---------------------------------------------------------------------------------------------------------------------------------------------------");
                Console.WriteLine(item);
            }

            Console.WriteLine("Press any key to continue.");
            Console.ReadLine();
        }

        private static void ListStadiums(StatisticalLogic statisticalLogic)
        {
            Console.WriteLine("--------------------------------------------------------------------------------------------------------------------------------------------------------");
            Console.WriteLine("ID".PadRight(4) + "|" + "Stadium name".PadRight(30) + "|" + "Capacity" + "|" + "Built" + "|" + "Address".PadRight(80) + "|" + "Phone number");
            Console.WriteLine("--------------------------------------------------------------------------------------------------------------------------------------------------------");
            foreach (var item in statisticalLogic.GetAllStadiums())
            {
                Console.WriteLine("--------------------------------------------------------------------------------------------------------------------------------------------------------");
                Console.WriteLine(item.ToString());
            }

            Console.WriteLine("Press any key to continue.");
            Console.ReadLine();
        }

        private static void ListPlayers(StatisticalLogic statisticalLogic)
        {
            Console.WriteLine("-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            Console.WriteLine("ID".PadRight(5) + "|Number|" + "Name".PadRight(40) + "|" + "Team".PadRight(30) + "|" + "Birthday".PadRight(11) + "|" + "Nationality".PadRight(20) + "|" + "Position".PadRight(18) + "|Matches|Goals|Assists|Market value");
            Console.WriteLine("-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            foreach (var item in statisticalLogic.GetAllPlayers())
            {
                Console.WriteLine("-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
                Console.WriteLine(item.ToString());
            }

            Console.WriteLine("Press any key to continue.");
            Console.ReadLine();
        }

        private static void AddLeague(StatisticalLogic statisticalLogic)
        {
            Console.WriteLine("Inserting new league record. Please fill the details.\nPress any key to continue.");
            Console.WriteLine("Name of the leage: ");
            string name = Console.ReadLine();
            Console.WriteLine("Season: (format xxxx-yyyy, for example 2020-2021)");
            string season = Console.ReadLine();
            while (season.Length != 9)
            {
                Console.WriteLine("Wrong format for the season. Try something like this: xxxx-xxxy");
                season = Console.ReadLine();
            }

            Console.WriteLine("The home country of this competition: ");
            string country = Console.ReadLine();
            Console.WriteLine("Division: ");
            string divStr = Console.ReadLine();
            int division;
            while (!int.TryParse(divStr, out division))
            {
                Console.WriteLine("Please give a number as input.");
                divStr = Console.ReadLine();
            }

            Console.WriteLine("The information you gave for the league:\n*************************************");
            Console.WriteLine("Name: " + name);
            Console.WriteLine("Season: " + season);
            Console.WriteLine("Country: " + country);
            Console.WriteLine("Division: " + division);
            Console.WriteLine("Do you want to save the record with these details? Y/N");
            string answer = Console.ReadLine();
            while (!(answer.ToUpper() == "Y") && !(answer.ToUpper() == "N"))
            {
                Console.WriteLine("The information you gave for the league:\n*************************************");
                Console.WriteLine("Name: " + name);
                Console.WriteLine("Season: " + season);
                Console.WriteLine("Country: " + country);
                Console.WriteLine("Division: " + division);
                Console.WriteLine("Do you want to save the record with these details? Y/N");
                answer = Console.ReadLine();
            }

            if (answer.ToUpper() == "Y")
            {
                League l = new League { Country = country, Division = division, Name = name, Season = season };
                statisticalLogic.InsertLeague(l);
                Console.WriteLine("Record successfully added to the database.\nPress any key to continue.");
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine("Process aborted. Press any key to continue.");
                Console.ReadLine();
            }
        }

        private static void AddStadium(InfrastructuralLogic infrastructuralLogic)
        {
            Console.WriteLine("Inserting new stadium record. Please fill the details.\nPress any key to continue.");
            Console.WriteLine("Name of the Stadium:");
            string name = Console.ReadLine();
            Console.WriteLine("Address:");
            string address = Console.ReadLine();
            Console.WriteLine("Capacity:");
            string capStr = Console.ReadLine();
            int capacity;
            while (!int.TryParse(capStr, out capacity) || capacity < 0)
            {
                Console.WriteLine("Please give a valid number as input.");
                capStr = Console.ReadLine();
            }

            Console.WriteLine("Phone: (if you would like to skip this detail type 0)");
            string phone = Console.ReadLine();
            if (phone == "0")
            {
                phone = null;
            }

            Console.WriteLine("Year of building: (if you would like to skip this detail type 0)");
            string builtStr = Console.ReadLine();
            int built;
            while (!int.TryParse(builtStr, out built) || built < 0 || built > DateTime.Now.Year)
            {
                Console.WriteLine("Please give a valid year as input. (Type 0 to skip this detail.)");
                builtStr = Console.ReadLine();
            }

            int? nullableBuilt = built;
            if (built == 0)
            {
                nullableBuilt = null;
            }

            Console.WriteLine("The information you gave for the stadium:\n****************************************");
            Console.WriteLine("Name: " + name);
            Console.WriteLine("Address: " + address);
            Console.WriteLine("Capacity: " + capacity);
            if (phone != null)
            {
                Console.WriteLine("Phone: " + phone);
            }

            if (nullableBuilt != null)
            {
                Console.WriteLine("Built: " + nullableBuilt);
            }

            Console.WriteLine("Do you want to save the record with these details? Y/N");
            string answer = Console.ReadLine();
            while (!(answer.ToUpper() == "Y") && !(answer.ToUpper() == "N"))
            {
                Console.WriteLine("The information you gave for the stadium:\n****************************************");
                Console.WriteLine("Name: " + name);
                Console.WriteLine("Address: " + address);
                Console.WriteLine("Capacity: " + capacity);
                if (phone != null)
                {
                    Console.WriteLine("Phone: " + phone);
                }

                if (nullableBuilt != null)
                {
                    Console.WriteLine("Built: " + nullableBuilt);
                }

                Console.WriteLine("Do you want to save the record with these details? Y/N");
                answer = Console.ReadLine();
            }

            if (answer.ToUpper() == "Y")
            {
                Stadium s = new Stadium { Address = address, Capacity = capacity, Name = name, Phone = phone, YearOfBuilt = nullableBuilt };
                infrastructuralLogic.InsertStadium(s);
                Console.WriteLine("Record successfully added to the database.Press any key to continue.");
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine("Process aborted. Press any key to continue.");
                Console.ReadLine();
            }
        }

        private static void AddTeam(StatisticalLogic statisticalLogic)
        {
            Console.WriteLine("Inserting new team record. Please fill the details.\nPress any key to continue.");
            Console.WriteLine("Team name:");
            string name = Console.ReadLine();
            Console.WriteLine("Home shirt's color:");
            string homeshirt = Console.ReadLine();
            Console.WriteLine("Away shirt's color:");
            string awayshirt = Console.ReadLine();
            Console.WriteLine("Year of establishment:");
            string estStr = Console.ReadLine();
            int est;
            while (!int.TryParse(estStr, out est) || est < 0 || est > DateTime.Now.Year)
            {
                Console.WriteLine("Please give a valid year as input. (Type 0 to skip this detail.)");
                estStr = Console.ReadLine();
            }

            int? nullableEst = est;
            if (est == 0)
            {
                nullableEst = null;
            }

            Console.WriteLine("Number of leagues won by this team:");
            int won;
            string wonStr = Console.ReadLine();
            while (!int.TryParse(wonStr, out won))
            {
                Console.WriteLine("Please give a valid number as input.");
                wonStr = Console.ReadLine();
            }

            Console.WriteLine("Id of the league where this team participates. (If you would like to completely abort this insertion, press 0.)");
            int leagueId;
            string lIdStr = Console.ReadLine();
            while (!int.TryParse(lIdStr, out leagueId) || (statisticalLogic.GetOneLeague(leagueId) == null && leagueId != 0))
            {
                Console.WriteLine("Please give a valid number as input. You can check which ids are currently in use by listing the database from the menu.\nIf you would like to completely abort this insertion, press 0.");
                lIdStr = Console.ReadLine();
            }

            if (leagueId != 0)
            {
                Console.WriteLine("Id of the stadium where this team plays its home matches. (If you would like to completely abort this insertion, press 0.)");
                int stadiumId;
                string sIdStr = Console.ReadLine();
                while (!int.TryParse(sIdStr, out stadiumId) || (statisticalLogic.GetOneStadium(stadiumId) == null && stadiumId != 0))
                {
                    Console.WriteLine("Please give a valid number as input. You can check which ids are currently in use by listing the database from the menu.\nIf you would like to completely abort this insertion, press 0.");
                    sIdStr = Console.ReadLine();
                }

                if (stadiumId != 0)
                {
                    Console.WriteLine("The information you gave for the team:\n****************************************");
                    Console.WriteLine("Name: " + name);
                    Console.WriteLine("Home shirt's color: " + homeshirt);
                    Console.WriteLine("Away shirt's color: " + awayshirt);
                    if (nullableEst != null)
                    {
                        Console.WriteLine("Established: " + nullableEst);
                    }

                    Console.WriteLine("Leagues won: " + won);
                    Console.WriteLine("League id: " + leagueId);
                    Console.WriteLine("Stadium id: " + stadiumId);
                    Console.WriteLine("Do you want to save the record with these details? Y/N");
                    string answer = Console.ReadLine();
                    while (!(answer.ToUpper() == "Y") && !(answer.ToUpper() == "N"))
                    {
                        Console.WriteLine("The information you gave for the team:\n****************************************");
                        Console.WriteLine("Name: " + name);
                        Console.WriteLine("Home shirt's color: " + homeshirt);
                        Console.WriteLine("Away shirt's color: " + awayshirt);
                        if (nullableEst != null)
                        {
                            Console.WriteLine("Established: " + nullableEst);
                        }

                        Console.WriteLine("Leagues won: " + won);
                        Console.WriteLine("League id: " + leagueId);
                        Console.WriteLine("Stadium id: " + stadiumId);
                        Console.WriteLine("Do you want to save the record with these details? Y/N");
                        answer = Console.ReadLine();
                    }

                    if (answer.ToUpper() == "Y")
                    {
                        Team t = new Team { Name = name, LeaguesWon = won, YearOfEstablishment = nullableEst, HomeShirt = homeshirt, AwayShirt = awayshirt, LeagueId = leagueId, StadiumId = stadiumId };
                        statisticalLogic.InsertTeam(t);
                        Console.WriteLine("Record successfully added to the database.Press any key to continue.");
                        Console.ReadLine();
                    }
                    else
                    {
                        Console.WriteLine("Process aborted. Press any key to continue.");
                        Console.ReadLine();
                    }
                }
                else
                {
                    Console.WriteLine("Process aborted. Press any key to continue.");
                    Console.ReadLine();
                }
            }
            else
            {
                Console.WriteLine("Process aborted. Press any key to continue.");
                Console.ReadLine();
            }
        }

        private static void AddPlayer(TransferLogic transferLogic, StatisticalLogic statisticalLogic, FinancialLogic financialLogic)
        {
            Console.WriteLine("Inserting new player record. Please fill the details.\nPress any key to continue.");
            Console.WriteLine("Player's name:");
            string name = Console.ReadLine();
            int birthY = -1;
            int birthM;
            int birthD;
            DateTime birth;
            string tmp;
            do
            {
                if (birthY != -1)
                {
                    Console.WriteLine("The given date doesn't exist. Please give a valid date.\nPress any key to continue.");
                    Console.ReadLine();
                }

                Console.WriteLine("Year of birth:");
                string yStr = Console.ReadLine();
                while (!int.TryParse(yStr, out birthY) || birthY < DateTime.Now.Year - 100 || birthY > DateTime.Now.Year)
                {
                    Console.WriteLine("Please give a valid year as input.");
                    yStr = Console.ReadLine();
                }

                Console.WriteLine("Month: (with number)");
                string mStr = Console.ReadLine();
                while (!int.TryParse(mStr, out birthM) || birthM > 12 || birthM < 0)
                {
                    Console.WriteLine("Please give a valid month as input. (With number.)");
                    mStr = Console.ReadLine();
                }

                Console.WriteLine("Day:");
                string dStr = Console.ReadLine();
                while (!int.TryParse(dStr, out birthD) || birthD > 31 || birthD < 0)
                {
                    Console.WriteLine("Please give a valid day as input.");
                    dStr = Console.ReadLine();
                }

                tmp = birthY + ", " + birthM + ", " + birthD;
            }
            while (!DateTime.TryParse(tmp, out birth));

            Console.WriteLine("Postion:");
            string position = Console.ReadLine();
            Console.WriteLine("Shirt number:");
            string numberStr = Console.ReadLine();
            int number;
            while (!int.TryParse(numberStr, out number) || number < 1 || number > 99)
            {
                Console.WriteLine("Please give a valid number as input. Shirt number is 1-99.");
                numberStr = Console.ReadLine();
            }

            Console.WriteLine("Matches:");
            string matchesStr = Console.ReadLine();
            int matches;
            while (!int.TryParse(matchesStr, out matches) || matches < 0 || matches > 100)
            {
                Console.WriteLine("Please give a valid number as input.");
                matchesStr = Console.ReadLine();
            }

            Console.WriteLine("Goals:");
            string goalsStr = Console.ReadLine();
            int goals;
            while (!int.TryParse(goalsStr, out goals) || goals < 0)
            {
                Console.WriteLine("Please give a valid number as input.");
                goalsStr = Console.ReadLine();
            }

            Console.WriteLine("assists:");
            string assistsStr = Console.ReadLine();
            int assists;
            while (!int.TryParse(assistsStr, out assists) || assists < 0)
            {
                Console.WriteLine("Please give a valid number as input.");
                assistsStr = Console.ReadLine();
            }

            Console.WriteLine("Country:");
            string nationality = Console.ReadLine();
            Console.WriteLine("Market value: (type 0 if it is unknown)");
            string valueStr = Console.ReadLine();
            int marketValue;
            while (!int.TryParse(valueStr, out marketValue) || marketValue < 0 || marketValue > 1000000000)
            {
                Console.WriteLine("Please give a valid number as input.");
                valueStr = Console.ReadLine();
            }

            Console.WriteLine("Id of the player's team: (If you would like to completely abort this insertion, press 0.)");
            string teamIdStr = Console.ReadLine();
            int teamId;
            while (!int.TryParse(teamIdStr, out teamId) || (statisticalLogic.GetOneTeam(teamId) == null && teamId != 0))
            {
                Console.WriteLine("Please give a valid number as input. You can check which ids are currently in use by listing the database from the menu.\nIf you would like to completely abort this insertion, press 0.");
                teamIdStr = Console.ReadLine();
            }

            if (teamId != 0)
            {
                Console.WriteLine("The information you gave for the player:\n****************************************");
                Console.WriteLine("Name: " + name);
                Console.WriteLine("Birthday " + birth);
                Console.WriteLine("Nation: " + nationality);
                Console.WriteLine("Number: " + number);
                Console.WriteLine("Position: " + position);
                Console.WriteLine("Matches: " + matches);
                Console.WriteLine("Goals: " + goals);
                Console.WriteLine("Assists: " + assists);
                Console.WriteLine("Market value: " + marketValue);
                Console.WriteLine("Team id: " + teamId);
                Console.WriteLine("Do you want to save the record with these details? Y/N");
                string answer = Console.ReadLine();
                while (!(answer.ToUpper() == "Y") && !(answer.ToUpper() == "N"))
                {
                    Console.WriteLine("The information you gave for the player:\n****************************************");
                    Console.WriteLine("Name: " + name);
                    Console.WriteLine("Birthday " + birth);
                    Console.WriteLine("Nation: " + nationality);
                    Console.WriteLine("Number: " + number);
                    Console.WriteLine("Position: " + position);
                    Console.WriteLine("Matches: " + matches);
                    Console.WriteLine("Goals: " + goals);
                    Console.WriteLine("Assists: " + assists);
                    Console.WriteLine("Market value: " + marketValue);
                    Console.WriteLine("Team id: " + teamId);
                    Console.WriteLine("Do you want to save the record with these details? Y/N");
                    answer = Console.ReadLine();
                }

                if (answer.ToUpper() == "Y")
                {
                    Player p = new Player { Assists = assists, DateOfBirth = birth, Goals = goals, MarketValue = marketValue, Matches = matches, Name = name, Nationality = nationality, Number = number, Position = position, TeamId = teamId };
                    transferLogic.InsertPlayer(p);
                    financialLogic.FixLeagueAverageMarketValue(statisticalLogic.GetOneTeam(teamId).LeagueId);
                    Console.WriteLine("Record successfully added to the database.Press any key to continue.");
                    Console.ReadLine();
                }
                else
                {
                    Console.WriteLine("Process aborted. Press any key to continue.");
                    Console.ReadLine();
                }
            }
            else
            {
                Console.WriteLine("Process aborted. Press any key to continue.");
                Console.ReadLine();
            }
        }

        private static void ModifyStadiumCapacity(InfrastructuralLogic infrastructuralLogic, StatisticalLogic statisticalLogic)
        {
            Console.WriteLine("Which stadium would you like to modify? (Type an id.)");
            int stadiumId;
            string sIdStr = Console.ReadLine();
            while (!int.TryParse(sIdStr, out stadiumId) || (statisticalLogic.GetOneStadium(stadiumId) == null && stadiumId != 0))
            {
                Console.WriteLine("Please give a valid number as input. You can check which ids are currently in use by listing the database from the menu.\nIf you would like to completely abort this, press 0.");
                sIdStr = Console.ReadLine();
            }

            if (stadiumId != 0)
            {
                Console.WriteLine("The stadium's current capacity is " + statisticalLogic.GetOneStadium(stadiumId).Capacity + ". What is the new capacity?\n(If you would like to cancel this change, press 0.)");
                int newCap;
                string newCapStr = Console.ReadLine();
                while (!int.TryParse(newCapStr, out newCap) || newCap < 0)
                {
                    Console.WriteLine("Please give a valid number as input.\n(If you would like to cancel this change, press 0.)");
                    newCapStr = Console.ReadLine();
                }

                if (newCap != 0)
                {
                    infrastructuralLogic.ChangeStadiumCapacity(stadiumId, newCap);
                    Console.WriteLine("Capacity successfully modified. Press any key to continue.");
                    Console.ReadLine();
                }
                else
                {
                    Console.WriteLine("Change cancelled. Press any key to continue.");
                    Console.ReadLine();
                }
            }
            else
            {
                Console.WriteLine("Change cancelled. Press any key to continue.");
                Console.ReadLine();
            }
        }

        private static void ModifyStadiumName(InfrastructuralLogic infrastructuralLogic, StatisticalLogic statisticalLogic)
        {
            Console.WriteLine("Which stadium would you like to modify? (Type an id.)");
            int stadiumId;
            string sIdStr = Console.ReadLine();
            while (!int.TryParse(sIdStr, out stadiumId) || (statisticalLogic.GetOneStadium(stadiumId) == null && stadiumId != 0))
            {
                Console.WriteLine("Please give a valid number as input. You can check which ids are currently in use by listing the database from the menu.\nIf you would like to completely abort this, press 0.");
                sIdStr = Console.ReadLine();
            }

            if (stadiumId != 0)
            {
                Console.WriteLine("The stadium's current name is " + statisticalLogic.GetOneStadium(stadiumId).Name + ". What is the new name?\n(If you would like to cancel this change, press 0.)");
                string newName = Console.ReadLine();

                if (!newName.Equals("0"))
                {
                    infrastructuralLogic.ChangeStadiumName(stadiumId, newName);
                    Console.WriteLine("Name successfully modified. Press any key to continue.");
                    Console.ReadLine();
                }
                else
                {
                    Console.WriteLine("Change cancelled. Press any key to continue.");
                    Console.ReadLine();
                }
            }
            else
            {
                Console.WriteLine("Change cancelled. Press any key to continue.");
                Console.ReadLine();
            }
        }

        private static void ModifyStadiumPhone(InfrastructuralLogic infrastructuralLogic, StatisticalLogic statisticalLogic)
        {
            Console.WriteLine("Which stadium would you like to modify? (Type an id.)");
            int stadiumId;
            string sIdStr = Console.ReadLine();
            while (!int.TryParse(sIdStr, out stadiumId) || (statisticalLogic.GetOneStadium(stadiumId) == null && stadiumId != 0))
            {
                Console.WriteLine("Please give a valid number as input. You can check which ids are currently in use by listing the database from the menu.\nIf you would like to completely abort this, press 0.");
                sIdStr = Console.ReadLine();
            }

            if (stadiumId != 0)
            {
                Console.WriteLine("The stadium's current phone is " + statisticalLogic.GetOneStadium(stadiumId).Phone + ". What is the new phone?\n(If you would like to cancel this change, press 0.)");
                string newPhone = Console.ReadLine();

                if (!newPhone.Equals("0"))
                {
                    infrastructuralLogic.ChangeStadiumPhone(stadiumId, newPhone);
                    Console.WriteLine("Phone successfully modified. Press any key to continue.");
                    Console.ReadLine();
                }
                else
                {
                    Console.WriteLine("Change cancelled. Press any key to continue.");
                    Console.ReadLine();
                }
            }
            else
            {
                Console.WriteLine("Change cancelled. Press any key to continue.");
                Console.ReadLine();
            }
        }

        private static void ModifyTeamName(StatisticalLogic statisticalLogic)
        {
            Console.WriteLine("Which team would you like to modify? (Type an id.)");
            int teamId;
            string tIdStr = Console.ReadLine();
            while (!int.TryParse(tIdStr, out teamId) || (statisticalLogic.GetOneTeam(teamId) == null && teamId != 0))
            {
                Console.WriteLine("Please give a valid number as input. You can check which ids are currently in use by listing the database from the menu.\nIf you would like to completely abort this, press 0.");
                tIdStr = Console.ReadLine();
            }

            if (teamId != 0)
            {
                Console.WriteLine("The team's current name is " + statisticalLogic.GetOneTeam(teamId).Name + ". What is the new name?\n(If you would like to cancel this change, press 0.)");
                string newName = Console.ReadLine();

                if (!newName.Equals("0"))
                {
                    statisticalLogic.ChangeTeamName(teamId, newName);
                    Console.WriteLine("Name successfully modified. Press any key to continue.");
                    Console.ReadLine();
                }
                else
                {
                    Console.WriteLine("Change cancelled. Press any key to continue.");
                    Console.ReadLine();
                }
            }
            else
            {
                Console.WriteLine("Change cancelled. Press any key to continue.");
                Console.ReadLine();
            }
        }

        private static void ModifyPlayerMatches(StatisticalLogic statisticalLogic)
        {
            Console.WriteLine("Which player would you like to modify? (Type an id.)");
            int playerId;
            string pIdStr = Console.ReadLine();
            while (!int.TryParse(pIdStr, out playerId) || (statisticalLogic.GetOnePlayer(playerId) == null && playerId != 0))
            {
                Console.WriteLine("Please give a valid number as input. You can check which ids are currently in use by listing the database from the menu.\nIf you would like to completely abort this, press 0.");
                pIdStr = Console.ReadLine();
            }

            if (playerId != 0)
            {
                Console.WriteLine("The player's current match number is " + statisticalLogic.GetOnePlayer(playerId).Matches + ". What is the new match number?\n(If you would like to cancel this change, press 0.)");
                int newMatches;
                string newMatchesStr = Console.ReadLine();
                while (!int.TryParse(newMatchesStr, out newMatches) || newMatches < 0)
                {
                    Console.WriteLine("Please give a valid number as input.\n(If you would like to cancel this change, press 0.)");
                    newMatchesStr = Console.ReadLine();
                }

                if (newMatches != 0)
                {
                    statisticalLogic.ChangePlayerMatches(playerId, newMatches);
                    Console.WriteLine("Matches successfully modified. Press any key to continue.");
                    Console.ReadLine();
                }
                else
                {
                    Console.WriteLine("Change cancelled. Press any key to continue.");
                    Console.ReadLine();
                }
            }
            else
            {
                Console.WriteLine("Change cancelled. Press any key to continue.");
                Console.ReadLine();
            }
        }

        private static void ModifyPlayerGoals(StatisticalLogic statisticalLogic)
        {
            Console.WriteLine("Which player would you like to modify? (Type an id.)");
            int playerId;
            string pIdStr = Console.ReadLine();
            while (!int.TryParse(pIdStr, out playerId) || (statisticalLogic.GetOnePlayer(playerId) == null && playerId != 0))
            {
                Console.WriteLine("Please give a valid number as input. You can check which ids are currently in use by listing the database from the menu.\nIf you would like to completely abort this, press 0.");
                pIdStr = Console.ReadLine();
            }

            if (playerId != 0)
            {
                Console.WriteLine("The player's current goal number is " + statisticalLogic.GetOnePlayer(playerId).Goals + ". What is the new goal number?\n(If you would like to cancel this change, press 0.)");
                int newGoals;
                string newGoalsStr = Console.ReadLine();
                while (!int.TryParse(newGoalsStr, out newGoals) || newGoals < 0)
                {
                    Console.WriteLine("Please give a valid number as input.\n(If you would like to cancel this change, press 0.)");
                    newGoalsStr = Console.ReadLine();
                }

                if (newGoals != 0)
                {
                    statisticalLogic.ChangePlayerGoals(playerId, newGoals);
                    Console.WriteLine("Goals successfully modified. Press any key to continue.");
                    Console.ReadLine();
                }
                else
                {
                    Console.WriteLine("Change cancelled. Press any key to continue.");
                    Console.ReadLine();
                }
            }
            else
            {
                Console.WriteLine("Change cancelled. Press any key to continue.");
                Console.ReadLine();
            }
        }

        private static void ModifyPlayerAssists(StatisticalLogic statisticalLogic)
        {
            Console.WriteLine("Which player would you like to modify? (Type an id.)");
            int playerId;
            string pIdStr = Console.ReadLine();
            while (!int.TryParse(pIdStr, out playerId) || (statisticalLogic.GetOnePlayer(playerId) == null && playerId != 0))
            {
                Console.WriteLine("Please give a valid number as input. You can check which ids are currently in use by listing the database from the menu.\nIf you would like to completely abort this, press 0.");
                pIdStr = Console.ReadLine();
            }

            if (playerId != 0)
            {
                Console.WriteLine("The player's current assist number is " + statisticalLogic.GetOnePlayer(playerId).Assists + ". What is the new assist number?\n(If you would like to cancel this change, press 0.)");
                int newAssists;
                string newAssistsStr = Console.ReadLine();
                while (!int.TryParse(newAssistsStr, out newAssists) || newAssists < 0)
                {
                    Console.WriteLine("Please give a valid number as input.\n(If you would like to cancel this change, press 0.)");
                    newAssistsStr = Console.ReadLine();
                }

                if (newAssists != 0)
                {
                    statisticalLogic.ChangePlayerAssists(playerId, newAssists);
                    Console.WriteLine("Assists successfully modified. Press any key to continue.");
                    Console.ReadLine();
                }
                else
                {
                    Console.WriteLine("Change cancelled. Press any key to continue.");
                    Console.ReadLine();
                }
            }
            else
            {
                Console.WriteLine("Change cancelled. Press any key to continue.");
                Console.ReadLine();
            }
        }

        private static void ModifyPlayerNumber(StatisticalLogic statisticalLogic)
        {
            Console.WriteLine("Which player would you like to modify? (Type an id.)");
            int playerId;
            string pIdStr = Console.ReadLine();
            while (!int.TryParse(pIdStr, out playerId) || (statisticalLogic.GetOnePlayer(playerId) == null && playerId != 0))
            {
                Console.WriteLine("Please give a valid number as input. You can check which ids are currently in use by listing the database from the menu.\nIf you would like to completely abort this, press 0.");
                pIdStr = Console.ReadLine();
            }

            if (playerId != 0)
            {
                Console.WriteLine("The player's current number is " + statisticalLogic.GetOnePlayer(playerId).Number + ". What is the new number?\n(If you would like to cancel this change, press 0.)");
                int newNumber;
                string newNumberStr = Console.ReadLine();
                while (!int.TryParse(newNumberStr, out newNumber) || newNumber < 0 || newNumber > 99)
                {
                    Console.WriteLine("Please give a valid number as input. Player numbers can only be 1-99\n(If you would like to cancel this change, press 0.)");
                    newNumberStr = Console.ReadLine();
                }

                if (newNumber != 0)
                {
                    statisticalLogic.ChangePlayerNumber(playerId, newNumber);
                    Console.WriteLine("Number successfully modified. Press any key to continue.");
                    Console.ReadLine();
                }
                else
                {
                    Console.WriteLine("Change cancelled. Press any key to continue.");
                    Console.ReadLine();
                }
            }
            else
            {
                Console.WriteLine("Change cancelled. Press any key to continue.");
                Console.ReadLine();
            }
        }

        private static void ModifyPlayerMarketValue(StatisticalLogic statisticalLogic, FinancialLogic financialLogic)
        {
            Console.WriteLine("Which player would you like to modify? (Type an id.)");
            int playerId;
            string pIdStr = Console.ReadLine();
            while (!int.TryParse(pIdStr, out playerId) || (statisticalLogic.GetOnePlayer(playerId) == null && playerId != 0))
            {
                Console.WriteLine("Please give a valid number as input. You can check which ids are currently in use by listing the database from the menu.\nIf you would like to completely abort this, press 0.");
                pIdStr = Console.ReadLine();
            }

            if (playerId != 0)
            {
                Console.WriteLine("The player's current market value is " + statisticalLogic.GetOnePlayer(playerId).MarketValue + " Euro. What is the new market value?\n(If you would like to cancel this change, press 0.)");
                int newValue;
                string newValueStr = Console.ReadLine();
                while (!int.TryParse(newValueStr, out newValue) || newValue < 0)
                {
                    Console.WriteLine("Please give a valid number as input.\n(If you would like to cancel this change, press 0.)");
                    newValueStr = Console.ReadLine();
                }

                if (newValue != 0)
                {
                    financialLogic.ChangePlayerMarketValue(playerId, newValue);
                    financialLogic.FixLeagueAverageMarketValue(statisticalLogic.GetOneTeam(statisticalLogic.GetOnePlayer(playerId).TeamId).LeagueId);
                    Console.WriteLine("Market value successfully modified. Press any key to continue.");
                    Console.ReadLine();
                }
                else
                {
                    Console.WriteLine("Change cancelled. Press any key to continue.");
                    Console.ReadLine();
                }
            }
            else
            {
                Console.WriteLine("Change cancelled. Press any key to continue.");
                Console.ReadLine();
            }
        }

        private static void ModifyPlayerTeam(StatisticalLogic statisticalLogic, TransferLogic transferLogic)
        {
            Console.WriteLine("Which player would you like to modify? (Type an id.)");
            int playerId;
            string pIdStr = Console.ReadLine();
            while (!int.TryParse(pIdStr, out playerId) || (statisticalLogic.GetOnePlayer(playerId) == null && playerId != 0))
            {
                Console.WriteLine("Please give a valid number as input. You can check which ids are currently in use by listing the database from the menu.\nIf you would like to completely abort this, press 0.");
                pIdStr = Console.ReadLine();
            }

            if (playerId != 0)
            {
                Console.WriteLine("The player's current team is " + statisticalLogic.GetOneTeam(statisticalLogic.GetOnePlayer(playerId).TeamId).Name + ", with an id of: " + statisticalLogic.GetOnePlayer(playerId).TeamId + ". \nWhat is the new id?(If you would like to cancel this change, press 0.)");
                int newTeamId;
                string newTIdStr = Console.ReadLine();
                while (!int.TryParse(newTIdStr, out newTeamId) || (statisticalLogic.GetOneTeam(newTeamId) == null && newTeamId != 0))
                {
                    Console.WriteLine("Please give a valid number as input. You can check which ids are currently in use by listing the database from the menu\n(If you would like to cancel this change, press 0.)");
                    newTIdStr = Console.ReadLine();
                }

                if (newTeamId != 0)
                {
                    transferLogic.ChangePlayerTeamId(playerId, newTeamId);
                    Console.WriteLine("Team successfully modified. Press any key to continue.");
                    Console.ReadLine();
                }
                else
                {
                    Console.WriteLine("Change cancelled. Press any key to continue.");
                    Console.ReadLine();
                }
            }
            else
            {
                Console.WriteLine("Change cancelled. Press any key to continue.");
                Console.ReadLine();
            }
        }

        private static void DeleteLeague(StatisticalLogic statisticalLogic)
        {
            Console.WriteLine("Which league would you like to delete? (Type an id.)");
            int leagueId;
            string leagueIdStr = Console.ReadLine();
            while (!int.TryParse(leagueIdStr, out leagueId) || (statisticalLogic.GetOneLeague(leagueId) == null && leagueId != 0))
            {
                Console.WriteLine("Please give a valid number as input. You can check which ids are currently in use by listing the database from the menu.\nIf you would like to completely abort this, press 0.");
                leagueIdStr = Console.ReadLine();
            }

            if (leagueId != 0)
            {
                bool inUse = false;
                int inUseId = 0;
                foreach (var item in statisticalLogic.GetAllTeams())
                {
                    if (item.LeagueId == leagueId)
                    {
                        inUse = true;
                        inUseId = item.Id;
                        break;
                    }
                }

                if (!inUse)
                {
                    statisticalLogic.RemoveLeague(statisticalLogic.GetOneLeague(leagueId));
                    Console.WriteLine("League successfully deleted. Press any key to continue.");
                    Console.ReadLine();
                }
                else
                {
                    Console.WriteLine(statisticalLogic.GetOneTeam(inUseId).Name + " is still exists in that league with an id of " + inUseId + ".\nYou can't delete a league before deleting every team it contains.\nPress any key to continue");
                    Console.ReadLine();
                }
            }
            else
            {
                Console.WriteLine("Deleting cancelled. Press any key to continue.");
                Console.ReadLine();
            }
        }

        private static void DeleteStadium(InfrastructuralLogic infrastructuralLogic, StatisticalLogic statisticalLogic)
        {
            Console.WriteLine("Which stadium would you like to delete? (Type an id.)");
            int stadiumId;
            string stadiumIdStr = Console.ReadLine();
            while (!int.TryParse(stadiumIdStr, out stadiumId) || (statisticalLogic.GetOneStadium(stadiumId) == null && stadiumId != 0))
            {
                Console.WriteLine("Please give a valid number as input. You can check which ids are currently in use by listing the database from the menu.\nIf you would like to completely abort this, press 0.");
                stadiumIdStr = Console.ReadLine();
            }

            if (stadiumId != 0)
            {
                bool inUse = false;
                int inUseId = 0;
                foreach (var item in statisticalLogic.GetAllTeams())
                {
                    if (item.StadiumId == stadiumId)
                    {
                        inUse = true;
                        inUseId = item.Id;
                        break;
                    }
                }

                if (!inUse)
                {
                    infrastructuralLogic.RemoveStadium(statisticalLogic.GetOneStadium(stadiumId));
                    Console.WriteLine("Stadium successfully deleted. Press any key to continue.");
                    Console.ReadLine();
                }
                else
                {
                    Console.WriteLine(statisticalLogic.GetOneTeam(inUseId).Name + " is still using that stadium with an id of " + inUseId + ".\nYou can't delete a stadium before deleting every team that plays there.\nPress any key to continue");
                    Console.ReadLine();
                }
            }
            else
            {
                Console.WriteLine("Deleting cancelled. Press any key to continue.");
                Console.ReadLine();
            }
        }

        private static void DeleteTeam(StatisticalLogic statisticalLogic)
        {
            Console.WriteLine("Which team would you like to delete? (Type an id.)");
            int teamId;
            string teamIdStr = Console.ReadLine();
            while (!int.TryParse(teamIdStr, out teamId) || (statisticalLogic.GetOneTeam(teamId) == null && teamId != 0))
            {
                Console.WriteLine("Please give a valid number as input. You can check which ids are currently in use by listing the database from the menu.\nIf you would like to completely abort this, press 0.");
                teamIdStr = Console.ReadLine();
            }

            if (teamId != 0)
            {
                bool inUse = false;
                int inUseId = 0;
                foreach (var item in statisticalLogic.GetAllPlayers())
                {
                    if (item.TeamId == teamId)
                    {
                        inUse = true;
                        inUseId = item.Id;
                        break;
                    }
                }

                if (!inUse)
                {
                    statisticalLogic.RemoveTeam(statisticalLogic.GetOneTeam(teamId));
                    Console.WriteLine("Team successfully deleted. Press any key to continue.");
                    Console.ReadLine();
                }
                else
                {
                    Console.WriteLine(statisticalLogic.GetOnePlayer(inUseId).Name + " is still part of the team with an id of " + inUseId + ".\nYou can't delete a team before deleting every player from there.\nPress any key to continue");
                    Console.ReadLine();
                }
            }
            else
            {
                Console.WriteLine("Deleting cancelled. Press any key to continue.");
                Console.ReadLine();
            }
        }

        private static void DeletePlayer(TransferLogic transferLogic, StatisticalLogic statisticalLogic, FinancialLogic financialLogic)
        {
            Console.WriteLine("Which player would you like to delete? (Type an id.)");
            int playerId;
            string playerIdStr = Console.ReadLine();
            while (!int.TryParse(playerIdStr, out playerId) || (statisticalLogic.GetOnePlayer(playerId) == null && playerId != 0))
            {
                Console.WriteLine("Please give a valid number as input. You can check which ids are currently in use by listing the database from the menu.\nIf you would like to completely abort this, press 0.");
                playerIdStr = Console.ReadLine();
            }

            if (playerId != 0)
            {
                transferLogic.RemovePlayer(statisticalLogic.GetOnePlayer(playerId));
                financialLogic.FixLeagueAverageMarketValue(statisticalLogic.GetOneTeam(statisticalLogic.GetOnePlayer(playerId).TeamId).LeagueId);
                Console.WriteLine("Player successfully deleted. Press any key to continue.");
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine("Deleting cancelled. Press any key to continue.");
                Console.ReadLine();
            }
        }

        private static void TransferHighest(TransferLogic transferLogic, FinancialLogic financialLogic, StatisticalLogic statisticalLogic)
        {
            var transfer = transferLogic.TransferHighestValue();
            foreach (var item in transfer)
            {
                Console.WriteLine(item);
            }

            FixLeaguesAverageMarketValues(financialLogic, statisticalLogic);
            Console.WriteLine("Press any key to continue.");
            Console.ReadLine();
        }

        private static void ListTeamsBelowLeagueAverageValue(FinancialLogic financialLogic)
        {
            var below = financialLogic.BelowLeagueAverage();
            foreach (var item in below)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("Press any key to continue.");
            Console.ReadLine();
        }

        private static void ListHighestCapacityStadiums(InfrastructuralLogic infrastructuralLogic)
        {
            var maxCap = infrastructuralLogic.MaximumCapacity();
            foreach (var item in maxCap)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("Press any key to continue.");
            Console.ReadLine();
        }

        private static void TransferHighestAsync(TransferLogic transferLogic, FinancialLogic financialLogic, StatisticalLogic statisticalLogic)
        {
            var transfer = transferLogic.TransferHighestValueAsync().Result;
            foreach (var item in transfer)
            {
                Console.WriteLine(item);
            }

            FixLeaguesAverageMarketValues(financialLogic, statisticalLogic);
            Console.WriteLine("Press any key to continue.");
            Console.ReadLine();
        }

        private static void ListTeamsBelowLeagueAverageValueAsync(FinancialLogic financialLogic)
        {
            var below = financialLogic.BelowLeagueAverageAsync().Result;
            foreach (var item in below)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("Press any key to continue.");
            Console.ReadLine();
        }

        private static void ListHighestCapacityStadiumsAsync(InfrastructuralLogic infrastructuralLogic)
        {
            var maxCap = infrastructuralLogic.MaximumCapacityAsync().Result;
            foreach (var item in maxCap)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("Press any key to continue.");
            Console.ReadLine();
        }
    }
}
