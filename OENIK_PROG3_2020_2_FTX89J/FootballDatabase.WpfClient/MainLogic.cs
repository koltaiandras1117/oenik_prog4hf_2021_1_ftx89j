﻿// <copyright file="MainLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Text.Json;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// Class for UI logic.
    /// </summary>
    public class MainLogic
    {
        private readonly string url = "http://localhost:41598/PlayersApi/";
        private HttpClient client = new HttpClient();
        private JsonSerializerOptions jsonOptions = new JsonSerializerOptions(JsonSerializerDefaults.Web);

        /// <summary>
        /// Send message method.
        /// </summary>
        /// <param name="success">Operation was successful or not.</param>
        public static void SendMessage(bool success)
        {
            string msg = success ? "Operation completed successfully." : "Operation failed.";
            Messenger.Default.Send(msg, "PlayerResult");
        }

        /// <summary>
        /// Method that calls an api method for getting the list of the players.
        /// </summary>
        /// <returns>The list of all players.</returns>
        public List<PlayerVM> ApiGetPlayers()
        {
            string json = this.client.GetStringAsync(this.url + "all").Result;
            var list = JsonSerializer.Deserialize<List<PlayerVM>>(json, this.jsonOptions);
            return list;
        }

        /// <summary>
        /// Method that calls an api method for deleting a player.
        /// </summary>
        /// <param name="player">The player that will be deleted.</param>
        public void ApiDeletePlayer(PlayerVM player)
        {
            bool success = false;
            if (player != null)
            {
                string json = this.client.GetStringAsync(this.url + "del/" + player.Id.ToString()).Result;
                JsonDocument doc = JsonDocument.Parse(json);
                success = doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
            }

            SendMessage(success);
        }

        /// <summary>
        /// Method that calls an api method for editing a player.
        /// </summary>
        /// <param name="player">The player that will be edited.</param>
        /// <param name="editorFunc">The function the editing will use.</param>
        public void EditPlayer(PlayerVM player, Func<PlayerVM, bool> editorFunc)
        {
            PlayerVM clone = new PlayerVM();
            if (player != null)
            {
                clone.CopyFrom(player);
            }

            bool? success = editorFunc?.Invoke(clone);
            if (success == true)
            {
                if (player != null)
                {
                    success = this.ApiEditPlayer(clone, true);
                }
                else
                {
                    success = this.ApiEditPlayer(clone, false);
                }
            }

            SendMessage(success == true);
        }

        private bool ApiEditPlayer(PlayerVM player, bool isEditing)
        {
            if (player == null)
            {
                return false;
            }

            string myUrl = this.url + (isEditing ? "mod" : "add");
            Dictionary<string, string> postData = new Dictionary<string, string>();
            postData.Add("id", player.Id.ToString());
            postData.Add("assists", player.Assists.ToString());
            postData.Add("birthday", player.DateOfBirth.ToString());
            postData.Add("goals", player.Goals.ToString());
            postData.Add("marketValue", player.MarketValue.ToString());
            postData.Add("matches", player.Matches.ToString());
            postData.Add("name", player.Name);
            postData.Add("nationality", player.Nationality);
            postData.Add("number", player.Number.ToString());
            postData.Add("position", player.Position);
            postData.Add("team", player.Team);

            string json = this.client.PostAsync(myUrl, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
            JsonDocument doc = JsonDocument.Parse(json);
            return doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
        }
    }
}
