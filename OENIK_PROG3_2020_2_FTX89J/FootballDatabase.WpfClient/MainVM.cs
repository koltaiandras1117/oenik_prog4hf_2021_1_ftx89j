﻿// <copyright file="MainVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// ViewModel class.
    /// </summary>
    public class MainVM : ViewModelBase
    {
        private MainLogic logic;
        private PlayerVM selectedPlayer;
        private ObservableCollection<PlayerVM> allPlayers;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        public MainVM()
        {
            this.logic = new MainLogic();

            this.LoadCmd = new RelayCommand(() => this.AllPlayers = new ObservableCollection<PlayerVM>(this.logic.ApiGetPlayers()));
            this.DelCmd = new RelayCommand(() => this.logic.ApiDeletePlayer(this.selectedPlayer));
            this.AddCmd = new RelayCommand(() => this.logic.EditPlayer(null, this.EditorFunc));
            this.ModCmd = new RelayCommand(() => this.logic.EditPlayer(this.selectedPlayer, this.EditorFunc));
        }

        /// <summary>
        /// Gets or sets the selected player.
        /// </summary>
        public PlayerVM SelectedPlayer
        {
            get
            {
                return this.selectedPlayer;
            }

            set
            {
                this.Set(ref this.selectedPlayer, value);
            }
        }

        /// <summary>
        /// Gets or sets the observable collection of all players.
        /// </summary>
        public ObservableCollection<PlayerVM> AllPlayers
        {
            get
            {
                return this.allPlayers;
            }

            set
            {
                this.Set(ref this.allPlayers, value);
            }
        }

        /// <summary>
        /// Gets or sets the editor function.
        /// </summary>
        public Func<PlayerVM, bool> EditorFunc { get; set; }

        /// <summary>
        /// Gets the add command.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets the delete command.
        /// </summary>
        public ICommand DelCmd { get; private set; }

        /// <summary>
        /// Gets the modify command.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets the load command.
        /// </summary>
        public ICommand LoadCmd { get; private set; }
    }
}
