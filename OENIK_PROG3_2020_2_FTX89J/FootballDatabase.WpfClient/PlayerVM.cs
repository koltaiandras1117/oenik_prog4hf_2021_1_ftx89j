﻿// <copyright file="PlayerVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Player ViewModel.
    /// </summary>
    public class PlayerVM : ObservableObject
    {
        private int id;
        private string name;
        private DateTime dateOfBirth;
        private string position;
        private int number;
        private int goals;
        private int assists;
        private int matches;
        private string nationality;
        private int marketValue;
        private string team;

        /// <summary>
        /// Gets or sets the id of a player.
        /// </summary>
        public int Id
        {
            get
            {
                return this.id;
            }

            set
            {
                this.Set(ref this.id, value);
            }
        }

        /// <summary>
        /// Gets or sets the player's name.
        /// </summary>
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                this.Set(ref this.name, value);
            }
        }

        /// <summary>
        /// Gets or sets the player's birthday.
        /// </summary>
        public DateTime DateOfBirth
        {
            get
            {
                return this.dateOfBirth;
            }

            set
            {
                this.Set(ref this.dateOfBirth, value);
            }
        }

        /// <summary>
        /// Gets or sets the player's position.
        /// </summary>
        public string Position
        {
            get
            {
                return this.position;
            }

            set
            {
                this.Set(ref this.position, value);
            }
        }

        /// <summary>
        /// Gets or sets the number on the player's shirt.
        /// </summary>
        public int Number
        {
            get
            {
                return this.number;
            }

            set
            {
                this.Set(ref this.number, value);
            }
        }

        /// <summary>
        /// Gets or sets how many goals the player has scored in the current season.
        /// </summary>
        public int Goals
        {
            get
            {
                return this.goals;
            }

            set
            {
                this.Set(ref this.goals, value);
            }
        }

        /// <summary>
        /// Gets or sets how many assists the player has given in the current season.
        /// </summary>
        public int Assists
        {
            get
            {
                return this.assists;
            }

            set
            {
                this.Set(ref this.assists, value);
            }
        }

        /// <summary>
        /// Gets or sets how many matches the player has played in the current season.
        /// </summary>
        public int Matches
        {
            get
            {
                return this.matches;
            }

            set
            {
                this.Set(ref this.matches, value);
            }
        }

        /// <summary>
        /// Gets or sets the player's nationality.
        /// </summary>
        public string Nationality
        {
            get
            {
                return this.nationality;
            }

            set
            {
                this.Set(ref this.nationality, value);
            }
        }

        /// <summary>
        /// Gets or sets the player's current market value in euros.
        /// </summary>
        public int MarketValue
        {
            get
            {
                return this.marketValue;
            }

            set
            {
                this.Set(ref this.marketValue, value);
            }
        }

        /// <summary>
        /// Gets or sets which team the player is playing in.
        /// </summary>
        public string Team
        {
            get
            {
                return this.team;
            }

            set
            {
                this.Set(ref this.team, value);
            }
        }

        /// <summary>
        /// Creates a copy from another PlayerVM object.
        /// </summary>
        /// <param name="other">The PlayerVM that will be copied.</param>
        public void CopyFrom(PlayerVM other)
        {
            if (other == null)
            {
                return;
            }

            this.Assists = other.Assists;
            this.DateOfBirth = other.DateOfBirth;
            this.Goals = other.Goals;
            this.Id = other.Id;
            this.MarketValue = other.MarketValue;
            this.Matches = other.Matches;
            this.Name = other.Name;
            this.Nationality = other.Nationality;
            this.Number = other.Number;
            this.Position = other.Position;
            this.Team = other.Team;
        }
    }
}
