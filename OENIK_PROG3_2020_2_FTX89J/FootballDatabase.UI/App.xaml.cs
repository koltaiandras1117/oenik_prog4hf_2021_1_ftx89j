﻿// <copyright file="App.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.UI
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows;
    using CommonServiceLocator;
    using FootballDatabase.Data;
    using FootballDatabase.Logic;
    using FootballDatabase.Repository;
    using FootballDatabase.UI.BL;
    using FootballDatabase.UI.UI;
    using GalaSoft.MvvmLight.Ioc;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// Interaction logic for App.xaml.
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="App"/> class.
        /// </summary>
        public App()
        {
            ServiceLocator.SetLocatorProvider(() => MyIoC.Instance);

            MyIoC.Instance.Register<IEditorService, EditorServiceViaWindow>();
            MyIoC.Instance.Register<IMessenger>(() => Messenger.Default);
            MyIoC.Instance.Register<ILeagueWPFLogic, LeagueWPFLogic>();

            MyIoC.Instance.Register<Microsoft.EntityFrameworkCore.DbContext, FootballContext>();

            MyIoC.Instance.Register<IPlayerRepository, PlayerRepository>();
            MyIoC.Instance.Register<ILeagueRepository, LeagueRepository>();
            MyIoC.Instance.Register<IStadiumRepository, StadiumRepository>();
            MyIoC.Instance.Register<ITeamRepository, TeamRepository>();

            MyIoC.Instance.Register<IStatisticalLogic, StatisticalLogic>();
            MyIoC.Instance.Register<IFinancialLogic, FinancialLogic>();
        }
    }
}
