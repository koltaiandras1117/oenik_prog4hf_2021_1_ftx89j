﻿// <copyright file="LeagueWPF.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.UI.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Class that represents a League object.
    /// </summary>
    public class LeagueWPF : ObservableObject
    {
        private string name;
        private string season;
        private string country;
        private int averageValue;
        private int division;
        private int id;

        /// <summary>
        /// Gets or sets the name of the League.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.Set(ref this.name, value); }
        }

        /// <summary>
        /// Gets or sets the current season of the League.
        /// </summary>
        public string Season
        {
            get { return this.season; }
            set { this.Set(ref this.season, value); }
        }

        /// <summary>
        /// Gets or sets the country of the League.
        /// </summary>
        public string Country
        {
            get { return this.country; }
            set { this.Set(ref this.country, value); }
        }

        /// <summary>
        /// Gets or sets the average value of the League.
        /// </summary>
        public int AverageValue
        {
            get { return this.averageValue; }
            set { this.Set(ref this.averageValue, value); }
        }

        /// <summary>
        /// Gets or sets the division of the League.
        /// </summary>
        public int Division
        {
            get { return this.division; }
            set { this.Set(ref this.division, value); }
        }

        /// <summary>
        /// Gets or sets the id of the League.
        /// </summary>
        public int Id
        {
            get { return this.id; }
            set { this.Set(ref this.id, value); }
        }

        /// <summary>
        /// Method for copying a League object.
        /// </summary>
        /// <param name="other">The other object, that provides the values for copying.</param>
        public void CopyFrom(LeagueWPF other)
        {
            this.GetType().GetProperties().ToList().ForEach(property => property.SetValue(this, property.GetValue(other)));
        }
    }
}
