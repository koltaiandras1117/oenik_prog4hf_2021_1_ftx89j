﻿// <copyright file="MyIoC.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight.Ioc;

    /// <summary>
    /// Same class as SimpleIoC.
    /// </summary>
    public class MyIoC : SimpleIoc, IServiceLocator
    {
        /// <summary>
        /// Gets a MyIoC instance.
        /// </summary>
        public static MyIoC Instance { get; private set; } = new MyIoC();
    }
}
