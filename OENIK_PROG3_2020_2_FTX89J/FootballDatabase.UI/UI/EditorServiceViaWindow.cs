﻿// <copyright file="EditorServiceViaWindow.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.UI.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FootballDatabase.UI.BL;
    using FootballDatabase.UI.Data;

    /// <summary>
    /// Class for editing the League object.
    /// </summary>
    public class EditorServiceViaWindow : IEditorService
    {
        /// <summary>
        /// Method for editing a League object.
        /// </summary>
        /// <param name="t">The league that will be edited.</param>
        /// <returns>Returns true if the edit was successful, otherwise false.</returns>
        public bool EditLeague(LeagueWPF t)
        {
            EditorWindow editorWindow = new EditorWindow(t);
            return editorWindow.ShowDialog() ?? false;
        }
    }
}
