﻿// <copyright file="ILeagueWPFLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.UI.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FootballDatabase.UI.Data;

    /// <summary>
    /// Interface that specifies the CRUD functions for the League object.
    /// </summary>
    public interface ILeagueWPFLogic
    {
        /// <summary>
        /// Method for adding a new League object to the database.
        /// </summary>
        /// <param name="list">List of the already existing League objects in the database.</param>
        void AddLeague(IList<LeagueWPF> list);

        /// <summary>
        /// Modifies an already existing League record in the database.
        /// </summary>
        /// <param name="leagueToModifiy">The League object that will be modified.</param>
        void ModifyLeague(LeagueWPF leagueToModifiy);

        /// <summary>
        /// Deletes the given League from the database.
        /// </summary>
        /// <param name="list">List of the League objects in the database.</param>
        /// <param name="league">The League object that will be deleted.</param>
        void DeleteLeague(IList<LeagueWPF> list, LeagueWPF league);

        /// <summary>
        /// Gets all the Leagues from the database.
        /// </summary>
        /// <returns>A List of Leagues that exist in the database.</returns>
        IList<LeagueWPF> GetAllLeagues();
    }
}
