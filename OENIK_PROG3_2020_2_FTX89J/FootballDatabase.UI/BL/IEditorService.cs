﻿// <copyright file="IEditorService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.UI.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FootballDatabase.UI.Data;

    /// <summary>
    /// Interface for editing an already existing object.
    /// </summary>
    public interface IEditorService
    {
        /// <summary>
        /// Method that edits a League object.
        /// </summary>
        /// <param name="t">The League object that will be edited.</param>
        /// <returns>Returns true if the editing was successful, otherwise false.</returns>
        bool EditLeague(LeagueWPF t);
    }
}
