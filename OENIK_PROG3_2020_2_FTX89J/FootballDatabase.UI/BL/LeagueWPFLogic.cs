﻿// <copyright file="LeagueWPFLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.UI.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FootballDatabase.Data;
    using FootballDatabase.Logic;
    using FootballDatabase.UI.Data;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// Class that implements the CRUD functions of the League object.
    /// </summary>
    public class LeagueWPFLogic : ILeagueWPFLogic
    {
        private IEditorService editorService;

        private IMessenger messengerService;

        private IStatisticalLogic statisticalLogic;

        private IFinancialLogic financialLogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="LeagueWPFLogic"/> class.
        /// </summary>
        /// <param name="editorService">Parameter which is compatible with the IEditorService interface.</param>
        /// <param name="messengerService">Parameter which is compatible with the IMessnger interface.</param>
        /// <param name="statisticalLogic">Parameter which is compatible with the IStatisticalLogic interface.</param>
        /// <param name="financialLogic">Parameter which is compatible with the IFinancialLogic interface.</param>
        public LeagueWPFLogic(IEditorService editorService, IMessenger messengerService, IStatisticalLogic statisticalLogic, IFinancialLogic financialLogic)
        {
            this.editorService = editorService;
            this.messengerService = messengerService;
            this.statisticalLogic = statisticalLogic;
            this.financialLogic = financialLogic;
        }

        /// <summary>
        /// Method for adding a new League object to the database.
        /// </summary>
        /// <param name="list">List of the already existing League objects in the database.</param>
        public void AddLeague(IList<LeagueWPF> list)
        {
            LeagueWPF newLeague = new LeagueWPF();
            if (this.editorService.EditLeague(newLeague))
            {
                League l = new League
                {
                    Name = newLeague.Name,
                    Season = newLeague.Season,
                    Country = newLeague.Country,
                    AverageValue = newLeague.AverageValue,
                    Division = newLeague.Division,
                };
                this.statisticalLogic.InsertLeague(l);
                newLeague.Id = l.Id;
                list.Add(newLeague);
                this.messengerService.Send("New League Added", "LogicResult");
            }
            else
            {
                this.messengerService.Send("Insertion has been canceled.", "LogicResult");
            }
        }

        /// <summary>
        /// Deletes the given League from the database.
        /// </summary>
        /// <param name="list">List of the League objects in the database.</param>
        /// <param name="league">The League object that will be deleted.</param>
        public void DeleteLeague(IList<LeagueWPF> list, LeagueWPF league)
        {
            int id = league.Id;
            if (league != null && list.Remove(league))
            {
                this.statisticalLogic.RemoveLeague(this.statisticalLogic.GetOneLeague(id));
                this.messengerService.Send("Deleted Successfully", "LogicResult");
            }
            else
            {
                this.messengerService.Send("Deletion failed");
            }
        }

        /// <summary>
        /// Modifies an already existing League record in the database.
        /// </summary>
        /// <param name="leagueToModifiy">The League object that will be modified.</param>
        public void ModifyLeague(LeagueWPF leagueToModifiy)
        {
            if (leagueToModifiy == null)
            {
                this.messengerService.Send("Edit Failed", "LogicResult");
                return;
            }

            LeagueWPF clone = new LeagueWPF();
            clone.CopyFrom(leagueToModifiy);
            if (this.editorService.EditLeague(clone))
            {
                leagueToModifiy.CopyFrom(clone);
                this.statisticalLogic.ChangeLeagueName(leagueToModifiy.Id, leagueToModifiy.Name);
                this.statisticalLogic.ChangeLeagueSeason(leagueToModifiy.Id, leagueToModifiy.Season);
                this.statisticalLogic.ChangeLeagueCountry(leagueToModifiy.Id, leagueToModifiy.Country);
                this.statisticalLogic.ChangeLeagueDivision(leagueToModifiy.Id, leagueToModifiy.Division);
                this.financialLogic.ChangeLeagueAverageValue(leagueToModifiy.Id, leagueToModifiy.AverageValue);
                this.messengerService.Send("Record has been modified.", "LogicResult");
            }
            else
            {
                this.messengerService.Send("Modification canceled.", "LogicResult");
            }
        }

        /// <summary>
        /// Gets all the Leagues from the database.
        /// </summary>
        /// <returns>A List of Leagues that exist in the database.</returns>
        public IList<LeagueWPF> GetAllLeagues()
        {
            List<LeagueWPF> list = new List<LeagueWPF>();
            foreach (var item in this.statisticalLogic.GetAllLeagues())
            {
                LeagueWPF l = new LeagueWPF();
                l.Name = item.Name;
                l.Season = item.Season;
                l.Country = item.Country;
                l.AverageValue = item.AverageValue;
                l.Division = item.Division;
                l.Id = item.Id;
                list.Add(l);
            }

            return list;
        }
    }
}
