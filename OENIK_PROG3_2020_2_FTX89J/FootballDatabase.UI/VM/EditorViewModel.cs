﻿// <copyright file="EditorViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.UI.VM
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FootballDatabase.UI.Data;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// View model for editing a League object.
    /// </summary>
    public class EditorViewModel : ViewModelBase
    {
        private LeagueWPF league;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorViewModel"/> class.
        /// </summary>
        public EditorViewModel()
        {
            this.league = new LeagueWPF();
            if (this.IsInDesignMode)
            {
                this.league.Name = "Test League";
                this.league.Season = "2020-2021";
                this.league.Country = "Testland";
                this.league.Division = 1;
                this.league.AverageValue = 0;
            }
        }

        /// <summary>
        /// Gets or sets the League object.
        /// </summary>
        public LeagueWPF League
        {
            get { return this.league; }
            set { this.Set(ref this.league, value); }
        }
    }
}
