﻿// <copyright file="MainViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.UI.VM
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using CommonServiceLocator;
    using FootballDatabase.UI.BL;
    using FootballDatabase.UI.Data;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// View model for the main window.
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private ILeagueWPFLogic leagueLogic;

        private LeagueWPF leagueSelected;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        /// <param name="leagueLogic">Interface for the League's logic functions.</param>
        public MainViewModel(ILeagueWPFLogic leagueLogic)
        {
            this.leagueLogic = leagueLogic;
            this.Leagues = new ObservableCollection<LeagueWPF>();
            var allLeagues = this.leagueLogic.GetAllLeagues();
            foreach (var item in allLeagues)
            {
                this.Leagues.Add(item);
            }

            if (this.IsInDesignMode)
            {
                LeagueWPF l1 = new LeagueWPF() { Name = "Test League 1", Season = "2020-2021", Country = "Testland", Division = 1, AverageValue = 0 };
                LeagueWPF l2 = new LeagueWPF() { Name = "Test League 2", Season = "2019-2020", Country = "Country", Division = 3, AverageValue = 1000000 };
                this.Leagues.Add(l1);
                this.Leagues.Add(l2);
            }

            this.AddCmd = new RelayCommand(() => this.leagueLogic.AddLeague(this.Leagues));
            this.ModCmd = new RelayCommand(() => this.leagueLogic.ModifyLeague(this.LeagueSelected));
            this.DelCmd = new RelayCommand(() => this.leagueLogic.DeleteLeague(this.Leagues, this.LeagueSelected));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        public MainViewModel()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<ILeagueWPFLogic>())
        {
        }

        /// <summary>
        /// Gets a list of all Leagues.
        /// </summary>
        public ObservableCollection<LeagueWPF> Leagues { get; private set; }

        /// <summary>
        /// Gets or sets the selected league.
        /// </summary>
        public LeagueWPF LeagueSelected
        {
            get { return this.leagueSelected; }
            set { this.Set(ref this.leagueSelected, value); }
        }

        /// <summary>
        /// Gets a command to add a new object.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets a command to modifiy an object.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets a command to delete an object.
        /// </summary>
        public ICommand DelCmd { get; private set; }
    }
}
