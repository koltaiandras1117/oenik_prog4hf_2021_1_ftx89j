﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "Not neccessary.", Scope = "member", Target = "~M:FootballDatabase.UI.BL.LeagueWPFLogic.AddLeague(System.Collections.Generic.IList{FootballDatabase.UI.Data.LeagueWPF})")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "Not neccessary.", Scope = "member", Target = "~M:FootballDatabase.UI.BL.LeagueWPFLogic.DeleteLeague(System.Collections.Generic.IList{FootballDatabase.UI.Data.LeagueWPF},FootballDatabase.UI.Data.LeagueWPF)")]
