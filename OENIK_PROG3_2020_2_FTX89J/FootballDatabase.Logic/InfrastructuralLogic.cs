﻿// <copyright file="InfrastructuralLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FootballDatabase.Data;
    using FootballDatabase.Repository;

    /// <summary>
    /// Class that implements the IImfrastructuralLogic interface.
    /// </summary>
    public class InfrastructuralLogic : IInfrastructuralLogic
    {
        private IStadiumRepository stadiumRepo;

        private ILeagueRepository leagueRepo;

        private ITeamRepository teamRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="InfrastructuralLogic"/> class.
        /// </summary>
        /// <param name="stadiumRepo">Parameter which is compatible with the IStadiumRepository interface.</param>
        /// <param name="leagueRepo">Parameter which is compatible with the ILeagueRepository interface.</param>
        /// <param name="teamRepo">Parameter which is compatible with the ITeamRepository interface.</param>
        public InfrastructuralLogic(IStadiumRepository stadiumRepo, ILeagueRepository leagueRepo, ITeamRepository teamRepo)
        {
            this.stadiumRepo = stadiumRepo;
            this.leagueRepo = leagueRepo;
            this.teamRepo = teamRepo;
        }

        /// <summary>
        /// Method that changes the value of the Capacity property of a Stadium object.
        /// </summary>
        /// <param name="id">The primary key of a Stadium object.</param>
        /// <param name="newValue">The new value that will be inserted to the Capacity property of a Stadium object.</param>
        public void ChangeStadiumCapacity(int id, int newValue)
        {
            this.stadiumRepo.ChangeCapacity(id, newValue);
        }

        /// <summary>
        /// Method that changes the value of the Name property of a Stadium object.
        /// </summary>
        /// <param name="id">The primary key of a Stadium object.</param>
        /// <param name="newValue">The new value that will be inserted to the Name property of a Stadium object.</param>
        public void ChangeStadiumName(int id, string newValue)
        {
            this.stadiumRepo.ChangeName(id, newValue);
        }

        /// <summary>
        /// Method that changes the value of the Phone property of a Stadium object.
        /// </summary>
        /// <param name="id">The primary key of a Stadium object.</param>
        /// <param name="newValue">The new value that will be inserted to the Phone property of a Stadium object.</param>
        public void ChangeStadiumPhone(int id, string newValue)
        {
            this.stadiumRepo.ChangePhone(id, newValue);
        }

        /// <summary>
        /// Inserts a Stadium class element into the database.
        /// </summary>
        /// <param name="stadium">The element that will be inserted into the database.</param>
        public void InsertStadium(Stadium stadium)
        {
            this.stadiumRepo.Insert(stadium);
        }

        /// <summary>
        /// Deletes a Stadium type element from the database.
        /// </summary>
        /// <param name="stadium">The element that needs to be removed.</param>
        /// <returns>Returns true if the element exists in the database.</returns>
        public bool RemoveStadium(Stadium stadium)
        {
            return this.stadiumRepo.Remove(stadium);
        }

        /// <summary>
        /// Finds the stadium with the biggest capacity in every league.
        /// </summary>
        /// <returns>The name of the league and the capacity of the biggest stadium.</returns>
        public List<LeagueMaximumCapacity> MaximumCapacity()
        {
            var maxCap = from league in this.leagueRepo.GetAll()
                         join team in this.teamRepo.GetAll() on league.Id equals team.LeagueId
                         join stadium in this.stadiumRepo.GetAll() on team.StadiumId equals stadium.Id
                         group stadium by new { league.Id, league.Name } into g
                         select new LeagueMaximumCapacity()
                         {
                             LeagueId = g.Key.Id,
                             LeagueName = g.Key.Name,
                             MaximumCapacity = g.Max(s => s.Capacity),
                         };
            return maxCap.ToList();
        }

        /// <summary>
        /// Async version for the MaximumCapacity method.
        /// </summary>
        /// <returns>Returns a task with List return value.</returns>
        public Task<List<LeagueMaximumCapacity>> MaximumCapacityAsync()
        {
            return Task.Run(() => this.MaximumCapacity());
        }
    }
}
