﻿// <copyright file="TransferLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FootballDatabase.Data;
    using FootballDatabase.Repository;

    /// <summary>
    /// Class that implements the ITransferLogic interface.
    /// </summary>
    public class TransferLogic : ITransferLogic
    {
        private IPlayerRepository playerRepo;

        private ITeamRepository teamRepo;

        private ILeagueRepository leagueRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="TransferLogic"/> class.
        /// </summary>
        /// <param name="playerRepo">Parameter which is compatible with the IPlayerRepository interface.</param>
        /// <param name="teamRepo">Parameter which is compatible with the ITeamRepository interface.</param>
        /// <param name="leagueRepo">Parameter which is compatible with the ILeagueRepository interface.</param>
        public TransferLogic(IPlayerRepository playerRepo, ITeamRepository teamRepo, ILeagueRepository leagueRepo)
        {
            this.playerRepo = playerRepo;
            this.teamRepo = teamRepo;
            this.leagueRepo = leagueRepo;
        }

        /// <summary>
        /// Inserts a Player class element into the database.
        /// </summary>
        /// <param name="player">The element that will be inserted into the database.</param>
        public void InsertPlayer(Player player)
        {
            this.playerRepo.Insert(player);
        }

        /// <summary>
        /// Deletes a Player type element from the database.
        /// </summary>
        /// <param name="player">The element that needs to be removed.</param>
        /// <returns>Returns true if the element exists in the database.</returns>
        public bool RemovePlayer(Player player)
        {
            return this.playerRepo.Remove(player);
        }

        /// <summary>
        /// Transfers the player to his new team.
        /// </summary>
        /// <param name="id">Id of the player who will be transferred.</param>
        /// <param name="newTeamId">His new team's id.</param>
        public void ChangePlayerTeamId(int id, int newTeamId)
        {
            this.playerRepo.ChangeTeamId(id, newTeamId);
        }

        /// <summary>
        /// Transfers the highest value player from each league, to one of the three richest clubs in the same league, based on the sum of their players' market value.
        /// </summary>
        /// <returns>The list of players, who changed their clubs.</returns>
        public List<TransferResult> TransferHighestValue()
        {
            List<int> transferredIds = new List<int>();

            Random rnd = new Random();

            foreach (var league in this.leagueRepo.GetAll().ToList())
            {
                int highestId = 0;
                int[] richestTeamsValues = new int[3];
                int[] richestTeamsIds = new int[3];

                var leagueTeams = from team in this.teamRepo.GetAll()
                                  where league.Id.Equals(team.LeagueId)
                                  select new { ID = team.Id };
                foreach (var team in leagueTeams)
                {
                    int teamValue = 0;
                    var teamPlayers = from player in this.playerRepo.GetAll()
                                      where team.ID.Equals(player.TeamId)
                                      select new { VALUE=player.MarketValue, ID=player.Id };
                    foreach (var player in teamPlayers)
                    {
                        if (this.playerRepo.GetOne(highestId) == null || !this.playerRepo.GetOne(highestId).Team.League.Equals(league))
                        {
                            highestId = player.ID;
                        }

                        if (player.VALUE > this.playerRepo.GetOne(highestId).MarketValue)
                        {
                            highestId = player.ID;
                        }

                        teamValue += (int)player.VALUE;
                    }

                    int minIndex = 0;
                    for (int i = 1; i < 3; i++)
                    {
                        if (richestTeamsValues[i] < richestTeamsValues[minIndex])
                        {
                            minIndex = i;
                        }
                    }

                    if (teamValue > richestTeamsValues[minIndex])
                    {
                        richestTeamsValues[minIndex] = teamValue;
                        richestTeamsIds[minIndex] = team.ID;
                    }
                }

                int newTeamID = richestTeamsIds[rnd.Next(0, 3)];
                while (newTeamID == this.playerRepo.GetOne(highestId).TeamId)
                {
                    newTeamID = richestTeamsIds[rnd.Next(0, 3)];
                }

                this.ChangePlayerTeamId(highestId, newTeamID);
                transferredIds.Add(highestId);
            }

            var newDatabase = from currentLeague in this.leagueRepo.GetAll()
                              join currentTeam in this.teamRepo.GetAll() on currentLeague.Id equals currentTeam.LeagueId
                              join currentPlayer in this.playerRepo.GetAll() on currentTeam.Id equals currentPlayer.TeamId
                              where transferredIds.Contains(currentPlayer.Id)
                              select new TransferResult()
                              {
                                  PlayerId = currentPlayer.Id, PlayerName = currentPlayer.Name, NewTeamName = currentTeam.Name,
                                  LeagueName = currentLeague.Name, TransferFee = currentPlayer.MarketValue,
                              };
            return newDatabase.ToList();
        }

        /// <summary>
        /// Async version for the TransferHighestValue method.
        /// </summary>
        /// <returns>Returns a task with List return value.</returns>
        public Task<List<TransferResult>> TransferHighestValueAsync()
        {
            return Task.Run(() => this.TransferHighestValue());
        }
    }
}