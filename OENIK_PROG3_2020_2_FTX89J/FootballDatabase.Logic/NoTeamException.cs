﻿// <copyright file="NoTeamException.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Custom exception class when wrong team is entered.
    /// </summary>
    public class NoTeamException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NoTeamException"/> class.
        /// </summary>
        public NoTeamException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NoTeamException"/> class.
        /// </summary>
        /// <param name="message">Error message.</param>
        public NoTeamException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NoTeamException"/> class.
        /// </summary>
        /// <param name="message">Error message.</param>
        /// <param name="innerException">Inner exception.</param>
        public NoTeamException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
