﻿// <copyright file="FinancialLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FootballDatabase.Repository;

    /// <summary>
    /// Class that implements the IFinancialLogic interface.
    /// </summary>
    public class FinancialLogic : IFinancialLogic
    {
        private IPlayerRepository playerRepo;

        private ITeamRepository teamRepo;

        private ILeagueRepository leagueRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="FinancialLogic"/> class.
        /// </summary>
        /// <param name="playerRepo">Parameter which is compatible with the IPlayerRepository interface.</param>
        /// <param name="teamRepo">Parameter which is compatible with the ITeamRepository interface.</param>
        /// <param name="leagueRepo">Parameter which is compatible with the ILeagueRepository interface.</param>
        public FinancialLogic(IPlayerRepository playerRepo, ITeamRepository teamRepo, ILeagueRepository leagueRepo)
        {
            this.playerRepo = playerRepo;
            this.teamRepo = teamRepo;
            this.leagueRepo = leagueRepo;
        }

        /// <summary>
        /// Method that changes the value of the AverageValue property of a League object.
        /// </summary>
        /// <param name="id">The primary key of a League object.</param>
        /// <param name="newValue">The new value that will be inserted to the AverageValue property of a League object.</param>
        public void ChangeLeagueAverageValue(int id, int newValue)
        {
            this.leagueRepo.ChangeAverageValue(id, newValue);
        }

        /// <summary>
        /// Method that changes the value of the MarketValue property of a Player object.
        /// </summary>
        /// <param name="id">The primary key of a Player object.</param>
        /// <param name="newValue">The new value that will be inserted to the MarketValue property of a Player object.</param>
        public void ChangePlayerMarketValue(int id, int newValue)
        {
            this.playerRepo.ChangeMarketValue(id, newValue);
        }

        /// <summary>
        /// Finds all teams with less valuable players on average than the league average.
        /// </summary>
        /// <returns>Returns the teams with less valuable players on average than the league average.</returns>
        public List<BelowAverage> BelowLeagueAverage()
        {
            var teamAverages = from team in this.teamRepo.GetAll()
                               join player in this.playerRepo.GetAll() on team.Id equals player.TeamId
                               group player by player.TeamId into g
                               select new { TEAM_ID = g.Key, AVG_VALUE = g.Average(v => v.MarketValue) };

            var teamsBelow = from league in this.leagueRepo.GetAll()
                             join team in this.teamRepo.GetAll() on league.Id equals team.LeagueId
                             join teamAvg in teamAverages on team.Id equals teamAvg.TEAM_ID
                             where teamAvg.AVG_VALUE < league.AverageValue
                             select new BelowAverage() { LeagueId = league.Id, LeagueName = league.Name, TeamId = team.Id, TeamName = team.Name };
            return teamsBelow.ToList();
        }

        /// <summary>
        /// Async version for the BelowLeagueAverage method.
        /// </summary>
        /// <returns>Returns a task with List return value.</returns>
        public Task<List<BelowAverage>> BelowLeagueAverageAsync()
        {
            return Task.Run(() => this.BelowLeagueAverage());
        }

        /// <summary>
        /// A method that changes a league's average market value according to the value of the players that plays in that league.
        /// </summary>
        /// <param name="id">The primary key of a League object.</param>
        public void FixLeagueAverageMarketValue(int id)
        {
            int count = 0;
            int value = 0;
            foreach (var item in this.playerRepo.GetAll().ToList())
            {
                if (this.teamRepo.GetOne(item.TeamId).LeagueId == id)
                {
                    value += item.MarketValue;
                    count++;
                }
            }

            if (count != 0)
            {
                this.leagueRepo.ChangeAverageValue(id, value / count);
            }
            else
            {
                this.leagueRepo.ChangeAverageValue(id, 0);
            }
        }
    }
}
