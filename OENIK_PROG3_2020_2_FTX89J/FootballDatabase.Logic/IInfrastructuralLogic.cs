﻿// <copyright file="IInfrastructuralLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FootballDatabase.Data;

    /// <summary>
    /// Interface for infrastructural operations on database entities.
    /// </summary>
    public interface IInfrastructuralLogic
    {
        /// <summary>
        /// Inserts a Stadium class element into the database.
        /// </summary>
        /// <param name="stadium">The element that will be inserted into the database.</param>
        void InsertStadium(Stadium stadium);

        /// <summary>
        /// Deletes a Stadium type element from the database.
        /// </summary>
        /// <param name="stadium">The element that needs to be removed.</param>
        /// <returns>Returns true if the element exists in the database.</returns>
        bool RemoveStadium(Stadium stadium);

        /// <summary>
        /// Method that changes the value of the Name property of a Stadium object.
        /// </summary>
        /// <param name="id">The primary key of a Stadium object.</param>
        /// <param name="newValue">The new value that will be inserted to the Name property of a Stadium object.</param>
        void ChangeStadiumName(int id, string newValue);

        /// <summary>
        /// Method that changes the value of the Capacity property of a Stadium object.
        /// </summary>
        /// <param name="id">The primary key of a Stadium object.</param>
        /// <param name="newValue">The new value that will be inserted to the Capacity property of a Stadium object.</param>
        void ChangeStadiumCapacity(int id, int newValue);

        /// <summary>
        /// Method that changes the value of the Phone property of a Stadium object.
        /// </summary>
        /// <param name="id">The primary key of a Stadium object.</param>
        /// <param name="newValue">The new value that will be inserted to the Phone property of a Stadium object.</param>
        void ChangeStadiumPhone(int id, string newValue);

        /// <summary>
        /// Finds the stadium with the biggest capacity in every league.
        /// </summary>
        /// <returns>The name of the league and the capacity of the biggest stadium in a List.</returns>
        List<LeagueMaximumCapacity> MaximumCapacity();

        /// <summary>
        /// Async version for the MaximumCapacity method.
        /// </summary>
        /// <returns>Returns a task with List return value.</returns>
        Task<List<LeagueMaximumCapacity>> MaximumCapacityAsync();
    }
}
