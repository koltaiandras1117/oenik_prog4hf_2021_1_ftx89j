﻿// <copyright file="IStatisticalLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using FootballDatabase.Data;

    /// <summary>
    /// Interface for statistical operations on database entities.
    /// </summary>
    public interface IStatisticalLogic
    {
        /// <summary>
        /// Method that finds a League element in the database by its key.
        /// </summary>
        /// <param name="id">Key that identifies the object.</param>
        /// <returns>Returns the League element with the matching key.</returns>
        League GetOneLeague(int id);

        /// <summary>
        /// Method for getting all League type elements in the database.
        /// </summary>
        /// <returns>Returns all Leage class elements in a List.</returns>
        IList<League> GetAllLeagues();

        /// <summary>
        /// Inserts a League class element into the database.
        /// </summary>
        /// <param name="league">The element that will be inserted into the database.</param>
        void InsertLeague(League league);

        /// <summary>
        /// Deletes a League type element from the database.
        /// </summary>
        /// <param name="league">The element that needs to be removed.</param>
        /// <returns>Returns true if the element exists in the database.</returns>
        bool RemoveLeague(League league);

        /// <summary>
        /// Method that finds a Player element in the database by its key.
        /// </summary>
        /// <param name="id">Key that identifies the object.</param>
        /// <returns>Returns the League element with the matching key.</returns>
        Player GetOnePlayer(int id);

        /// <summary>
        /// Method for getting all Player type elements in the database.
        /// </summary>
        /// <returns>Returns all Player class elements in a List.</returns>
        IList<Player> GetAllPlayers();

        /// <summary>
        /// Method that changes the value of the Assists property of a Player object.
        /// </summary>
        /// <param name="id">The primary key of a Player object.</param>
        /// <param name="newValue">The new value that will be inserted to the Assists property of a Player object.</param>
        void ChangePlayerAssists(int id, int newValue);

        /// <summary>
        /// Method that changes the value of the Goals property of a Player object.
        /// </summary>
        /// <param name="id">The primary key of a Player object.</param>
        /// <param name="newValue">The new value that will be inserted to the Goals property of a Player object.</param>
        void ChangePlayerGoals(int id, int newValue);

        /// <summary>
        /// Method that changes the value of the Matches property of a Player object.
        /// </summary>
        /// <param name="id">The primary key of a Player object.</param>
        /// <param name="newValue">The new value that will be inserted to the Matches property of a Player object.</param>
        void ChangePlayerMatches(int id, int newValue);

        /// <summary>
        /// Method that changes the value of the Number property of a Player object.
        /// </summary>
        /// <param name="id">The primary key of a Player object.</param>
        /// <param name="newValue">The new value that will be inserted to the Number property of a Player object.</param>
        void ChangePlayerNumber(int id, int newValue);

        /// <summary>
        /// Method that finds a Team element in the database by its key.
        /// </summary>
        /// <param name="id">Key that identifies the object.</param>
        /// <returns>Returns the Team element with the matching key.</returns>
        Team GetOneTeam(int id);

        /// <summary>
        /// Method for getting all Team type elements in the database.
        /// </summary>
        /// <returns>Returns all Team class elements in a List.</returns>
        IList<Team> GetAllTeams();

        /// <summary>
        /// Inserts a Team class element into the database.
        /// </summary>
        /// <param name="team">The element that will be inserted into the database.</param>
        void InsertTeam(Team team);

        /// <summary>
        /// Deletes a Team type element from the database.
        /// </summary>
        /// <param name="team">The element that needs to be removed.</param>
        /// <returns>Returns true if the element exists in the database.</returns>
        bool RemoveTeam(Team team);

        /// <summary>
        /// Method that finds a Stadium element in the database by its key.
        /// </summary>
        /// <param name="id">Key that identifies the object.</param>
        /// <returns>Returns the Stadium element with the matching key.</returns>
        Stadium GetOneStadium(int id);

        /// <summary>
        /// Method for getting all Stadium type elements in the database.
        /// </summary>
        /// <returns>Returns all Stadium class elements in a List.</returns>
        IList<Stadium> GetAllStadiums();

        /// <summary>
        /// Method that changes the value of the Name property of a Team object.
        /// </summary>
        /// <param name="id">The primary key of a Team object.</param>
        /// <param name="newValue">The new value that will be inserted to the Name property of a Team object.</param>
        void ChangeTeamName(int id, string newValue);

        /// <summary>
        /// Method that changes the value of the Name property of a League object.
        /// </summary>
        /// <param name="id">The primary key of a League object.</param>
        /// <param name="newValue">The new value that will be inserted to the Name property of a League object.</param>
        void ChangeLeagueName(int id, string newValue);

        /// <summary>
        /// Method that changes the value of the Season property of a League object.
        /// </summary>
        /// <param name="id">The primary key of a League object.</param>
        /// <param name="newValue">The new value that will be inserted to the Season property of a League object.</param>
        void ChangeLeagueSeason(int id, string newValue);

        /// <summary>
        /// Method that changes the value of the Country property of a League object.
        /// </summary>
        /// <param name="id">The primary key of a League object.</param>
        /// <param name="newValue">The new value that will be inserted to the Country property of a League object.</param>
        void ChangeLeagueCountry(int id, string newValue);

        /// <summary>
        /// Method that changes the value of the Division property of a League object.
        /// </summary>
        /// <param name="id">The primary key of a League object.</param>
        /// <param name="newValue">The new value that will be inserted to the Division property of a League object.</param>
        void ChangeLeagueDivision(int id, int newValue);

        /// <summary>
        /// Method for getting the a team's id given its name.
        /// </summary>
        /// <param name="name">The name of the team.</param>
        /// <returns>The id of the team.</returns>
        int GetIdFromTeamName(string name);

        /// <summary>
        /// Method that changes the value of the Name property of a Player object.
        /// </summary>
        /// <param name="id">The primary key of a Player object.</param>
        /// <param name="newValue">The new value that will be inserted to the Name property of a Player object.</param>
        void ChangePlayerName(int id, string newValue);

        /// <summary>
        /// Method that changes the value of the DateOfBirth property of a Player object.
        /// </summary>
        /// <param name="id">The primary key of a Player object.</param>
        /// <param name="newValue">The new value that will be inserted to the DateOfBirth property of a Player object.</param>
        void ChangePlayerBirthday(int id, DateTime newValue);

        /// <summary>
        /// Method that changes the value of the Nationality property of a Player object.
        /// </summary>
        /// <param name="id">The primary key of a Player object.</param>
        /// <param name="newValue">The new value that will be inserted to the Nationality property of a Player object.</param>
        void ChangePlayerNationality(int id, string newValue);

        /// <summary>
        /// Method that changes the value of the Position property of a Player object.
        /// </summary>
        /// <param name="id">The primary key of a Player object.</param>
        /// <param name="newValue">The new value that will be inserted to the Position property of a Player object.</param>
        void ChangePlayerPosition(int id, string newValue);
    }
}
