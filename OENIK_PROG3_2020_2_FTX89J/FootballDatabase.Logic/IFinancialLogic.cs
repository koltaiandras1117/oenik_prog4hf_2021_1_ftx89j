﻿// <copyright file="IFinancialLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for financial operations on database entities.
    /// </summary>
    public interface IFinancialLogic
    {
        /// <summary>
        /// Method that changes the value of the AverageValue property of a League object.
        /// </summary>
        /// <param name="id">The primary key of a League object.</param>
        /// <param name="newValue">The new value that will be inserted to the AverageValue property of a League object.</param>
        void ChangeLeagueAverageValue(int id, int newValue);

        /// <summary>
        /// Method that changes the value of the MarketValue property of a Player object.
        /// </summary>
        /// <param name="id">The primary key of a Player object.</param>
        /// <param name="newValue">The new value that will be inserted to the MarketValue property of a Player object.</param>
        void ChangePlayerMarketValue(int id, int newValue);

        /// <summary>
        /// Finds all teams with less valuable players on average than the league average.
        /// </summary>
        /// <returns>Returns the teams with less valuable players on average than the league average.</returns>
        public List<BelowAverage> BelowLeagueAverage();

        /// <summary>
        /// Async version for the BelowLeagueAverage method.
        /// </summary>
        /// <returns>Returns a task with List return value.</returns>
        Task<List<BelowAverage>> BelowLeagueAverageAsync();

        /// <summary>
        /// A method that changes a league's average market value according to the value of the players that plays in that league.
        /// </summary>
        /// <param name="id">The primary key of a League object.</param>
        void FixLeagueAverageMarketValue(int id);
    }
}
