﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Globalization", "CA1307:Specify StringComparison", Justification = "Unnecessary", Scope = "member", Target = "~M:FootballDatabase.Logic.TransferResult.Equals(System.Object)~System.Boolean")]
[assembly: SuppressMessage("Security", "CA5394:Do not use insecure randomness", Justification = "Unnecessary", Scope = "member", Target = "~M:FootballDatabase.Logic.TransferLogic.TransferHighestValue~System.Collections.Generic.List{FootballDatabase.Logic.TransferResult}")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "Unnecessary", Scope = "member", Target = "~M:FootballDatabase.Logic.ITransferLogic.TransferHighestValue~System.Collections.Generic.List{FootballDatabase.Logic.TransferResult}")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "Unnecessary", Scope = "member", Target = "~M:FootballDatabase.Logic.IInfrastructuralLogic.MaximumCapacity~System.Collections.Generic.List{FootballDatabase.Logic.LeagueMaximumCapacity}")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "Unnecessary", Scope = "member", Target = "~M:FootballDatabase.Logic.IFinancialLogic.BelowLeagueAverage~System.Collections.Generic.List{FootballDatabase.Logic.BelowAverage}")]
