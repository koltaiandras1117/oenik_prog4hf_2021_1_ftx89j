﻿// <copyright file="TransferResult.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// A class that represents a transfer.
    /// </summary>
    public class TransferResult
    {
        /// <summary>
        /// Gets or sets the player's id.
        /// </summary>
        public int PlayerId { get; set; }

        /// <summary>
        /// Gets or sets the player's name.
        /// </summary>
        public string PlayerName { get; set; }

        /// <summary>
        /// Gets or sets the player's new team's name.
        /// </summary>
        public string NewTeamName { get; set; }

        /// <summary>
        /// Gets or sets the new team's league.
        /// </summary>
        public string LeagueName { get; set; }

        /// <summary>
        /// Gets or sets the transfer fee.
        /// </summary>
        public int TransferFee { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>True if the specified object is equal to the current object; otherwise, false.</returns>
        public override bool Equals(object obj)
        {
            if (obj is TransferResult)
            {
                TransferResult other = obj as TransferResult;
                return this.PlayerId == other.PlayerId && this.PlayerName == other.PlayerName &&
                    this.NewTeamName == other.NewTeamName && this.LeagueName == other.LeagueName &&
                    this.TransferFee == other.TransferFee;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            return this.PlayerId;
        }

        /// <summary>
        /// Returns a string that represents the current TransferResult object.
        /// </summary>
        /// <returns>A string that represents the current TransferResult object. </returns>
        public override string ToString()
        {
            return "Player_ID: " + this.PlayerId + ", Player_Name: " + this.PlayerName + ", New_Team_Name: " + this.NewTeamName + ", Leage_Name: " + this.LeagueName + ", Transfer_Fee: " + this.TransferFee;
        }
    }
}
