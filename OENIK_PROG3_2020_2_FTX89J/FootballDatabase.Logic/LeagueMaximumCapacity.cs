﻿// <copyright file="LeagueMaximumCapacity.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// A class that represents every league's maximum capacity.
    /// </summary>
    public class LeagueMaximumCapacity
    {
        /// <summary>
        /// Gets or sets the league' id.
        /// </summary>
        public int LeagueId { get; set; }

        /// <summary>
        /// Gets or sets the league's name.
        /// </summary>
        public string LeagueName { get; set; }

        /// <summary>
        /// Gets or sets the maximum capacitiy (of the league).
        /// </summary>
        public int MaximumCapacity { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>True if the specified object is equal to the current object; otherwise, false.</returns>
        public override bool Equals(object obj)
        {
            return obj is LeagueMaximumCapacity capacity &&
                   this.LeagueId == capacity.LeagueId &&
                   this.LeagueName == capacity.LeagueName &&
                   this.MaximumCapacity == capacity.MaximumCapacity;
        }

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            return this.LeagueId * this.MaximumCapacity;
        }

        /// <summary>
        /// Returns a string that represents the current LeagueMaximumCapacity object.
        /// </summary>
        /// <returns>A string that represents the current LeagueMaximumCapacity object. </returns>
        public override string ToString()
        {
            return "League_ID: " + this.LeagueId + ", League_Name: " + this.LeagueName + ", Maximum_Capacity: " + this.MaximumCapacity;
        }
    }
}
