﻿// <copyright file="BelowAverage.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// A class that represents the teams that are below the league average in terms of average player value.
    /// </summary>
    public class BelowAverage
    {
        /// <summary>
        /// Gets or sets the league's id.
        /// </summary>
        public int LeagueId { get; set; }

        /// <summary>
        /// Gets or sets the league's name.
        /// </summary>
        public string LeagueName { get; set; }

        /// <summary>
        /// Gets or sets the team's id.
        /// </summary>
        public int TeamId { get; set; }

        /// <summary>
        /// Gets or sets the team's name.
        /// </summary>
        public string TeamName { get; set; }

        /// <summary>
        /// Returns a string that represents the current BelowAverage object.
        /// </summary>
        /// <returns>A string that represents the current BelowAverage object. </returns>
        public override string ToString()
        {
            return "League_ID: " + this.LeagueId + ", League_Name: " + this.LeagueName + ", Team_ID: " + this.TeamId + ", Team_Name: " + this.TeamName;
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>True if the specified object is equal to the current object; otherwise, false.</returns>
        public override bool Equals(object obj)
        {
            return obj is BelowAverage average &&
                   this.LeagueId == average.LeagueId &&
                   this.LeagueName == average.LeagueName &&
                   this.TeamId == average.TeamId &&
                   this.TeamName == average.TeamName;
        }

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            return this.TeamId;
        }
    }
}
