﻿// <copyright file="ITransferLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FootballDatabase.Data;

    /// <summary>
    /// Interface for transfer operations on database entities.
    /// </summary>
    public interface ITransferLogic
    {
        /// <summary>
        /// Inserts a Player class element into the database.
        /// </summary>
        /// <param name="player">The element that will be inserted into the database.</param>
        void InsertPlayer(Player player);

        /// <summary>
        /// Deletes a Player type element from the database.
        /// </summary>
        /// <param name="player">The element that needs to be removed.</param>
        /// <returns>Returns true if the element exists in the database.</returns>
        bool RemovePlayer(Player player);

        /// <summary>
        /// Transfers the player to his new team.
        /// </summary>
        /// <param name="id">Id of the player who will be transferred.</param>
        /// <param name="newTeamId">His new team's id.</param>
        void ChangePlayerTeamId(int id, int newTeamId);

        /// <summary>
        /// Transfers the highest value player from each league, to one of the five richest clubs in the same league, based on the sum of their players' market value.
        /// </summary>
        /// <returns>The list of players, who changed their clubs.</returns>
        public List<TransferResult> TransferHighestValue();

        /// <summary>
        /// Async version for the TransferHighestValue method.
        /// </summary>
        /// <returns>Returns a task with List return value.</returns>
        public Task<List<TransferResult>> TransferHighestValueAsync();
    }
}
