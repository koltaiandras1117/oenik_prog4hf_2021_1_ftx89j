﻿// <copyright file="StatisticalLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using FootballDatabase.Data;
    using FootballDatabase.Repository;

    /// <summary>
    /// Class that implements the IStatisticalLogic interface.
    /// </summary>
    public class StatisticalLogic : IStatisticalLogic
    {
        private IPlayerRepository playerRepo;

        private ILeagueRepository leagueRepo;

        private IStadiumRepository stadiumRepo;

        private ITeamRepository teamRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="StatisticalLogic"/> class.
        /// </summary>
        /// <param name="playerRepo">Parameter which is compatible with the IPlayerRepository interface.</param>
        /// <param name="leagueRepo">Parameter which is compatible with the ILeagueRepository interface.</param>
        /// <param name="stadiumRepo">Parameter which is compatible with the IStadiumRepository interface.</param>
        /// <param name="teamRepo">Parameter which is compatible with the ITeamRepository interface.</param>
        public StatisticalLogic(IPlayerRepository playerRepo, ILeagueRepository leagueRepo, IStadiumRepository stadiumRepo, ITeamRepository teamRepo)
        {
            this.playerRepo = playerRepo;
            this.leagueRepo = leagueRepo;
            this.stadiumRepo = stadiumRepo;
            this.teamRepo = teamRepo;
        }

        /// <summary>
        /// Method that changes the value of the Assists property of a Player object.
        /// </summary>
        /// <param name="id">The primary key of a Player object.</param>
        /// <param name="newValue">The new value that will be inserted to the Assists property of a Player object.</param>
        public void ChangePlayerAssists(int id, int newValue)
        {
            this.playerRepo.ChangeAssists(id, newValue);
        }

        /// <summary>
        /// Method that changes the value of the Goals property of a Player object.
        /// </summary>
        /// <param name="id">The primary key of a Player object.</param>
        /// <param name="newValue">The new value that will be inserted to the Goals property of a Player object.</param>
        public void ChangePlayerGoals(int id, int newValue)
        {
            this.playerRepo.ChangeGoals(id, newValue);
        }

        /// <summary>
        /// Method that changes the value of the Name property of a Team object.
        /// </summary>
        /// <param name="id">The primary key of a Team object.</param>
        /// <param name="newValue">The new value that will be inserted to the Name property of a Team object.</param>
        public void ChangeTeamName(int id, string newValue)
        {
            this.teamRepo.ChangeName(id, newValue);
        }

        /// <summary>
        /// Method for getting all Stadium type elements in the database.
        /// </summary>
        /// <returns>Returns all Stadium class elements in a List.</returns>
        public IList<Stadium> GetAllStadiums()
        {
            return this.stadiumRepo.GetAll().ToList();
        }

        /// <summary>
        /// Method for getting all Team type elements in the database.
        /// </summary>
        /// <returns>Returns all Team class elements in a List.</returns>
        public IList<Team> GetAllTeams()
        {
            return this.teamRepo.GetAll().ToList();
        }

        /// <summary>
        /// Method that finds a Stadium element in the database by its key.
        /// </summary>
        /// <param name="id">Key that identifies the object.</param>
        /// <returns>Returns the Stadium element with the matching key.</returns>
        public Stadium GetOneStadium(int id)
        {
            return this.stadiumRepo.GetOne(id);
        }

        /// <summary>
        /// Method that finds a Team element in the database by its key.
        /// </summary>
        /// <param name="id">Key that identifies the object.</param>
        /// <returns>Returns the Team element with the matching key.</returns>
        public Team GetOneTeam(int id)
        {
            return this.teamRepo.GetOne(id);
        }

        /// <summary>
        /// Inserts a Team class element into the database.
        /// </summary>
        /// <param name="team">The element that will be inserted into the database.</param>
        public void InsertTeam(Team team)
        {
            this.teamRepo.Insert(team);
        }

        /// <summary>
        /// Deletes a Team type element from the database.
        /// </summary>
        /// <param name="team">The element that needs to be removed.</param>
        /// <returns>Returns true if the element exists in the database.</returns>
        public bool RemoveTeam(Team team)
        {
            return this.teamRepo.Remove(team);
        }

        /// <summary>
        /// Method that changes the value of the Matches property of a Player object.
        /// </summary>
        /// <param name="id">The primary key of a Player object.</param>
        /// <param name="newValue">The new value that will be inserted to the Matches property of a Player object.</param>
        public void ChangePlayerMatches(int id, int newValue)
        {
            this.playerRepo.ChangeMatches(id, newValue);
        }

        /// <summary>
        /// Method that changes the value of the Number property of a Player object.
        /// </summary>
        /// <param name="id">The primary key of a Player object.</param>
        /// <param name="newValue">The new value that will be inserted to the Number property of a Player object.</param>
        public void ChangePlayerNumber(int id, int newValue)
        {
            this.playerRepo.ChangeNumber(id, newValue);
        }

        /// <summary>
        /// Method for getting all League type elements in the database.
        /// </summary>
        /// <returns>Returns all Leage class elements in a List.</returns>
        public IList<League> GetAllLeagues()
        {
            return this.leagueRepo.GetAll().ToList();
        }

        /// <summary>
        /// Method for getting all Player type elements in the database.
        /// </summary>
        /// <returns>Returns all Player class elements in a List.</returns>
        public IList<Player> GetAllPlayers()
        {
            return this.playerRepo.GetAll().ToList();
        }

        /// <summary>
        /// Method that finds a League element in the database by its key.
        /// </summary>
        /// <param name="id">Key that identifies the object.</param>
        /// <returns>Returns the League element with the matching key.</returns>
        public League GetOneLeague(int id)
        {
            return this.leagueRepo.GetOne(id);
        }

        /// <summary>
        /// Method that finds a Player element in the database by its key.
        /// </summary>
        /// <param name="id">Key that identifies the object.</param>
        /// <returns>Returns the League element with the matching key.</returns>
        public Player GetOnePlayer(int id)
        {
            return this.playerRepo.GetOne(id);
        }

        /// <summary>
        /// Inserts a League class element into the database.
        /// </summary>
        /// <param name="league">The element that will be inserted into the database.</param>
        public void InsertLeague(League league)
        {
            this.leagueRepo.Insert(league);
        }

        /// <summary>
        /// Deletes a League type element from the database.
        /// </summary>
        /// <param name="league">The element that needs to be removed.</param>
        /// <returns>Returns true if the element exists in the database.</returns>
        public bool RemoveLeague(League league)
        {
            return this.leagueRepo.Remove(league);
        }

        /// <summary>
        /// Method that changes the value of the Name property of a League object.
        /// </summary>
        /// <param name="id">The primary key of a League object.</param>
        /// <param name="newValue">The new value that will be inserted to the Name property of a League object.</param>
        public void ChangeLeagueName(int id, string newValue)
        {
            this.leagueRepo.ChangeName(id, newValue);
        }

        /// <summary>
        /// Method that changes the value of the Season property of a League object.
        /// </summary>
        /// <param name="id">The primary key of a League object.</param>
        /// <param name="newValue">The new value that will be inserted to the Season property of a League object.</param>
        public void ChangeLeagueSeason(int id, string newValue)
        {
            this.leagueRepo.ChangeSeason(id, newValue);
        }

        /// <summary>
        /// Method that changes the value of the Country property of a League object.
        /// </summary>
        /// <param name="id">The primary key of a League object.</param>
        /// <param name="newValue">The new value that will be inserted to the Country property of a League object.</param>
        public void ChangeLeagueCountry(int id, string newValue)
        {
            this.leagueRepo.ChangeCountry(id, newValue);
        }

        /// <summary>
        /// Method that changes the value of the Division property of a League object.
        /// </summary>
        /// <param name="id">The primary key of a League object.</param>
        /// <param name="newValue">The new value that will be inserted to the Division property of a League object.</param>
        public void ChangeLeagueDivision(int id, int newValue)
        {
            this.leagueRepo.ChangeDivision(id, newValue);
        }

        /// <summary>
        /// Method for getting the a team's id given its name.
        /// </summary>
        /// <param name="name">The name of the team.</param>
        /// <returns>The id of the team.</returns>
        public int GetIdFromTeamName(string name)
        {
            if (this.GetAllTeams().ToList().Find(team => team.Name == name) != null)
            {
                var id = this.GetAllTeams().ToList().Find(team => team.Name == name).Id;
                return id;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Method that changes the value of the Name property of a Player object.
        /// </summary>
        /// <param name="id">The primary key of a Player object.</param>
        /// <param name="newValue">The new value that will be inserted to the Name property of a Player object.</param>
        public void ChangePlayerName(int id, string newValue)
        {
            this.playerRepo.ChangeName(id, newValue);
        }

        /// <summary>
        /// Method that changes the value of the DateOfBirth property of a Player object.
        /// </summary>
        /// <param name="id">The primary key of a Player object.</param>
        /// <param name="newValue">The new value that will be inserted to the DateOfBirth property of a Player object.</param>
        public void ChangePlayerBirthday(int id, DateTime newValue)
        {
            this.playerRepo.ChangeBirthday(id, newValue);
        }

        /// <summary>
        /// Method that changes the value of the Nationality property of a Player object.
        /// </summary>
        /// <param name="id">The primary key of a Player object.</param>
        /// <param name="newValue">The new value that will be inserted to the Nationality property of a Player object.</param>
        public void ChangePlayerNationality(int id, string newValue)
        {
            this.playerRepo.ChangeNationality(id, newValue);
        }

        /// <summary>
        /// Method that changes the value of the Position property of a Player object.
        /// </summary>
        /// <param name="id">The primary key of a Player object.</param>
        /// <param name="newValue">The new value that will be inserted to the Position property of a Player object.</param>
        public void ChangePlayerPosition(int id, string newValue)
        {
            this.playerRepo.ChangePosition(id, newValue);
        }
    }
}
