﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Security", "CA5394:Do not use insecure randomness", Justification = "Unnecessary", Scope = "member", Target = "~M:FootballDatabase.Logic.Tests.InfrastructuralLogicTests.InfraStructuralLogic_WhenMaximumCapacityIsCalledWithRandomValues_ItReturnsTheHighestCapacitiesFromEachLeague")]
[assembly: SuppressMessage("Security", "CA5394:Do not use insecure randomness", Justification = "Unnecessary", Scope = "member", Target = "~M:FootballDatabase.Logic.Tests.FinancialLogicTests.FinancialLogic_WhenBelowLeagueAverageIsCalledWithRandomValues_ItReturnsEveryTeamBelowTheLeaguesAverageValue")]
[assembly: SuppressMessage("Security", "CA5394:Do not use insecure randomness", Justification = "Unnecessary", Scope = "member", Target = "~M:FootballDatabase.Logic.Tests.TransferLogicTests.TransferLogic_WhenTransferHighestValueIsCalledWithRandomValues_ItFindsTheHighestValuePlayersFromEachLeague")]
