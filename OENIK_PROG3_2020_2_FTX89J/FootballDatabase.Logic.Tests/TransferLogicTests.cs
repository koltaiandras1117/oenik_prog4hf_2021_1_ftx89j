﻿// <copyright file="TransferLogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using FootballDatabase.Data;
    using FootballDatabase.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Test class for Transfer Logic methods.
    /// </summary>
    [TestFixture]
    public class TransferLogicTests
    {
        /// <summary>
        /// Test for the RemovePlayer delete method.
        /// </summary>
        [Test]
        public void TransferLogic_WhenRemovePlayerIsCalled_ItCallsRemoveOnce()
        {
            Mock<IPlayerRepository> mockedPlayerRepo = new Mock<IPlayerRepository>();
            Mock<ITeamRepository> mockedTeamRepo = new Mock<ITeamRepository>();
            Mock<ILeagueRepository> mockedLeagueRepo = new Mock<ILeagueRepository>();
            TransferLogic transferLogic = new TransferLogic(mockedPlayerRepo.Object, mockedTeamRepo.Object, mockedLeagueRepo.Object);
            Player p = new Player() { Assists = 1, DateOfBirth = new DateTime(1990, 1, 1), Goals = 10, Id = 20, MarketValue = 100000, Matches = 6, Name = "Test Name", Nationality = "Hungary", Number = 10, Position = "Attacker", TeamId = 20 };

            transferLogic.RemovePlayer(p);

            mockedPlayerRepo.Verify(repo => repo.Remove(p), Times.Once);
        }

        /// <summary>
        /// Test for the TransferHighestValue method.
        /// </summary>
        [Test]
        public void TransferLogic_WhenTransferHighestValueIsCalledWithRandomValues_ItFindsTheHighestValuePlayersFromEachLeague()
        {
            Mock<IPlayerRepository> mockedPlayerRepo = new Mock<IPlayerRepository>();
            Mock<ITeamRepository> mockedTeamRepo = new Mock<ITeamRepository>();
            Mock<ILeagueRepository> mockedLeagueRepo = new Mock<ILeagueRepository>();
            TransferLogic transferLogic = new TransferLogic(mockedPlayerRepo.Object, mockedTeamRepo.Object, mockedLeagueRepo.Object);
            List<League> leagues = new List<League>()
            {
                new League() { Country = "England", Division = 1, Id = 1, Name = "Premier League", Season = "2018-2019" },
                new League() { Country = "Germany", Division = 1, Id = 2, Name = "Bundesliga", Season = "2019-2020" },
                new League() { Country = "Hungary", Division = 1, Id = 3, Name = "NB1", Season = "2013-2014" },
            };
            mockedLeagueRepo.Setup(repo => repo.GetAll()).Returns(leagues.AsQueryable());
            List<Team> teams = new List<Team>()
            {
                new Team() { AwayShirt = It.IsAny<string>(), HomeShirt = It.IsAny<string>(), Id = 2, LeagueId = 1, LeaguesWon = 2, Name = "Test team1", StadiumId = It.IsAny<int>(), League = leagues[0] },
                new Team() { AwayShirt = It.IsAny<string>(), HomeShirt = It.IsAny<string>(), Id = 3, LeagueId = 1, LeaguesWon = 2, Name = "Test team2", StadiumId = It.IsAny<int>(), League = leagues[0] },
                new Team() { AwayShirt = It.IsAny<string>(), HomeShirt = It.IsAny<string>(), Id = 4, LeagueId = 1, LeaguesWon = 2, Name = "Test team3", StadiumId = It.IsAny<int>(), League = leagues[0] },
                new Team() { AwayShirt = It.IsAny<string>(), HomeShirt = It.IsAny<string>(), Id = 5, LeagueId = 1, LeaguesWon = 2, Name = "Test team4", StadiumId = It.IsAny<int>(), League = leagues[0] },
                new Team() { AwayShirt = It.IsAny<string>(), HomeShirt = It.IsAny<string>(), Id = 6, LeagueId = 1, LeaguesWon = 2, Name = "Test team5", StadiumId = It.IsAny<int>(), League = leagues[0] },
                new Team() { AwayShirt = It.IsAny<string>(), HomeShirt = It.IsAny<string>(), Id = 7, LeagueId = 2, LeaguesWon = 2, Name = "Test team6", StadiumId = It.IsAny<int>(), League = leagues[1] },
                new Team() { AwayShirt = It.IsAny<string>(), HomeShirt = It.IsAny<string>(), Id = 8, LeagueId = 2, LeaguesWon = 2, Name = "Test team7", StadiumId = It.IsAny<int>(), League = leagues[1] },
                new Team() { AwayShirt = It.IsAny<string>(), HomeShirt = It.IsAny<string>(), Id = 9, LeagueId = 2, LeaguesWon = 2, Name = "Test team8", StadiumId = It.IsAny<int>(), League = leagues[1] },
                new Team() { AwayShirt = It.IsAny<string>(), HomeShirt = It.IsAny<string>(), Id = 10, LeagueId = 2, LeaguesWon = 2, Name = "Test team9", StadiumId = It.IsAny<int>(), League = leagues[1] },
                new Team() { AwayShirt = It.IsAny<string>(), HomeShirt = It.IsAny<string>(), Id = 11, LeagueId = 2, LeaguesWon = 2, Name = "Test team10", StadiumId = It.IsAny<int>(), League = leagues[1] },
                new Team() { AwayShirt = It.IsAny<string>(), HomeShirt = It.IsAny<string>(), Id = 12, LeagueId = 3, LeaguesWon = 2, Name = "Test team11", StadiumId = It.IsAny<int>(), League = leagues[2] },
                new Team() { AwayShirt = It.IsAny<string>(), HomeShirt = It.IsAny<string>(), Id = 13, LeagueId = 3, LeaguesWon = 2, Name = "Test team12", StadiumId = It.IsAny<int>(), League = leagues[2] },
                new Team() { AwayShirt = It.IsAny<string>(), HomeShirt = It.IsAny<string>(), Id = 14, LeagueId = 3, LeaguesWon = 2, Name = "Test team13", StadiumId = It.IsAny<int>(), League = leagues[2] },
                new Team() { AwayShirt = It.IsAny<string>(), HomeShirt = It.IsAny<string>(), Id = 15, LeagueId = 3, LeaguesWon = 2, Name = "Test team14", StadiumId = It.IsAny<int>(), League = leagues[2] },
                new Team() { AwayShirt = It.IsAny<string>(), HomeShirt = It.IsAny<string>(), Id = 16, LeagueId = 3, LeaguesWon = 2, Name = "Test team15", StadiumId = It.IsAny<int>(), League = leagues[2] },
            };
            mockedTeamRepo.Setup(repo => repo.GetAll()).Returns(teams.AsQueryable());
            int[] randomMarketValues = new int[165];
            Random rnd = new Random();
            List<Player> players = new List<Player>();
            for (int i = 0; i < 15; i++)
            {
                for (int j = 0; j < 11; j++)
                {
                    randomMarketValues[(i * 11) + j] = rnd.Next(1000000, 200000000);
                    Player p = new Player()
                    {
                        Assists = It.IsAny<int>(), DateOfBirth = It.IsAny<DateTime>(), Goals = It.IsAny<int>(),
                        Id = (i * 11) + j + 1, MarketValue = randomMarketValues[(i * 11) + j], Matches = It.IsAny<int>(),
                        Name = It.IsAny<string>(), Nationality = It.IsAny<string>(), Number = It.IsAny<int>(), Position = It.IsAny<string>(),
                        TeamId = i + 2, Team = teams[i],
                    };
                    players.Add(p);
                    mockedPlayerRepo.Setup(repo => repo.GetOne((i * 11) + j + 1)).Returns(players[(i * 11) + j]);
                }
            }

            mockedPlayerRepo.Setup(repo => repo.GetAll()).Returns(players.AsQueryable());
            int firstLeagueHighestValueIdx = 0;
            for (int i = 1; i < 55; i++)
            {
                if (randomMarketValues[i] > randomMarketValues[firstLeagueHighestValueIdx])
                {
                    firstLeagueHighestValueIdx = i;
                }
            }

            int secondLeagueHighestValueIdx = 55;
            for (int i = 56; i < 110; i++)
            {
                if (randomMarketValues[i] > randomMarketValues[secondLeagueHighestValueIdx])
                {
                    secondLeagueHighestValueIdx = i;
                }
            }

            int thirdLeagueHighestValueIdx = 110;
            for (int i = 111; i < 165; i++)
            {
                if (randomMarketValues[i] > randomMarketValues[thirdLeagueHighestValueIdx])
                {
                    thirdLeagueHighestValueIdx = i;
                }
            }

            var result = transferLogic.TransferHighestValue();
            List<int> queryIds = new List<int>();
            foreach (var item in result)
            {
                queryIds.Add(item.PlayerId);
            }

            Assert.That(queryIds, Does.Contain(firstLeagueHighestValueIdx + 1));
            Assert.That(queryIds, Does.Contain(secondLeagueHighestValueIdx + 1));
            Assert.That(queryIds, Does.Contain(thirdLeagueHighestValueIdx + 1));
        }
    }
}
