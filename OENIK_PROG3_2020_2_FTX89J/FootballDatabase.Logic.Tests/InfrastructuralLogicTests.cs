﻿// <copyright file="InfrastructuralLogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using FootballDatabase.Data;
    using FootballDatabase.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Test class for Infrastructural Logic methods.
    /// </summary>
    [TestFixture]
    public class InfrastructuralLogicTests
    {
        /// <summary>
        /// Test for the InsertStadium creating method.
        /// </summary>
        [Test]
        public void InfrastructuralLogic_WhenInsertStadiumIsCalled_ItCallsInsertOnce()
        {
            Mock<IStadiumRepository> mockedStadiumRepo = new Mock<IStadiumRepository>();
            Mock<ILeagueRepository> mockedLeagueRepo = new Mock<ILeagueRepository>();
            Mock<ITeamRepository> mockedTeamRepo = new Mock<ITeamRepository>();
            InfrastructuralLogic infrastructuralLogic = new InfrastructuralLogic(mockedStadiumRepo.Object, mockedLeagueRepo.Object, mockedTeamRepo.Object);
            Stadium s = new Stadium() { Id = 15, Address = "Test Address", Capacity = 1000, Name = "Test Name", Phone = "06123456789", YearOfBuilt = 1900 };

            infrastructuralLogic.InsertStadium(s);

            mockedStadiumRepo.Verify(repo => repo.Insert(s), Times.Once);
        }

        /// <summary>
        /// Test for the MaximumCapacity method.
        /// </summary>
        [Test]
        public void InfraStructuralLogic_WhenMaximumCapacityIsCalledWithRandomValues_ItReturnsTheHighestCapacitiesFromEachLeague()
        {
            Mock<ILeagueRepository> mockedLeagueRepo = new Mock<ILeagueRepository>();
            Mock<ITeamRepository> mockedTeamRepo = new Mock<ITeamRepository>();
            Mock<IStadiumRepository> mockedStadiumRepo = new Mock<IStadiumRepository>();
            IInfrastructuralLogic infrastructuralLogic = new InfrastructuralLogic(mockedStadiumRepo.Object, mockedLeagueRepo.Object, mockedTeamRepo.Object);
            List<League> leagues = new List<League>()
            {
                new League() { Country = "England", Division = 1, Id = 1, Name = "Premier League", Season = "2018-2019" },
                new League() { Country = "Germany", Division = 1, Id = 2, Name = "Bundesliga", Season = "2019-2020" },
                new League() { Country = "Hungary", Division = 1, Id = 3, Name = "NB1", Season = "2013-2014" },
            };
            mockedLeagueRepo.Setup(repo => repo.GetAll()).Returns(leagues.AsQueryable());
            List<Stadium> stadiums = new List<Stadium>();
            Random rnd = new Random();
            int[] randomCapacities = new int[9];
            for (int i = 0; i < 9; i++)
            {
                randomCapacities[i] = rnd.Next(5000, 100000);
                stadiums.Add(new Stadium() { Address = It.IsAny<string>(), Capacity = randomCapacities[i], Id = i + 1, Name = It.IsAny<string>() });
            }

            mockedStadiumRepo.Setup(repo => repo.GetAll()).Returns(stadiums.AsQueryable());
            List<Team> teams = new List<Team>()
            {
                new Team() { AwayShirt = It.IsAny<string>(), HomeShirt = It.IsAny<string>(), Id = 1, LeagueId = 1, LeaguesWon = It.IsAny<int>(), Name = "Test team1", StadiumId = 1, League = leagues[0] },
                new Team() { AwayShirt = It.IsAny<string>(), HomeShirt = It.IsAny<string>(), Id = 2, LeagueId = 1, LeaguesWon = It.IsAny<int>(), Name = "Test team2", StadiumId = 2, League = leagues[0] },
                new Team() { AwayShirt = It.IsAny<string>(), HomeShirt = It.IsAny<string>(), Id = 3, LeagueId = 1, LeaguesWon = It.IsAny<int>(), Name = "Test team3", StadiumId = 3, League = leagues[0] },
                new Team() { AwayShirt = It.IsAny<string>(), HomeShirt = It.IsAny<string>(), Id = 4, LeagueId = 2, LeaguesWon = It.IsAny<int>(), Name = "Test team4", StadiumId = 4, League = leagues[1] },
                new Team() { AwayShirt = It.IsAny<string>(), HomeShirt = It.IsAny<string>(), Id = 7, LeagueId = 2, LeaguesWon = It.IsAny<int>(), Name = "Test team5", StadiumId = 5, League = leagues[1] },
                new Team() { AwayShirt = It.IsAny<string>(), HomeShirt = It.IsAny<string>(), Id = 30, LeagueId = 2, LeaguesWon = It.IsAny<int>(), Name = "Test team6", StadiumId = 6, League = leagues[1] },
                new Team() { AwayShirt = It.IsAny<string>(), HomeShirt = It.IsAny<string>(), Id = 37, LeagueId = 3, LeaguesWon = It.IsAny<int>(), Name = "Test team7", StadiumId = 7, League = leagues[2] },
                new Team() { AwayShirt = It.IsAny<string>(), HomeShirt = It.IsAny<string>(), Id = 20, LeagueId = 3, LeaguesWon = It.IsAny<int>(), Name = "Test team8", StadiumId = 8, League = leagues[2] },
                new Team() { AwayShirt = It.IsAny<string>(), HomeShirt = It.IsAny<string>(), Id = 11, LeagueId = 3, LeaguesWon = It.IsAny<int>(), Name = "Test team9", StadiumId = 9, League = leagues[2] },
            };
            mockedTeamRepo.Setup(repo => repo.GetAll()).Returns(teams.AsQueryable());
            int firstLeagueBiggestCapacityIdx = 0;
            for (int i = 1; i < 3; i++)
            {
                if (randomCapacities[i] > randomCapacities[firstLeagueBiggestCapacityIdx])
                {
                    firstLeagueBiggestCapacityIdx = i;
                }
            }

            int secondLeagueBiggestCapacityIdx = 3;
            for (int i = 4; i < 6; i++)
            {
                if (randomCapacities[i] > randomCapacities[secondLeagueBiggestCapacityIdx])
                {
                    secondLeagueBiggestCapacityIdx = i;
                }
            }

            int thirdLeagueBiggestCapacityIdx = 6;
            for (int i = 7; i < 9; i++)
            {
                if (randomCapacities[i] > randomCapacities[thirdLeagueBiggestCapacityIdx])
                {
                    thirdLeagueBiggestCapacityIdx = i;
                }
            }

            var result = infrastructuralLogic.MaximumCapacity();
            List<int> queryCapacities = new List<int>();
            foreach (var item in result)
            {
                queryCapacities.Add(item.MaximumCapacity);
            }

            Assert.That(queryCapacities, Does.Contain(randomCapacities[firstLeagueBiggestCapacityIdx]));
            Assert.That(queryCapacities, Does.Contain(randomCapacities[secondLeagueBiggestCapacityIdx]));
            Assert.That(queryCapacities, Does.Contain(randomCapacities[thirdLeagueBiggestCapacityIdx]));
        }
    }
}
