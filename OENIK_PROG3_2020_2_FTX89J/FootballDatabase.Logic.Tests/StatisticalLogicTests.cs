﻿// <copyright file="StatisticalLogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using FootballDatabase.Data;
    using FootballDatabase.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Test class for Statistical Logic methods.
    /// </summary>
    [TestFixture]
    public class StatisticalLogicTests
    {
        /// <summary>
        /// Test for the GetAll method.
        /// </summary>
        [Test]
        public void StatisticalLogic_WhenGetAllTeamsIsCalled_ItCallsGetAllOnce()
        {
            Mock<IPlayerRepository> mockedPlayerRepo = new Mock<IPlayerRepository>();
            Mock<ITeamRepository> mockedTeamRepo = new Mock<ITeamRepository>();
            Mock<IStadiumRepository> mockedStadiumRepo = new Mock<IStadiumRepository>();
            Mock<ILeagueRepository> mockedLeagueRepo = new Mock<ILeagueRepository>();
            List<Team> teams = new List<Team>()
            {
                new Team() { AwayShirt = "Red", HomeShirt = "Blue", Id = 20, LeagueId = 2, LeaguesWon = 20, Name = "Test Team", StadiumId = 20, YearOfEstablishment = 1920 },
                new Team() { AwayShirt = "Green", HomeShirt = "White", Id = 20, LeagueId = 4, LeaguesWon = 0, Name = "Test Team2", StadiumId = 25, YearOfEstablishment = 1820 },
            };
            mockedTeamRepo.Setup(repo => repo.GetAll()).Returns(teams.AsQueryable());
            StatisticalLogic statisticalLogic = new StatisticalLogic(mockedPlayerRepo.Object, mockedLeagueRepo.Object, mockedStadiumRepo.Object, mockedTeamRepo.Object);

            statisticalLogic.GetAllTeams();

            mockedTeamRepo.Verify(repo => repo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Test for the GetOne merhod.
        /// </summary>
        [Test]
        public void StatisticalLogic_WhenGetOneLeagueIsCalled_ItCallsGetOneOnce()
        {
            Mock<IPlayerRepository> mockedPlayerRepo = new Mock<IPlayerRepository>();
            Mock<ITeamRepository> mockedTeamRepo = new Mock<ITeamRepository>();
            Mock<IStadiumRepository> mockedStadiumRepo = new Mock<IStadiumRepository>();
            Mock<ILeagueRepository> mockedLeagueRepo = new Mock<ILeagueRepository>();
            StatisticalLogic statisticalLogic = new StatisticalLogic(mockedPlayerRepo.Object, mockedLeagueRepo.Object, mockedStadiumRepo.Object, mockedTeamRepo.Object);

            statisticalLogic.GetOneLeague(20);

            mockedLeagueRepo.Verify(repo => repo.GetOne(20), Times.Once);
        }
    }
}
