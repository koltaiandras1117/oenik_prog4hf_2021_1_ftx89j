﻿// <copyright file="FinancialLogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using FootballDatabase.Data;
    using FootballDatabase.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Test class for Financial Logic methods.
    /// </summary>
    [TestFixture]
    public class FinancialLogicTests
    {
        /// <summary>
        /// Test for the LeagueAverage updating method.
        /// </summary>
        [Test]
        public void FinancialLogic_WhenChangeLeagueAverageValueIsCalled_ItCallsChangeAverageValueOnce()
        {
            Mock<IPlayerRepository> mockedPlayerRepo = new Mock<IPlayerRepository>();
            Mock<ITeamRepository> mockedTeamRepo = new Mock<ITeamRepository>();
            Mock<ILeagueRepository> mockedLeagueRepo = new Mock<ILeagueRepository>();
            FinancialLogic financialLogic = new FinancialLogic(mockedPlayerRepo.Object, mockedTeamRepo.Object, mockedLeagueRepo.Object);

            financialLogic.ChangeLeagueAverageValue(10, 10);

            mockedLeagueRepo.Verify(repo => repo.ChangeAverageValue(10, 10), Times.Once);
        }

        /// <summary>
        /// Test for the BelowAverage method.
        /// </summary>
        [Test]
        public void FinancialLogic_WhenBelowLeagueAverageIsCalledWithRandomValues_ItReturnsEveryTeamBelowTheLeaguesAverageValue()
        {
            Mock<IPlayerRepository> mockedPlayerRepo = new Mock<IPlayerRepository>();
            Mock<ITeamRepository> mockedTeamRepo = new Mock<ITeamRepository>();
            Mock<ILeagueRepository> mockedLeagueRepo = new Mock<ILeagueRepository>();
            FinancialLogic financialLogic = new FinancialLogic(mockedPlayerRepo.Object, mockedTeamRepo.Object, mockedLeagueRepo.Object);
            List<League> leagues = new List<League>()
            {
                new League() { Country = "England", Division = 1, Id = 1, Name = "Premier League", Season = "2018-2019" },
                new League() { Country = "Germany", Division = 1, Id = 2, Name = "Bundesliga", Season = "2019-2020" },
                new League() { Country = "Hungary", Division = 1, Id = 3, Name = "NB1", Season = "2013-2014" },
            };
            List<Team> teams = new List<Team>();
            for (int i = 0; i < leagues.Count; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    Team t = new Team()
                    {
                        AwayShirt = It.IsAny<string>(),
                        HomeShirt = It.IsAny<string>(),
                        Id = (i * 10) + j + 1,
                        LeagueId = i + 1,
                        LeaguesWon = It.IsAny<int>(),
                        Name = It.IsAny<string>(),
                        StadiumId = It.IsAny<int>(),
                    };
                    teams.Add(t);
                }
            }

            mockedTeamRepo.Setup(repo => repo.GetAll()).Returns(teams.AsQueryable());
            List<Player> players = new List<Player>();
            int[] randomPlayerValues = new int[300];
            Random rnd = new Random();
            int[] teamValues = new int[30];
            for (int i = 0; i < teams.Count; i++)
            {
                int teamValue = 0;
                for (int j = 0; j < 10; j++)
                {
                    randomPlayerValues[(i * 10) + j] = rnd.Next(500000, 100000000);
                    Player p = new Player()
                    {
                        Assists = It.IsAny<int>(),
                        DateOfBirth = It.IsAny<DateTime>(),
                        Goals = It.IsAny<int>(),
                        Id = (i * 10) + j + 1,
                        MarketValue = randomPlayerValues[(i * 10) + j],
                        Matches = It.IsAny<int>(),
                        Name = It.IsAny<string>(),
                        Nationality = It.IsAny<string>(),
                        Number = It.IsAny<int>(),
                        Position = It.IsAny<string>(),
                        TeamId = i + 1,
                    };
                    players.Add(p);
                    teamValue += p.MarketValue;
                }

                teamValues[i] = teamValue;
            }

            mockedPlayerRepo.Setup(repo => repo.GetAll()).Returns(players.AsQueryable());
            int firstLeagueValue = 0;
            for (int i = 0; i < teams.Count / 3; i++)
            {
                firstLeagueValue += teamValues[i];
            }

            int firstLeagueAverageValue = firstLeagueValue / 10;
            List<int> firstLeagueBelowLeagueAverageTeamsId = new List<int>();
            for (int i = 0; i < teams.Count / 3; i++)
            {
                if (teamValues[i] / 10 < firstLeagueAverageValue)
                {
                    firstLeagueBelowLeagueAverageTeamsId.Add(teams[i].Id);
                }
            }

            int secondLeagueValue = 0;
            for (int i = 10; i < 2 * (teams.Count / 3); i++)
            {
                secondLeagueValue += teamValues[i];
            }

            int secondLeagueAverageValue = secondLeagueValue / 10;
            List<int> secondLeagueBelowLeagueAverageTeamsId = new List<int>();
            for (int i = 10; i < 2 * (teams.Count / 3); i++)
            {
                if (teamValues[i] / 10 < secondLeagueAverageValue)
                {
                    secondLeagueBelowLeagueAverageTeamsId.Add(teams[i].Id);
                }
            }

            int thirdLeagueValue = 0;
            for (int i = 20; i < 3 * (teams.Count / 3); i++)
            {
                thirdLeagueValue += teamValues[i];
            }

            int thirdLeagueAverageValue = thirdLeagueValue / 10;
            List<int> thirdLeagueBelowLeagueAverageTeamsId = new List<int>();
            for (int i = 20; i < 3 * (teams.Count / 3); i++)
            {
                if (teamValues[i] / 10 < thirdLeagueAverageValue)
                {
                    thirdLeagueBelowLeagueAverageTeamsId.Add(teams[i].Id);
                }
            }

            leagues[0].AverageValue = firstLeagueAverageValue;
            leagues[1].AverageValue = secondLeagueAverageValue;
            leagues[2].AverageValue = thirdLeagueAverageValue;
            mockedLeagueRepo.Setup(repo => repo.GetAll()).Returns(leagues.AsQueryable());

            var result = financialLogic.BelowLeagueAverage();
            List<int> firstResults = new List<int>();
            foreach (var item in result)
            {
                if (item.LeagueId == 1)
                {
                    firstResults.Add(item.TeamId);
                }
            }

            List<int> secondResults = new List<int>();
            foreach (var item in result)
            {
                if (item.LeagueId == 2)
                {
                    secondResults.Add(item.TeamId);
                }
            }

            List<int> thirdResults = new List<int>();
            foreach (var item in result)
            {
                if (item.LeagueId == 3)
                {
                    thirdResults.Add(item.TeamId);
                }
            }

            Assert.That(firstResults, Is.EquivalentTo(firstLeagueBelowLeagueAverageTeamsId));
            Assert.That(secondResults, Is.EquivalentTo(secondLeagueBelowLeagueAverageTeamsId));
            Assert.That(thirdResults, Is.EquivalentTo(thirdLeagueBelowLeagueAverageTeamsId));
        }
    }
}
