﻿// <copyright file="TeamRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using FootballDatabase.Data;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Implements the ITeamRepository interface for CRUD operations on the Team class.
    /// </summary>
    public class TeamRepository : RepositoryClass<Team>, ITeamRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TeamRepository"/> class.
        /// </summary>
        /// <param name="ctx">DbContext type parameterer that initializes the ctx attribute.</param>
        public TeamRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// Method that changes the value of the Name property of a Team object.
        /// </summary>
        /// <param name="id">The primary key of a Team object.</param>
        /// <param name="newValue">The new value that will be inserted to the Name property of a Team object.</param>
        public void ChangeName(int id, string newValue)
        {
            var team = this.GetOne(id);
            if (team == null)
            {
                throw new InvalidOperationException("Team not found with the given key.");
            }

            team.Name = newValue;
            this.GetCtx().SaveChanges();
        }

        /// <summary>
        /// Method that finds a Team element in the DbContext by its key.
        /// </summary>
        /// <param name="id">Key that identifies the object.</param>
        /// <returns>Returns the Team element with the matching key.</returns>
        public override Team GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.Id == id);
        }
    }
}
