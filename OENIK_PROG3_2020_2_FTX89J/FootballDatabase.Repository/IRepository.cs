﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using FootballDatabase.Data;

    /// <summary>
    /// Interface for generic CRUD operations.
    /// </summary>
    /// <typeparam name="T">A generic parameter which can only be a class.</typeparam>
    public interface IRepository<T>
        where T : class
    {
        /// <summary>
        /// Method that finds an element in a given class by its key.
        /// </summary>
        /// <param name="id">Key that identifies the object.</param>
        /// <returns>Returns the element with the matching key.</returns>
        T GetOne(int id);

        /// <summary>
        /// Method for getting all T type elements in a database.
        /// </summary>
        /// <returns>Returns all T class elements in a set that implements the IQueryable interface. </returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// Inserts a T class element into the database.
        /// </summary>
        /// <param name="entity">The element that will be inserted into the database.</param>
        void Insert(T entity);

        /// <summary>
        /// Deletes a T type entity from the database.
        /// </summary>
        /// <param name="entity">The entity that needs to be removed.</param>
        /// <returns>Returns true if the entity exists in the database.</returns>
        bool Remove(T entity);
    }
}
