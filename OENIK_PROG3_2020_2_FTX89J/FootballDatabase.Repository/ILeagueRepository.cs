﻿// <copyright file="ILeagueRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using FootballDatabase.Data;

    /// <summary>
    /// Interface for implementing CRUD operations on a League type.
    /// </summary>
    public interface ILeagueRepository : IRepository<League>
    {
        /// <summary>
        /// Method that changes the value of the AverageValue property of a League object.
        /// </summary>
        /// <param name="id">The primary key of a League object.</param>
        /// <param name="newValue">The new value that will be inserted to the AverageValue property of a League object.</param>
        void ChangeAverageValue(int id, int newValue);

        /// <summary>
        /// Method that changes the value of the Name property of a League object.
        /// </summary>
        /// <param name="id">The primary key of a League object.</param>
        /// <param name="newValue">The new value that will be inserted to the Name property of a League object.</param>
        void ChangeName(int id, string newValue);

        /// <summary>
        /// Method that changes the value of the Season property of a League object.
        /// </summary>
        /// <param name="id">The primary key of a League object.</param>
        /// <param name="newValue">The new value that will be inserted to the Season property of a League object.</param>
        void ChangeSeason(int id, string newValue);

        /// <summary>
        /// Method that changes the value of the Country property of a League object.
        /// </summary>
        /// <param name="id">The primary key of a League object.</param>
        /// <param name="newValue">The new value that will be inserted to the Country property of a League object.</param>
        void ChangeCountry(int id, string newValue);

        /// <summary>
        /// Method that changes the value of the Division property of a League object.
        /// </summary>
        /// <param name="id">The primary key of a League object.</param>
        /// <param name="newValue">The new value that will be inserted to the Division property of a League object.</param>
        void ChangeDivision(int id, int newValue);
    }
}
