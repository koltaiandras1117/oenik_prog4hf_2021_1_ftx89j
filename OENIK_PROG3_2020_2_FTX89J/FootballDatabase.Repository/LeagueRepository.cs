﻿// <copyright file="LeagueRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.InteropServices.ComTypes;
    using System.Security.Cryptography.X509Certificates;
    using System.Text;
    using FootballDatabase.Data;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Implements the ILeagueRepository interface for CRUD operations on the League class.
    /// </summary>
    public class LeagueRepository : RepositoryClass<League>, ILeagueRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LeagueRepository"/> class.
        /// </summary>
        /// <param name="ctx">DbContext type parameterer that initializes the ctx attribute.</param>
        public LeagueRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// Method that changes the value of the AverageValue property of a League object.
        /// </summary>
        /// <param name="id">The primary key of a League object.</param>
        /// <param name="newValue">The new value that will be inserted to the AverageValue property of a League object.</param>
        public void ChangeAverageValue(int id, int newValue)
        {
            var league = this.GetOne(id);
            if (league == null)
            {
                throw new InvalidOperationException("League not found with the given key.");
            }

            league.AverageValue = newValue;
            this.GetCtx().SaveChanges();
        }

        /// <summary>
        /// Method that changes the value of the Name property of a League object.
        /// </summary>
        /// <param name="id">The primary key of a League object.</param>
        /// <param name="newValue">The new value that will be inserted to the Name property of a League object.</param>
        public void ChangeName(int id, string newValue)
        {
            var league = this.GetOne(id);
            if (league == null)
            {
                throw new InvalidOperationException("League not found with the given key.");
            }

            league.Name = newValue;
            this.GetCtx().SaveChanges();
        }

        /// <summary>
        /// Method that changes the value of the Season property of a League object.
        /// </summary>
        /// <param name="id">The primary key of a League object.</param>
        /// <param name="newValue">The new value that will be inserted to the Season property of a League object.</param>
        public void ChangeSeason(int id, string newValue)
        {
            var league = this.GetOne(id);
            if (league == null)
            {
                throw new InvalidOperationException("League not found with the given key.");
            }

            league.Season = newValue;
            this.GetCtx().SaveChanges();
        }

        /// <summary>
        /// Method that changes the value of the Country property of a League object.
        /// </summary>
        /// <param name="id">The primary key of a League object.</param>
        /// <param name="newValue">The new value that will be inserted to the Country property of a League object.</param>
        public void ChangeCountry(int id, string newValue)
        {
            var league = this.GetOne(id);
            if (league == null)
            {
                throw new InvalidOperationException("League not found with the given key.");
            }

            league.Country = newValue;
            this.GetCtx().SaveChanges();
        }

        /// <summary>
        /// Method that changes the value of the Division property of a League object.
        /// </summary>
        /// <param name="id">The primary key of a League object.</param>
        /// <param name="newValue">The new value that will be inserted to the Division property of a League object.</param>
        public void ChangeDivision(int id, int newValue)
        {
            var league = this.GetOne(id);
            if (league == null)
            {
                throw new InvalidOperationException("League not found with the given key.");
            }

            league.Division = newValue;
            this.GetCtx().SaveChanges();
        }

        /// <summary>
        /// Method that finds a League element in the DbContext by its key.
        /// </summary>
        /// <param name="id">Key that identifies the object.</param>
        /// <returns>Returns the League element with the matching key.</returns>
        public override League GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.Id == id);
        }
    }
}
