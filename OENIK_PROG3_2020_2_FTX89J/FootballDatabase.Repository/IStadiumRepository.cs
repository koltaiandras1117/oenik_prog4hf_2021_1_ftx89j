﻿// <copyright file="IStadiumRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using FootballDatabase.Data;

    /// <summary>
    /// Interface for implementing CRUD operations on a Stadium type.
    /// </summary>
    public interface IStadiumRepository : IRepository<Stadium>
    {
        /// <summary>
        /// Method that changes the value of the Name property of a Stadium object.
        /// </summary>
        /// <param name="id">The primary key of a Stadium object.</param>
        /// <param name="newValue">The new value that will be inserted to the Name property of a Stadium object.</param>
        void ChangeName(int id, string newValue);

        /// <summary>
        /// Method that changes the value of the Capacity property of a Stadium object.
        /// </summary>
        /// <param name="id">The primary key of a Stadium object.</param>
        /// <param name="newValue">The new value that will be inserted to the Capacity property of a Stadium object.</param>
        void ChangeCapacity(int id, int newValue);

        /// <summary>
        /// Method that changes the value of the Phone property of a Stadium object.
        /// </summary>
        /// <param name="id">The primary key of a Stadium object.</param>
        /// <param name="newValue">The new value that will be inserted to the Phone property of a Stadium object.</param>
        void ChangePhone(int id, string newValue);
    }
}
