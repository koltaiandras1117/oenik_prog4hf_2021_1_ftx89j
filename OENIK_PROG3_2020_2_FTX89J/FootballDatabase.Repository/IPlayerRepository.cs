﻿// <copyright file="IPlayerRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using FootballDatabase.Data;

    /// <summary>
    /// Interface for implementing CRUD operations on a Player type.
    /// </summary>
    public interface IPlayerRepository : IRepository<Player>
    {
        /// <summary>
        /// Method that changes the value of the Number property of a Player object.
        /// </summary>
        /// <param name="id">The primary key of a Player object.</param>
        /// <param name="newValue">The new value that will be inserted to the Number property of a Player object.</param>
        void ChangeNumber(int id, int newValue);

        /// <summary>
        /// Method that changes the value of the Goals property of a Player object.
        /// </summary>
        /// <param name="id">The primary key of a Player object.</param>
        /// <param name="newValue">The new value that will be inserted to the Goals property of a Player object.</param>
        void ChangeGoals(int id, int newValue);

        /// <summary>
        /// Method that changes the value of the Assists property of a Player object.
        /// </summary>
        /// <param name="id">The primary key of a Player object.</param>
        /// <param name="newValue">The new value that will be inserted to the Assists property of a Player object.</param>
        void ChangeAssists(int id, int newValue);

        /// <summary>
        /// Method that changes the value of the Matches property of a Player object.
        /// </summary>
        /// <param name="id">The primary key of a Player object.</param>
        /// <param name="newValue">The new value that will be inserted to the Matches property of a Player object.</param>
        void ChangeMatches(int id, int newValue);

        /// <summary>
        /// Method that changes the value of the MarketValue property of a Player object.
        /// </summary>
        /// <param name="id">The primary key of a Player object.</param>
        /// <param name="newValue">The new value that will be inserted to the MarketValue property of a Player object.</param>
        void ChangeMarketValue(int id, int newValue);

        /// <summary>
        /// Method that changes the value of the TeamId property of a Player object.
        /// </summary>
        /// <param name="id">The primary key of a Player object.</param>
        /// <param name="newValue">The new value that will be inserted to the TeamId property of a Player object.</param>
        void ChangeTeamId(int id, int newValue);

        /// <summary>
        /// Method that changes the value of the Name property of a Player object.
        /// </summary>
        /// <param name="id">The primary key of a Player object.</param>
        /// <param name="newValue">The new value that will be inserted to the Name property of a Player object.</param>
        void ChangeName(int id, string newValue);

        /// <summary>
        /// Method that changes the value of the DateOfBirth property of a Player object.
        /// </summary>
        /// <param name="id">The primary key of a Player object.</param>
        /// <param name="newValue">The new value that will be inserted to the DateOfBirth property of a Player object.</param>
        void ChangeBirthday(int id, DateTime newValue);

        /// <summary>
        /// Method that changes the value of the Nationality property of a Player object.
        /// </summary>
        /// <param name="id">The primary key of a Player object.</param>
        /// <param name="newValue">The new value that will be inserted to the Nationality property of a Player object.</param>
        void ChangeNationality(int id, string newValue);

        /// <summary>
        /// Method that changes the value of the Position property of a Player object.
        /// </summary>
        /// <param name="id">The primary key of a Player object.</param>
        /// <param name="newValue">The new value that will be inserted to the Position property of a Player object.</param>
        void ChangePosition(int id, string newValue);
    }
}
