﻿// <copyright file="ITeamRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using FootballDatabase.Data;

    /// <summary>
    /// Interface for implementing CRUD operations on a Team type.
    /// </summary>
    public interface ITeamRepository : IRepository<Team>
    {
        /// <summary>
        /// Method that changes the value of the Name property of a Team object.
        /// </summary>
        /// <param name="id">The primary key of a Team object.</param>
        /// <param name="newValue">The new value that will be inserted to the Name property of a Team object.</param>
        public void ChangeName(int id, string newValue);
    }
}
