﻿// <copyright file="StadiumRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using FootballDatabase.Data;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Implements the IStadiumRepository interface for CRUD operations on the Stadium class.
    /// </summary>
    public class StadiumRepository : RepositoryClass<Stadium>, IStadiumRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StadiumRepository"/> class.
        /// </summary>
        /// <param name="ctx">DbContext type parameterer that initializes the ctx attribute.</param>
        public StadiumRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// Method that changes the value of the Capacity property of a Stadium object.
        /// </summary>
        /// <param name="id">The primary key of a Stadium object.</param>
        /// <param name="newValue">The new value that will be inserted to the Capacity property of a Stadium object.</param>
        public void ChangeCapacity(int id, int newValue)
        {
            var stadium = this.GetOne(id);
            if (stadium == null)
            {
                throw new InvalidOperationException("Stadium not found with the given key.");
            }

            stadium.Capacity = newValue;
            this.GetCtx().SaveChanges();
        }

        /// <summary>
        /// Method that changes the value of the Name property of a Stadium object.
        /// </summary>
        /// <param name="id">The primary key of a Stadium object.</param>
        /// <param name="newValue">The new value that will be inserted to the Name property of a Stadium object.</param>
        public void ChangeName(int id, string newValue)
        {
            var stadium = this.GetOne(id);
            if (stadium == null)
            {
                throw new InvalidOperationException("Stadium not found with the given key.");
            }

            stadium.Name = newValue;
            this.GetCtx().SaveChanges();
        }

        /// <summary>
        /// Method that changes the value of the Phone property of a Stadium object.
        /// </summary>
        /// <param name="id">The primary key of a Stadium object.</param>
        /// <param name="newValue">The new value that will be inserted to the Phone property of a Stadium object.</param>
        public void ChangePhone(int id, string newValue)
        {
            var stadium = this.GetOne(id);
            if (stadium == null)
            {
                throw new InvalidOperationException("Stadium not found with the given key.");
            }

            stadium.Phone = newValue;
            this.GetCtx().SaveChanges();
        }

        /// <summary>
        /// Method that finds a Stadium element in the DbContext by its key.
        /// </summary>
        /// <param name="id">Key that identifies the object.</param>
        /// <returns>Returns the Stadium element with the matching key.</returns>
        public override Stadium GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.Id == id);
        }
    }
}
