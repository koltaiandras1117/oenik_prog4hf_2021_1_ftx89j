﻿// <copyright file="PlayerRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using FootballDatabase.Data;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Implements the IPlayerRepository interface for CRUD operations on the Player class.
    /// </summary>
    public class PlayerRepository : RepositoryClass<Player>, IPlayerRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerRepository"/> class.
        /// </summary>
        /// <param name="ctx">DbContext type parameterer that initializes the ctx attribute.</param>
        public PlayerRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// Method that changes the value of the Assists property of a Player object.
        /// </summary>
        /// <param name="id">The primary key of a Player object.</param>
        /// <param name="newValue">The new value that will be inserted to the Assists property of a Player object.</param>
        public void ChangeAssists(int id, int newValue)
        {
            var player = this.GetOne(id);
            if (player == null)
            {
                throw new InvalidOperationException("Player not found with the given key.");
            }

            player.Assists = newValue;
            this.GetCtx().SaveChanges();
        }

        /// <summary>
        /// Method that changes the value of the Goals property of a Player object.
        /// </summary>
        /// <param name="id">The primary key of a Player object.</param>
        /// <param name="newValue">The new value that will be inserted to the Goals property of a Player object.</param>
        public void ChangeGoals(int id, int newValue)
        {
            var player = this.GetOne(id);
            if (player == null)
            {
                throw new InvalidOperationException("Player not found with the given key.");
            }

            player.Goals = newValue;
            this.GetCtx().SaveChanges();
        }

        /// <summary>
        /// Method that changes the value of the MarketValue property of a Player object.
        /// </summary>
        /// <param name="id">The primary key of a Player object.</param>
        /// <param name="newValue">The new value that will be inserted to the MarketValue property of a Player object.</param>
        public void ChangeMarketValue(int id, int newValue)
        {
            var player = this.GetOne(id);
            if (player == null)
            {
                throw new InvalidOperationException("Player not found with the given key.");
            }

            player.MarketValue = newValue;
            this.GetCtx().SaveChanges();
        }

        /// <summary>
        /// Method that changes the value of the Matches property of a Player object.
        /// </summary>
        /// <param name="id">The primary key of a Player object.</param>
        /// <param name="newValue">The new value that will be inserted to the Matches property of a Player object.</param>
        public void ChangeMatches(int id, int newValue)
        {
            var player = this.GetOne(id);
            if (player == null)
            {
                throw new InvalidOperationException("Player not found with the given key.");
            }

            player.Matches = newValue;
            this.GetCtx().SaveChanges();
        }

        /// <summary>
        /// Method that changes the value of the Number property of a Player object.
        /// </summary>
        /// <param name="id">The primary key of a Player object.</param>
        /// <param name="newValue">The new value that will be inserted to the Number property of a Player object.</param>
        public void ChangeNumber(int id, int newValue)
        {
            var player = this.GetOne(id);
            if (player == null)
            {
                throw new InvalidOperationException("Player not found with the given key.");
            }

            player.Number = newValue;
            this.GetCtx().SaveChanges();
        }

        /// <summary>
        /// Method that changes the value of the TeamId property of a Player object.
        /// </summary>
        /// <param name="id">The primary key of a Player object.</param>
        /// <param name="newValue">The new value that will be inserted to the TeamId property of a Player object.</param>
        public void ChangeTeamId(int id, int newValue)
        {
            var player = this.GetOne(id);
            if (player == null)
            {
                throw new InvalidOperationException("Player not found with the given key.");
            }

            player.TeamId = newValue;
            this.GetCtx().SaveChanges();
        }

        /// <summary>
        /// Method that changes the value of the Name property of a Player object.
        /// </summary>
        /// <param name="id">The primary key of a Player object.</param>
        /// <param name="newValue">The new value that will be inserted to the Name property of a Player object.</param>
        public void ChangeName(int id, string newValue)
        {
            var player = this.GetOne(id);
            if (player == null)
            {
                throw new InvalidOperationException("Player not found with the given key.");
            }

            player.Name = newValue;
            this.GetCtx().SaveChanges();
        }

        /// <summary>
        /// Method that changes the value of the DateOfBirth property of a Player object.
        /// </summary>
        /// <param name="id">The primary key of a Player object.</param>
        /// <param name="newValue">The new value that will be inserted to the DateOfBirth property of a Player object.</param>
        public void ChangeBirthday(int id, DateTime newValue)
        {
            var player = this.GetOne(id);
            if (player == null)
            {
                throw new InvalidOperationException("Player not found with the given key.");
            }

            player.DateOfBirth = newValue;
            this.GetCtx().SaveChanges();
        }

        /// <summary>
        /// Method that changes the value of the Nationality property of a Player object.
        /// </summary>
        /// <param name="id">The primary key of a Player object.</param>
        /// <param name="newValue">The new value that will be inserted to the Nationality property of a Player object.</param>
        public void ChangeNationality(int id, string newValue)
        {
            var player = this.GetOne(id);
            if (player == null)
            {
                throw new InvalidOperationException("Player not found with the given key.");
            }

            player.Nationality = newValue;
            this.GetCtx().SaveChanges();
        }

        /// <summary>
        /// Method that changes the value of the Position property of a Player object.
        /// </summary>
        /// <param name="id">The primary key of a Player object.</param>
        /// <param name="newValue">The new value that will be inserted to the Position property of a Player object.</param>
        public void ChangePosition(int id, string newValue)
        {
            var player = this.GetOne(id);
            if (player == null)
            {
                throw new InvalidOperationException("Player not found with the given key.");
            }

            player.Position = newValue;
            this.GetCtx().SaveChanges();
        }

        /// <summary>
        /// Method that finds a Player element in the DbContext by its key.
        /// </summary>
        /// <param name="id">Key that identifies the object.</param>
        /// <returns>Returns the Player element with the matching key.</returns>
        public override Player GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.Id == id);
        }
    }
}
