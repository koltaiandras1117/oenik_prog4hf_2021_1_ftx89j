﻿// <copyright file="RepositoryClass.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Castle.DynamicProxy.Contributors;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Implements the IRepository interface for generic CRUD operations.
    /// </summary>
    /// <typeparam name="T">A generic parameter which can only be a class.</typeparam>
    public abstract class RepositoryClass<T> : IRepository<T>
        where T : class
    {
        /// <summary>
        /// DbContext object where the CRUD operations are used.
        /// </summary>
        private DbContext ctx;

        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryClass{T}"/> class.
        /// </summary>
        /// <param name="ctx">DbContext type parameterer that initializes the ctx attribute.</param>
        protected RepositoryClass(DbContext ctx)
        {
            this.ctx = ctx;
        }

        /// <summary>
        /// Getter for DbContext ctx.
        /// </summary>
        /// <returns>Returns the value of DbContext ctx.</returns>
        public DbContext GetCtx()
        {
            return this.ctx;
        }

        /// <summary>
        /// Setter for DbContext ctx.
        /// </summary>
        /// <param name="value">Sets the DbContext ctx as value.</param>
        public void SetCtx(DbContext value)
        {
            this.ctx = value;
        }

        /// <summary>
        /// Method for getting all T type elements in a database.
        /// </summary>
        /// <returns>Returns all T class elements in a set that implements the IQueryable interface. </returns>
        public IQueryable<T> GetAll()
        {
            return this.ctx.Set<T>();
        }

        /// <summary>
        /// Method that finds an element in a given class by its key.
        /// </summary>
        /// <param name="id">Key that identifies the object.</param>
        /// <returns>Returns the element with the matching key.</returns>
        public abstract T GetOne(int id);

        /// <summary>
        /// Inserts a T class element into the database.
        /// </summary>
        /// <param name="entity">The element that will be inserted into the database.</param>
        public void Insert(T entity)
        {
            this.ctx.Add(entity);
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// Deletes a T type entity from the database.
        /// </summary>
        /// <param name="entity">The entity that needs to be removed.</param>
        /// <returns>Returns true if the entity exists in the database.</returns>
        public bool Remove(T entity)
        {
            if (!this.ctx.Set<T>().Local.Any(e => e == entity))
            {
                return false;
            }

            this.ctx.Remove(entity);
            this.ctx.SaveChanges();
            return true;
        }
    }
}
