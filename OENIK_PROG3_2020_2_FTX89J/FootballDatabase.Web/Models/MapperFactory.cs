﻿// <copyright file="MapperFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;

    /// <summary>
    /// Class for creating Mapper instance.
    /// </summary>
    public static class MapperFactory
    {
        /// <summary>
        /// Method for creating a Mapper instance.
        /// </summary>
        /// <returns>The created mapper instance.</returns>
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
              {
                  cfg.CreateMap<Data.Player, Web.Models.Player>().
                  ForMember(dest => dest.Id, map => map.MapFrom(src => src.Id)).
                  ForMember(dest => dest.Assists, map => map.MapFrom(src => src.Assists)).
                  ForMember(dest => dest.DateOfBirth, map => map.MapFrom(src => src.DateOfBirth)).
                  ForMember(dest => dest.Goals, map => map.MapFrom(src => src.Goals)).
                  ForMember(dest => dest.MarketValue, map => map.MapFrom(src => src.MarketValue)).
                  ForMember(dest => dest.Matches, map => map.MapFrom(src => src.Matches)).
                  ForMember(dest => dest.Name, map => map.MapFrom(src => src.Name)).
                  ForMember(dest => dest.Nationality, map => map.MapFrom(src => src.Nationality)).
                  ForMember(dest => dest.Number, map => map.MapFrom(src => src.Number)).
                  ForMember(dest => dest.Position, map => map.MapFrom(src => src.Position)).
                  ForMember(dest => dest.Team, map => map.MapFrom(src => src.Team == null ? string.Empty : src.Team.Name));
              });
            return config.CreateMapper();
        }
    }
}
