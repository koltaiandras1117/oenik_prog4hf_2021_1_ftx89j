﻿// <copyright file="Player.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Model class representing a football player.
    /// </summary>
    public class Player
    {
        /// <summary>
        /// Gets or sets the id of a player.
        /// </summary>
        [Display(Name="Player Id")]
        [Required]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the player's name.
        /// </summary>
        [Display(Name = "Player Name")]
        [Required]
        [MaxLength(60)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the player's birthday.
        /// </summary>
        [Display(Name = "Player Birthday")]
        [Required]
        public DateTime DateOfBirth { get; set; }

        /// <summary>
        /// Gets or sets the player's position.
        /// </summary>
        [Display(Name = "Player Position")]
        [Required]
        public string Position { get; set; }

        /// <summary>
        /// Gets or sets the number on the player's shirt.
        /// </summary>
        [Display(Name = "Player Number")]
        [Required]
        public int Number { get; set; }

        /// <summary>
        /// Gets or sets how many goals the player has scored in the current season.
        /// </summary>
        [Display(Name = "Player Goals")]
        [Required]
        public int Goals { get; set; }

        /// <summary>
        /// Gets or sets how many assists the player has given in the current season.
        /// </summary>
        [Display(Name = "Player Assists")]
        [Required]
        public int Assists { get; set; }

        /// <summary>
        /// Gets or sets how many matches the player has played in the current season.
        /// </summary>
        [Display(Name = "Player Matches")]
        [Required]
        public int Matches { get; set; }

        /// <summary>
        /// Gets or sets the player's nationality.
        /// </summary>
        [Display(Name = "Player Nationality")]
        [Required]
        public string Nationality { get; set; }

        /// <summary>
        /// Gets or sets the player's current market value in euros.
        /// </summary>
        [Display(Name = "Player Marketvalue")]
        public int MarketValue { get; set; }

        /// <summary>
        /// Gets or sets which team the player is playing in.
        /// </summary>
        [Display(Name = "Player Team")]
        public string Team { get; set; }
    }
}
