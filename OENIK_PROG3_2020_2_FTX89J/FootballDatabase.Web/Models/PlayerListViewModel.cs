﻿// <copyright file="PlayerListViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Viewmodel for displaying all players and storing the selected player.
    /// </summary>
    public class PlayerListViewModel
    {
        /// <summary>
        /// Gets or sets the list of all players to display.
        /// </summary>
        public List<Player> ListOfPlayers { get; set; }

        /// <summary>
        /// Gets or sets the Player which is currently edited.
        /// </summary>
        public Player EditedPlayer { get; set; }
    }
}
