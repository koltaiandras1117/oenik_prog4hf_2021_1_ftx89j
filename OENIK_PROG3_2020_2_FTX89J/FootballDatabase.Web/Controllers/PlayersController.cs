﻿// <copyright file="PlayersController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using FootballDatabase.Logic;
    using FootballDatabase.Web.Models;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Player controller class.
    /// </summary>
    public class PlayersController : Controller
    {
        private IStatisticalLogic statisticalLogic;
        private ITransferLogic transferLogic;
        private IFinancialLogic financialLogic;
        private IMapper mapper;
        private PlayerListViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayersController"/> class.
        /// </summary>
        /// <param name="statisticalLogic">IStatisticalLogic DI.</param>
        /// <param name="transferLogic">ITransferLogic DI.</param>
        /// <param name="financialLogic">IFinancialLogic DI.</param>
        /// <param name="mapper">IMapper DI.</param>
        public PlayersController(IStatisticalLogic statisticalLogic, ITransferLogic transferLogic, IFinancialLogic financialLogic, IMapper mapper)
        {
            this.statisticalLogic = statisticalLogic;
            this.transferLogic = transferLogic;
            this.financialLogic = financialLogic;
            this.mapper = mapper;

            this.vm = new PlayerListViewModel();
            this.vm.EditedPlayer = new Models.Player();

            var players = this.statisticalLogic.GetAllPlayers();
            this.vm.ListOfPlayers = this.mapper.Map<IList<Data.Player>, List<Models.Player>>(players);
        }

        /// <summary>
        /// Method for sendig a response of the Index page.
        /// </summary>
        /// <returns>An IAction result.</returns>
        public IActionResult Index()
        {
            this.ViewData["editAction"] = "AddNew";
            return this.View("PlayersIndex", this.vm);
        }

        /// <summary>
        /// Method for getting a player with a certain id.
        /// </summary>
        /// <param name="id">Id of the player.</param>
        /// <returns>IAction result.</returns>
        public IActionResult Details(int id)
        {
            return this.View("PlayersDetails", this.GetPlayer(id));
        }

        /// <summary>
        /// Method for removing a player with a certain Ii.
        /// </summary>
        /// <param name="id">The id of the player.</param>
        /// <returns>IAction result.</returns>
        public IActionResult Remove(int id)
        {
            this.TempData["editResult"] = "Delete failed.";
            if (this.transferLogic.RemovePlayer(this.statisticalLogic.GetOnePlayer(id)))
            {
                this.TempData["editResult"] = "Delete successful.";
            }

            return this.RedirectToAction(nameof(this.Index));
        }

        /// <summary>
        /// Method for editing a player.
        /// </summary>
        /// <param name="id">The player's id.</param>
        /// <returns>IAction result.</returns>
        public IActionResult Edit(int id)
        {
            this.ViewData["editAction"] = "Edit";
            this.vm.EditedPlayer = this.GetPlayer(id);
            return this.View("PlayersIndex", this.vm);
        }

        /// <summary>
        /// Method for editing a player.
        /// </summary>
        /// <param name="player">The edited Player object.</param>
        /// <param name="editAction">Status of edit.</param>
        /// <returns>IAction result.</returns>
        [HttpPost]
        public IActionResult Edit(Models.Player player, string editAction)
        {
            if (this.ModelState.IsValid && player != null)
            {
                int teamId = this.statisticalLogic.GetIdFromTeamName(player.Team);
                this.TempData["editResult"] = "Edit successful.";
                if (editAction == "AddNew")
                {
                    if (teamId != 0)
                    {
                        this.transferLogic.InsertPlayer(new Data.Player()
                        {
                            Assists = player.Assists,
                            DateOfBirth = player.DateOfBirth,
                            Goals = player.Goals,
                            MarketValue = player.MarketValue,
                            Matches = player.Matches,
                            Name = player.Name,
                            Nationality = player.Nationality,
                            Number = player.Number,
                            Position = player.Position,
                            TeamId = teamId,

                            // Team = this.statisticalLogic.GetOneTeam(teamId),
                        });
                    }
                    else
                    {
                        this.TempData["editResult"] = "Insertion failed. Team doesn't exist.";
                    }
                }
                else
                {
                    if (teamId != 0)
                    {
                        this.statisticalLogic.ChangePlayerName(player.Id, player.Name);
                        this.statisticalLogic.ChangePlayerGoals(player.Id, player.Goals);
                        this.statisticalLogic.ChangePlayerMatches(player.Id, player.Goals);
                        this.statisticalLogic.ChangePlayerAssists(player.Id, player.Assists);
                        this.statisticalLogic.ChangePlayerBirthday(player.Id, player.DateOfBirth);
                        this.statisticalLogic.ChangePlayerNationality(player.Id, player.Nationality);
                        this.statisticalLogic.ChangePlayerPosition(player.Id, player.Position);
                        this.statisticalLogic.ChangePlayerNumber(player.Number, player.Number);
                        this.transferLogic.ChangePlayerTeamId(player.Id, teamId);
                        this.financialLogic.ChangePlayerMarketValue(player.Id, player.MarketValue);
                    }
                    else
                    {
                        this.TempData["editResult"] = "Edit failed. Team doesn't exist.";
                    }
                }

                return this.RedirectToAction(nameof(this.Index));
            }
            else
            {
                this.ViewData["editAction"] = "Edit";
                this.vm.EditedPlayer = player;
                return this.View("PlayersIndex", this.vm);
            }
        }

        private Models.Player GetPlayer(int id)
        {
            Data.Player onePlayer = this.statisticalLogic.GetOnePlayer(id);
            return this.mapper.Map<Data.Player, Models.Player>(onePlayer);
        }
    }
}
