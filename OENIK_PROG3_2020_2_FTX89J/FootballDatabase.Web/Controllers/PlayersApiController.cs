﻿// <copyright file="PlayersApiController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using FootballDatabase.Logic;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Playe controller api class.
    /// </summary>
    public class PlayersApiController : Controller
    {
        private IFinancialLogic financialLogic;
        private IStatisticalLogic statisticalLogic;
        private ITransferLogic transferLogic;
        private IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayersApiController"/> class.
        /// </summary>
        /// <param name="financialLogic">FinancialLogic DI.</param>
        /// <param name="statisticalLogic">StatisticalLogic DI.</param>
        /// <param name="transferLogic">TransferLogic DI.</param>
        /// <param name="mapper">Mapper DI.</param>
        public PlayersApiController(IFinancialLogic financialLogic, IStatisticalLogic statisticalLogic, ITransferLogic transferLogic, IMapper mapper)
        {
            this.financialLogic = financialLogic;
            this.statisticalLogic = statisticalLogic;
            this.transferLogic = transferLogic;
            this.mapper = mapper;
        }

        /// <summary>
        /// Api method for getting all Players from the database.
        /// </summary>
        /// <returns>IEnumerable list of the players.</returns>
        [HttpGet]
        [ActionName("all")]
        public IEnumerable<Models.Player> GetAll()
        {
            var players = this.statisticalLogic.GetAllPlayers();
            return this.mapper.Map<IList<Data.Player>, List<Models.Player>>(players);
        }

        /// <summary>
        /// Api method for deleting a player.
        /// </summary>
        /// <param name="id">Id of the player.</param>
        /// <returns>ApiResult indicating whether the deletion was successful or not.</returns>
        [HttpGet]
        [ActionName("del")]
        public ApiResult DeleteOnePlayer(int id)
        {
            return new ApiResult() { OperationResult = this.transferLogic.RemovePlayer(this.statisticalLogic.GetOnePlayer(id)) };
        }

        /// <summary>
        /// Api method for inserting a player.
        /// </summary>
        /// <param name="player">The player that will be added to the database.</param>
        /// <returns>ApiResult indicating whether the insertion was successful or not.</returns>
        [HttpPost]
        [ActionName("add")]
        public ApiResult AddOnePlayer(Models.Player player)
        {
            int teamId = this.statisticalLogic.GetIdFromTeamName(player.Team);
            if (teamId != 0)
            {
                this.transferLogic.InsertPlayer(new Data.Player()
                {
                    Assists = player.Assists,
                    DateOfBirth = player.DateOfBirth,
                    Goals = player.Goals,
                    MarketValue = player.MarketValue,
                    Matches = player.Matches,
                    Name = player.Name,
                    Nationality = player.Nationality,
                    Number = player.Number,
                    Position = player.Position,
                    TeamId = teamId,
                });
                return new ApiResult() { OperationResult = true };
            }
            else
            {
                return new ApiResult() { OperationResult = false };
            }
        }

        /// <summary>
        /// Api method for modifying an existing player.
        /// </summary>
        /// <param name="player">The player that will be modified.</param>
        /// <returns>ApiResult indicating whether the modification was successful or not.</returns>
        [HttpPost]
        [ActionName("mod")]
        public ApiResult ModifyOnePlayer(Models.Player player)
        {
            int teamId = this.statisticalLogic.GetIdFromTeamName(player.Team);
            if (teamId != 0)
            {
                this.statisticalLogic.ChangePlayerName(player.Id, player.Name);
                this.statisticalLogic.ChangePlayerGoals(player.Id, player.Goals);
                this.statisticalLogic.ChangePlayerMatches(player.Id, player.Goals);
                this.statisticalLogic.ChangePlayerAssists(player.Id, player.Assists);
                this.statisticalLogic.ChangePlayerBirthday(player.Id, player.DateOfBirth);
                this.statisticalLogic.ChangePlayerNationality(player.Id, player.Nationality);
                this.statisticalLogic.ChangePlayerPosition(player.Id, player.Position);
                this.statisticalLogic.ChangePlayerNumber(player.Number, player.Number);
                this.transferLogic.ChangePlayerTeamId(player.Id, teamId);
                this.financialLogic.ChangePlayerMarketValue(player.Id, player.MarketValue);
                return new ApiResult() { OperationResult = true };
            }
            else
            {
                return new ApiResult() { OperationResult = false };
            }
        }
    }
}
