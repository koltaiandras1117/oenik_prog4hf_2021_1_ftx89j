﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("StyleCop.CSharp.NamingRules", "SA1309:Field names should not begin with underscore", Justification = "<NikGitStats>", Scope = "member", Target = "~F:FootballDatabase.Web.Controllers.HomeController._logger")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "<NikGitStats>", Scope = "member", Target = "~P:FootballDatabase.Web.Models.PlayerListViewModel.ListOfPlayers")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "<NikGitStats>", Scope = "member", Target = "~P:FootballDatabase.Web.Models.PlayerListViewModel.ListOfPlayers")]
[assembly: SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "<NikGitStats>", Scope = "member", Target = "~M:FootballDatabase.Web.Controllers.PlayersController.Edit(FootballDatabase.Web.Models.Player,System.String)~Microsoft.AspNetCore.Mvc.IActionResult")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<NikGitStats>", Scope = "member", Target = "~M:FootballDatabase.Web.Controllers.PlayersApiController.AddOnePlayer(FootballDatabase.Web.Models.Player)~FootballDatabase.Web.ApiResult")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<NikGitStats>", Scope = "member", Target = "~M:FootballDatabase.Web.Controllers.PlayersApiController.ModifyOnePlayer(FootballDatabase.Web.Models.Player)~FootballDatabase.Web.ApiResult")]
