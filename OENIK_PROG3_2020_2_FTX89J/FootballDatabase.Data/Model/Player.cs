﻿// <copyright file="Player.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Globalization;
    using System.Text;

    /// <summary>
    /// A class that contains informations of a player.
    /// </summary>
    public class Player
    {
        /// <summary>
        /// Gets or sets the primary key of a player.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the player's name.
        /// </summary>
        [Required]
        [MaxLength(60)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the player's birthday.
        /// </summary>
        [Required]
        public DateTime DateOfBirth { get; set; }

        /// <summary>
        /// Gets or sets which position the player is playing at.
        /// </summary>
        [Required]
        public string Position { get; set; }

        /// <summary>
        /// Gets or sets the number on the player's shirt.
        /// </summary>
        [Required]
        [MaxLength(2)]
        public int Number { get; set; }

        /// <summary>
        /// Gets or sets how many goals the player has scored in the current season.
        /// </summary>
        [Required]
        [MaxLength(3)]
        public int Goals { get; set; }

        /// <summary>
        /// Gets or sets how many assists the player has given in the current season.
        /// </summary>
        [Required]
        [MaxLength(3)]
        public int Assists { get; set; }

        /// <summary>
        /// Gets or sets how many matches the player has played in the current season.
        /// </summary>
        [Required]
        [MaxLength(3)]
        public int Matches { get; set; }

        /// <summary>
        /// Gets or sets the player's nationality.
        /// </summary>
        [Required]
        public string Nationality { get; set; }

        /// <summary>
        /// Gets or sets the player's current market value in euros.
        /// </summary>
        [MaxLength(9)]
        public int MarketValue { get; set; }

        /// <summary>
        /// Gets or sets the foreign key from the teams table.
        /// </summary>
        [ForeignKey(nameof(Team))]
        public int TeamId { get; set; }

        /// <summary>
        /// Gets or sets which team the player is playing in.
        /// </summary>
        public virtual Team Team { get; set; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>True if the specified object is equal to the current object; otherwise, false.</returns>
        public override bool Equals(object obj)
        {
            return obj is Player player &&
                   this.Id == player.Id &&
                   this.Name == player.Name &&
                   this.DateOfBirth == player.DateOfBirth &&
                   this.Position == player.Position &&
                   this.Number == player.Number &&
                   this.Goals == player.Goals &&
                   this.Assists == player.Assists &&
                   this.Matches == player.Matches &&
                   this.Nationality == player.Nationality &&
                   this.MarketValue == player.MarketValue &&
                   this.TeamId == player.TeamId;
        }

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            return this.Id;
        }

        /// <summary>
        /// Returns a string that represents the current Player object.
        /// </summary>
        /// <returns>A string that represents the current Player object. </returns>
        public override string ToString()
        {
            return this.Id.ToString("D", CultureInfo.CurrentCulture).PadRight(5) + "|" + this.Number.ToString("D", CultureInfo.CurrentCulture).PadRight(6) + "|" + this.Name.PadRight(40) + "|" + this.Team.Name.PadRight(30) + "|" + this.DateOfBirth.ToShortDateString() + "|" + this.Nationality.PadRight(20) + "|" + this.Position.PadRight(18) + "|" + this.Matches.ToString("D", CultureInfo.CurrentCulture).PadRight(7) + "|" + this.Goals.ToString("D", CultureInfo.CurrentCulture).PadRight(5) + "|" + this.Assists.ToString("D", CultureInfo.CurrentCulture).PadRight(7) + "|" + this.MarketValue.ToString("##,#", CultureInfo.CurrentCulture) + " Euro";
        }
    }
}
