﻿// <copyright file="FootballContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.Data
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Timers;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// A class that cointains the tables of the database.
    /// </summary>
    public class FootballContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FootballContext"/> class.
        /// </summary>
        public FootballContext()
        {
            this.Database.EnsureCreated();
        }

        /// <summary>
        /// Gets or sets the leagues table.
        /// </summary>
        public virtual DbSet<League> Leagues { get; set; }

        /// <summary>
        /// Gets or sets the players table.
        /// </summary>
        public virtual DbSet<Player> Players { get; set; }

        /// <summary>
        /// Gets or sets the stadiums table.
        /// </summary>
        public virtual DbSet<Stadium> Stadiums { get; set; }

        /// <summary>
        /// Gets or sets the teams table.
        /// </summary>
        public virtual DbSet<Team> Teams { get; set; }

        /// <summary>
        /// Configures the database when it is created. Sets the lazy loading and the location for the database.
        /// </summary>
        /// <param name="optionsBuilder">Parameter that sets options for the database when it is created for the first time.</param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder != null && !optionsBuilder.IsConfigured)
            {
                optionsBuilder
                    .UseLazyLoadingProxies()
                    .UseSqlServer(@"Data Source = (LocalDB)\MSSQLLocalDB; AttachDbFilename = |DataDirectory|\FootballDatabase.mdf; Integrated Security = True; MultipleActiveResultSets = True");
            }
        }

        /// <summary>
        /// A method for creating data and setting up the navigation properties between the tables of the database.
        /// </summary>
        /// <param name="modelBuilder">Parameter that sets the navigation properties between the tables of the database.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            if (modelBuilder != null)
            {
                modelBuilder.Entity<Team>(entity =>
                {
                    entity.HasOne(team => team.Stadium)
                        .WithMany(stadium => stadium.Teams)
                        .HasForeignKey(team => team.StadiumId)
                        .OnDelete(DeleteBehavior.ClientSetNull);
                });
                modelBuilder.Entity<Team>(entity =>
                {
                    entity.HasOne(team => team.League)
                        .WithMany(league => league.Teams)
                        .HasForeignKey(team => team.LeagueId)
                        .OnDelete(DeleteBehavior.ClientSetNull);
                });
                modelBuilder.Entity<Player>(entity =>
                {
                    entity.HasOne(player => player.Team)
                    .WithMany(team => team.Players)
                    .HasForeignKey(player => player.TeamId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
                });
                League[] leagues = new League[3];
                League premierLeague = new League { Id = 1, Name = "Premier League", Season = "2020-2021", AverageValue = 16020000, Country = "England", Division = 1 };
                leagues[0] = premierLeague;
                League bundesliga = new League { Id = 2, Name = "Bundesliga", Season = "2020-2021", AverageValue = 0, Country = "Germany", Division = 1 };
                leagues[1] = bundesliga;
                League laLiga = new League { Id = 3, Name = "La Liga", Season = "2020-2021", AverageValue = 0, Country = "Spain", Division = 1 };
                leagues[2] = laLiga;

                Stadium[] stadiums = new Stadium[15];
                int i = 0;

                Stadium anfield = new Stadium { Id = 1, Name = "Anfield", Address = "Anfield, Anfield Road, L4 0TH Liverpool, England", Capacity = 54074, YearOfBuilt = 1884 };
                stadiums[i++] = anfield;
                Stadium etihad = new Stadium { Id = 2, Name = "Etihad Stadium", Address = "Etihad Stadium, Rowsley Street, M11 3FF Manchester, England", Capacity = 55017, YearOfBuilt = 2002 };
                stadiums[i++] = etihad;
                Stadium stamford = new Stadium { Id = 3, Name = "Stamford Bridge", Address = "Stamford Bridge, Fulham Road, SW6 1HS London, England", Capacity = 40853, Phone = "+44 207 915 2900", YearOfBuilt = 1877 };
                stadiums[i++] = stamford;
                Stadium oldTrafford = new Stadium { Id = 4, Name = "Old Trafford", Address = "Old Trafford, Sir Matt Busby Way, Old Trafford, M16 ORA Manchester, England", Capacity = 74879, Phone = "+44 870 757 1968", YearOfBuilt = 1910 };
                stadiums[i++] = oldTrafford;
                Stadium hotspur = new Stadium { Id = 5, Name = "Tottenham Hotspur Stadium", Address = "Tottenham Hotspur Stadium, 782 High Road, N17 0BX London, England", Capacity = 62062, YearOfBuilt = 2019 };
                stadiums[i++] = hotspur;
                /*
                Stadium emirates = new Stadium { Id = 6, Name = "Emirates Stadium", Address = "Emirates Stadium, Hornsey Road, N5 1BU London, England", Capacity = 60704, YearOfBuilt = 2006 };
                stadiums[i++] = emirates;
                Stadium goodison = new Stadium { Id = 7, Name = "Goodison Park", Address = "Goodison Park, Goodison Road, L4 4EL Liverpool, England", Capacity = 39571, Phone = "+44 151 330 2200", YearOfBuilt = 1892 };
                stadiums[i++] = goodison;
                Stadium kingPower = new Stadium { Id = 8, Name = "King Power Stadium", Address = "King Power Stadium, Filbert Way, LE2 7FL Leicester, England", Capacity = 32273, Phone = "0844 815 5000", YearOfBuilt = 2002 };
                stadiums[i++] = kingPower;
                Stadium molineux = new Stadium { Id = 9, Name = "Molineux Stadium", Address = "Molineux Stadium, Waterloo Road, WV1 4QR Wolverhampton, England", Capacity = 32050, Phone = "0871 222 1877", YearOfBuilt = 1889 };
                stadiums[i++] = molineux;
                Stadium villa = new Stadium { Id = 10, Name = "Villa Park", Address = "Villa Park, Trinity Road, B6 6HE Birmingham, England", Capacity = 42682, YearOfBuilt = 1897 };
                stadiums[i++] = villa;
                Stadium london = new Stadium { Id = 11, Name = "London Stadium", Address = "London Stadium, Queen Elizabeth Olympic Park, E20 2ST London, England", Capacity = 60000, YearOfBuilt = 2011 };
                stadiums[i++] = london;
                Stadium stJames = new Stadium { Id = 12, Name = "St James' Park", Address = "St James' Park, NE1 4ST Newcastle upon Tyne, England", Capacity = 52338, YearOfBuilt = 1880 };
                stadiums[i++] = stJames;
                Stadium amex = new Stadium { Id = 13, Name = "AMEX Stadium", Address = "AMEX Stadium, Village Way, BN1 9BL Brighton, England", Capacity = 30666, Phone = "01273 878288", YearOfBuilt = 2011 };
                stadiums[i++] = amex;
                Stadium selhurst = new Stadium { Id = 14, Name = "Selhurst Park", Address = "Selhurst Park, Whitehorse Lane, SE25 6PU London, England", Capacity = 26047, Phone = "+44 8712 00 00 71", YearOfBuilt = 1924 };
                stadiums[i++] = selhurst;
                Stadium stMary = new Stadium { Id = 15, Name = "St Mary's Stadium", Address = "St Mary's Stadium, Britannia Road, SO14 5FP Southampton, England", Capacity = 32384, Phone = "0870 2200150", YearOfBuilt = 2001 };
                stadiums[i++] = stMary;
                Stadium craven = new Stadium { Id = 16, Name = "Craven Cottage", Address = "Craven Cottage, Stevenage Road, SW6 6HH London, England", Capacity = 19000, Phone = "0843 208 1222", YearOfBuilt = 1896 };
                stadiums[i++] = craven;
                Stadium elland = new Stadium { Id = 17, Name = "Elland Road", Address = "Elland Road, Elland Road, LS11 0ES Leeds, England", Capacity = 37890, YearOfBuilt = 1919 };
                stadiums[i++] = elland;
                Stadium bramall = new Stadium { Id = 18, Name = "Bramall Lane", Address = "Bramall Lane, Bramall Lane, S2 4SU Sheffield, England", Capacity = 32702, YearOfBuilt = 1855 };
                stadiums[i++] = bramall;
                Stadium turfMoor = new Stadium { Id = 19, Name = "Turf Moor", Address = "Turf Moor, Harry Potts Way, BB10 4BX Burnley, England", Capacity = 21994, Phone = "+44 844 807 1882", YearOfBuilt = 1883 };
                stadiums[i++] = turfMoor;
                Stadium hawthorns = new Stadium { Id = 20, Name = "The Hawthorns", Address = "The Hawthorns, Halfords Lane, B71 4LF West Bromwich, England", Capacity = 26850, Phone = "0871 271 9861", YearOfBuilt = 1900 };
                stadiums[i++] = hawthorns;
                */
                Stadium allianz = new Stadium { Id = 21, Name = "Allianz Arena", Address = "Allianz Arena, Werner - Heisenberg - Allee 25, 80939 München, Germany", Capacity = 75024, Phone = "+49 (89) 69931333", YearOfBuilt = 2005 };
                stadiums[i++] = allianz;
                Stadium signal = new Stadium { Id = 22, Name = "SIGNAL IDUNA PARK", Address = "SIGNAL IDUNA PARK, Strobelallee 50, 44139 Dortmund, Germany", Capacity = 81365, Phone = "+49 (231) 90200", YearOfBuilt = 1974 };
                stadiums[i++] = signal;
                Stadium redBull = new Stadium { Id = 23, Name = "Red Bull Arena", Address = "Red Bull Arena, Am Sportforum 1, 04105 Leipzig, Germany", Capacity = 42146, YearOfBuilt = 2003 };
                stadiums[i++] = redBull;
                Stadium bayarena = new Stadium { Id = 24, Name = "BayArena", Address = "BayArena, Bismarckstraße 122 - 124, 51373 Leverkusen, Germany", Capacity = 30210, Phone = "+49 (1805) 040404", YearOfBuilt = 1956 };
                stadiums[i++] = bayarena;
                Stadium borussiaPark = new Stadium { Id = 25, Name = "Stadion im Borussia-Park", Address = "Stadion im Borussia-Park, Hennes - Weisweiler - Allee, 41069 Mönchengladbach, Germany", Capacity = 54022, Phone = "+49 (2161) 9293-1000", YearOfBuilt = 37869 };
                stadiums[i++] = borussiaPark;

                Stadium campNou = new Stadium { Id = 26, Name = "Camp Nou", Address = "Camp Nou, Carrer d´Aristides Maillol, entrance no 7, 08028 Barcelona, Spain", Capacity = 99354, Phone = "00 34 - 902 - 189900", YearOfBuilt = 1957 };
                stadiums[i++] = campNou;
                Stadium diStefano = new Stadium { Id = 27, Name = "Estadio Alfredo Di Stéfano", Address = "Estadio Alfredo Di Stéfano, Camino Sintra 4, 28042 Madrid, Spain", Capacity = 9000, Phone = "+34 (91) 3984000", YearOfBuilt = 2006 };
                stadiums[i++] = diStefano;
                Stadium wanda = new Stadium { Id = 28, Name = "Wanda Metropolitano", Address = "Wanda Metropolitano, Plaza de Grecia, 28022 Madrid, Spain", Capacity = 68456, YearOfBuilt = 2017 };
                stadiums[i++] = wanda;
                Stadium ramon = new Stadium { Id = 29, Name = "Ramón Sánchez Pizjuan", Address = "Ramón Sánchez Pizjuan, Avenida Eduardo Datom, 41005 Sevilla, Spain", Capacity = 43883, Phone = "00 34 - 902 - 510011", YearOfBuilt = 1958 };
                stadiums[i++] = ramon;
                Stadium realeArena = new Stadium { Id = 30, Name = "Reale Arena", Address = "Reale Arena, San Sebastián, Spain", Capacity = 39500, YearOfBuilt = 1993 };
                stadiums[i++] = realeArena;

                Team[] teams = new Team[15];
                i = 0;

                Team liverpool = new Team { Id = 1, Name = "Liverpool FC", HomeShirt = "Red", AwayShirt = "Turqoise", YearOfEstablishment = 1892, LeaguesWon = 19, LeagueId = premierLeague.Id, StadiumId = anfield.Id };
                teams[i++] = liverpool;
                Team manCity = new Team { Id = 2, Name = "Manchester City", HomeShirt = "Blue", AwayShirt = "Black", YearOfEstablishment = 1880, LeaguesWon = 6, LeagueId = premierLeague.Id, StadiumId = etihad.Id };
                teams[i++] = manCity;
                Team chelsea = new Team { Id = 3, Name = "Chelsea FC", HomeShirt = "Blue", AwayShirt = "Gray", YearOfEstablishment = 1905, LeaguesWon = 6, LeagueId = premierLeague.Id, StadiumId = stamford.Id };
                teams[i++] = chelsea;
                Team manUtd = new Team { Id = 4, Name = "Manchester United", HomeShirt = "Red", AwayShirt = "Black", YearOfEstablishment = 1878, LeaguesWon = 20, LeagueId = premierLeague.Id, StadiumId = oldTrafford.Id };
                teams[i++] = manUtd;
                Team tottenham = new Team { Id = 5, Name = "Tottenham Hotspur", HomeShirt = "White", AwayShirt = "Green", YearOfEstablishment = 1882, LeaguesWon = 2, LeagueId = premierLeague.Id, StadiumId = hotspur.Id };
                teams[i++] = tottenham;
                /*
                Team arsenal = new Team { Id = 6, Name = "Arsenal FC", HomeShirt = "Red", AwayShirt = "White", YearOfEstablishment = 1886, LeaguesWon = 13, LeagueId = premierLeague.Id, StadiumId = emirates.Id };
                teams[i++] = arsenal;
                Team everton = new Team { Id = 7, Name = "Everton FC", HomeShirt = "Blue", AwayShirt = "Yellow", YearOfEstablishment = 1878, LeaguesWon = 9, LeagueId = premierLeague.Id, StadiumId = goodison.Id };
                teams[i++] = everton;
                Team leicester = new Team { Id = 8, Name = "Leicester City", HomeShirt = "Blue", AwayShirt = "White", YearOfEstablishment = 1884, LeaguesWon = 1, LeagueId = premierLeague.Id, StadiumId = kingPower.Id };
                teams[i++] = leicester;
                Team wolverhampton = new Team { Id = 9, Name = "Wolverhampton Wanderers", HomeShirt = "Orange", AwayShirt = "White and Blue", YearOfEstablishment = 1877, LeaguesWon = 3, LeagueId = premierLeague.Id, StadiumId = molineux.Id };
                teams[i++] = wolverhampton;
                Team astonVilla = new Team { Id = 10, Name = "Aston Villa", HomeShirt = "Red", AwayShirt = "Black", YearOfEstablishment = 1874, LeaguesWon = 7, LeagueId = premierLeague.Id, StadiumId = villa.Id };
                teams[i++] = astonVilla;
                Team westHam = new Team { Id = 11, Name = "West Ham United", HomeShirt = "Red", AwayShirt = "Blue", YearOfEstablishment = 1895, LeaguesWon = 0, LeagueId = premierLeague.Id, StadiumId = london.Id };
                teams[i++] = westHam;
                Team newcastle = new Team { Id = 12, Name = "Newcastle United", HomeShirt = "Black and White", AwayShirt = "Yellow", YearOfEstablishment = 1892, LeaguesWon = 4, LeagueId = premierLeague.Id, StadiumId = stJames.Id };
                teams[i++] = newcastle;
                Team brighton = new Team { Id = 13, Name = "Brighton & Hove Albion", HomeShirt = "Blue", AwayShirt = "Yellow", YearOfEstablishment = 1901, LeaguesWon = 0, LeagueId = premierLeague.Id, StadiumId = amex.Id };
                teams[i++] = brighton;
                Team crystal = new Team { Id = 14, Name = "Crystal Palace", HomeShirt = "Blue and Red", AwayShirt = "White and Blue and Red", YearOfEstablishment = 1905, LeaguesWon = 0, LeagueId = premierLeague.Id, StadiumId = selhurst.Id };
                teams[i++] = crystal;
                Team southampton = new Team { Id = 15, Name = "Southampton FC", HomeShirt = "Red", AwayShirt = "Blue", YearOfEstablishment = 1885, LeaguesWon = 0, LeagueId = premierLeague.Id, StadiumId = stMary.Id };
                teams[i++] = southampton;
                Team fulham = new Team { Id = 16, Name = "Fulham FC", HomeShirt = "White", AwayShirt = "Yellow", YearOfEstablishment = 1879, LeaguesWon = 0, LeagueId = premierLeague.Id, StadiumId = craven.Id };
                teams[i++] = fulham;
                Team leeds = new Team { Id = 17, Name = "Leeds United", HomeShirt = "White", AwayShirt = "Blue", YearOfEstablishment = 1919, LeaguesWon = 3, LeagueId = premierLeague.Id, StadiumId = elland.Id };
                teams[i++] = leeds;
                Team sheffield = new Team { Id = 18, Name = "Sheffield United", HomeShirt = "White and Red", AwayShirt = "Pink", YearOfEstablishment = 1889, LeaguesWon = 1, LeagueId = premierLeague.Id, StadiumId = bramall.Id };
                teams[i++] = sheffield;
                Team burnley = new Team { Id = 19, Name = "Burnley FC", HomeShirt = "Red", AwayShirt = "Black", YearOfEstablishment = 1882, LeaguesWon = 2, LeagueId = premierLeague.Id, StadiumId = turfMoor.Id };
                teams[i++] = burnley;
                Team westBromwich = new Team { Id = 20, Name = "West Bromwich Albion", HomeShirt = "Blue and White", AwayShirt = "Green and Yellow", YearOfEstablishment = 1878, LeaguesWon = 1, LeagueId = premierLeague.Id, StadiumId = hawthorns.Id };
                teams[i++] = westBromwich;
                */
                Team bayern = new Team { Id = 21, Name = "Bayern Munich", HomeShirt = "Red", AwayShirt = "White", YearOfEstablishment = 1900, LeaguesWon = 30, LeagueId = bundesliga.Id, StadiumId = allianz.Id };
                teams[i++] = bayern;
                Team dortmund = new Team { Id = 22, Name = "Borussia Dortmund", HomeShirt = "Yellow", AwayShirt = "Black", YearOfEstablishment = 1909, LeaguesWon = 8, LeagueId = bundesliga.Id, StadiumId = signal.Id };
                teams[i++] = dortmund;
                Team leipzig = new Team { Id = 23, Name = "RB Leipzig", HomeShirt = "White", AwayShirt = "Blue", YearOfEstablishment = 2009, LeaguesWon = 0, LeagueId = bundesliga.Id, StadiumId = redBull.Id };
                teams[i++] = leipzig;
                Team leverkusen = new Team { Id = 24, Name = "Bayer 04 Leverkusen", HomeShirt = "Black", AwayShirt = "Red", YearOfEstablishment = 1904, LeaguesWon = 0, LeagueId = bundesliga.Id, StadiumId = bayarena.Id };
                teams[i++] = leverkusen;
                Team gladbach = new Team { Id = 25, Name = "Borussia Mönchengladbach", HomeShirt = "White and Black", AwayShirt = "Green", YearOfEstablishment = 1900, LeaguesWon = 5, LeagueId = bundesliga.Id, StadiumId = borussiaPark.Id };
                teams[i++] = gladbach;

                Team barcelona = new Team { Id = 26, Name = "FC Barcelona", HomeShirt = "Red and Blue", AwayShirt = "Black", YearOfEstablishment = 1899, LeaguesWon = 26, LeagueId = laLiga.Id, StadiumId = campNou.Id };
                teams[i++] = barcelona;
                Team realMadrid = new Team { Id = 27, Name = "Real Madrid", HomeShirt = "White", AwayShirt = "Pink", YearOfEstablishment = 1902, LeaguesWon = 34, LeagueId = laLiga.Id, StadiumId = diStefano.Id };
                teams[i++] = realMadrid;
                Team atleticoMadrid = new Team { Id = 28, Name = "Atletico Madrid", HomeShirt = "Red and White", AwayShirt = "Blue", YearOfEstablishment = 1903, LeaguesWon = 10, LeagueId = laLiga.Id, StadiumId = wanda.Id };
                teams[i++] = atleticoMadrid;
                Team sevilla = new Team { Id = 29, Name = "Sevilla FC", HomeShirt = "White", AwayShirt = "Red", YearOfEstablishment = 1905, LeaguesWon = 1, LeagueId = laLiga.Id, StadiumId = ramon.Id };
                teams[i++] = sevilla;
                Team realSociedad = new Team { Id = 30, Name = "Real Sociedad", HomeShirt = "Blue and White", AwayShirt = "Red", YearOfEstablishment = 1909, LeaguesWon = 2, LeagueId = laLiga.Id, StadiumId = realeArena.Id };
                teams[i++] = realSociedad;

                Player[] players = new Player[174];
                i = 0;

                players[i++] = new Player { Id = 1, Name = "Allison", DateOfBirth = new DateTime(1992, 10, 2), Position = "Goalkeeper", Number = 1, Goals = 0, Assists = 0, Matches = 4, Nationality = "Brazil", MarketValue = 80000000, TeamId = liverpool.Id };
                players[i++] = new Player { Id = 2, Name = "Adrián", DateOfBirth = new DateTime(1987, 1, 3), Position = "Goalkeeper", Number = 13, Goals = 0, Assists = 0, Matches = 2, Nationality = "Spain", MarketValue = 4000000, TeamId = liverpool.Id };
                players[i++] = new Player { Id = 3, Name = "Caomhin Kelleher", DateOfBirth = new DateTime(1998, 11, 23), Position = "Goalkeeper", Number = 62, Goals = 0, Assists = 0, Matches = 0, Nationality = "Ireland", MarketValue = 900000, TeamId = liverpool.Id };
                players[i++] = new Player { Id = 4, Name = "Virgil van Dijk", DateOfBirth = new DateTime(1991, 7, 8), Position = "Centre-Back", Number = 4, Goals = 1, Assists = 0, Matches = 5, Nationality = "Netherlands", MarketValue = 80000000, TeamId = liverpool.Id };
                players[i++] = new Player { Id = 5, Name = "Joe Gomez", DateOfBirth = new DateTime(1997, 3, 23), Position = "Centre-Back", Number = 12, Goals = 0, Assists = 0, Matches = 5, Nationality = "England", MarketValue = 40000000, TeamId = liverpool.Id };
                players[i++] = new Player { Id = 6, Name = "Joel Matip", DateOfBirth = new DateTime(1991, 8, 8), Position = "Centre-Back", Number = 32, Goals = 0, Assists = 0, Matches = 2, Nationality = "Cameroon", MarketValue = 32000000, TeamId = liverpool.Id };
                players[i++] = new Player { Id = 7, Name = "Sepp van den Berg", DateOfBirth = new DateTime(2001, 12, 20), Position = "Centre-Back", Number = 72, Goals = 0, Assists = 0, Matches = 0, Nationality = "Netherlands", MarketValue = 1800000, TeamId = liverpool.Id };
                players[i++] = new Player { Id = 8, Name = "Andrew Robertson", DateOfBirth = new DateTime(1994, 3, 11), Position = "Left-Back", Number = 26, Goals = 1, Assists = 2, Matches = 6, Nationality = "Scotland", MarketValue = 75000000, TeamId = liverpool.Id };
                players[i++] = new Player { Id = 9, Name = "Konstantinos Tsimikas", DateOfBirth = new DateTime(1996, 5, 12), Position = "Left-Back", Number = 21, Goals = 0, Assists = 0, Matches = 0, Nationality = "Greece", MarketValue = 9000000, TeamId = liverpool.Id };
                players[i++] = new Player { Id = 10, Name = "Trent Alexander-Arnold", DateOfBirth = new DateTime(1998, 10, 7), Position = "Right-Back", Number = 66, Goals = 0, Assists = 1, Matches = 6, Nationality = "England", MarketValue = 110000000, TeamId = liverpool.Id };
                players[i++] = new Player { Id = 11, Name = "Neco William", DateOfBirth = new DateTime(2001, 4, 13), Position = "Right-Back", Number = 76, Goals = 0, Assists = 0, Matches = 0, Nationality = "Wales", MarketValue = 4000000, TeamId = liverpool.Id };
                players[i++] = new Player { Id = 12, Name = "Fabinho", DateOfBirth = new DateTime(1993, 10, 23), Position = "Defensive Midfield", Number = 3, Goals = 0, Assists = 1, Matches = 6, Nationality = "Brazil", MarketValue = 60000000, TeamId = liverpool.Id };
                players[i++] = new Player { Id = 13, Name = "Thiago", DateOfBirth = new DateTime(1991, 4, 11), Position = "Central Midfield", Number = 6, Goals = 0, Assists = 0, Matches = 2, Nationality = "Spain", MarketValue = 48000000, TeamId = liverpool.Id };
                players[i++] = new Player { Id = 14, Name = "Georgino Wijnaldum", DateOfBirth = new DateTime(1990, 11, 11), Position = "Central Midfield", Number = 5, Goals = 0, Assists = 0, Matches = 6, Nationality = "Netherlands", MarketValue = 40000000, TeamId = liverpool.Id };
                players[i++] = new Player { Id = 15, Name = "Naby Keïta", DateOfBirth = new DateTime(1995, 2, 10), Position = "Central Midfield", Number = 8, Goals = 0, Assists = 0, Matches = 4, Nationality = "Guinea", MarketValue = 40000000, TeamId = liverpool.Id };
                players[i++] = new Player { Id = 16, Name = "Jordan Henderson", DateOfBirth = new DateTime(1990, 6, 17), Position = "Central Midfield", Number = 14, Goals = 0, Assists = 0, Matches = 4, Nationality = "England", MarketValue = 28000000, TeamId = liverpool.Id };
                players[i++] = new Player { Id = 17, Name = "Alex Oxlade-Chamberlain", DateOfBirth = new DateTime(1993, 8, 15), Position = "Central Midfield", Number = 15, Goals = 0, Assists = 0, Matches = 0, Nationality = "England", MarketValue = 24000000, TeamId = liverpool.Id };
                players[i++] = new Player { Id = 18, Name = "James Milner", DateOfBirth = new DateTime(1986, 1, 4), Position = "Central Midfield", Number = 7, Goals = 0, Assists = 0, Matches = 4, Nationality = "England", MarketValue = 5000000, TeamId = liverpool.Id };
                players[i++] = new Player { Id = 19, Name = "Curtis Jones", DateOfBirth = new DateTime(2001, 1, 30), Position = "Central Midfield", Number = 17, Goals = 0, Assists = 0, Matches = 2, Nationality = "England", MarketValue = 5000000, TeamId = liverpool.Id };
                players[i++] = new Player { Id = 20, Name = "Sadio Mané", DateOfBirth = new DateTime(1992, 4, 10), Position = "Left Winger", Number = 10, Goals = 4, Assists = 1, Matches = 5, Nationality = "Senegal", MarketValue = 120000000, TeamId = liverpool.Id };
                players[i++] = new Player { Id = 21, Name = "Diogo Jota", DateOfBirth = new DateTime(1996, 12, 4), Position = "Left Winger", Number = 20, Goals = 2, Assists = 0, Matches = 4, Nationality = "Portugal", MarketValue = 40000000, TeamId = liverpool.Id };
                players[i++] = new Player { Id = 22, Name = "Takumi Minamino", DateOfBirth = new DateTime(1995, 1, 16), Position = "Left Winger", Number = 18, Goals = 0, Assists = 0, Matches = 4, Nationality = "Japan", MarketValue = 10000000, TeamId = liverpool.Id };
                players[i++] = new Player { Id = 23, Name = "Mohamed Salah", DateOfBirth = new DateTime(1992, 6, 15), Position = "Right Winger", Number = 11, Goals = 6, Assists = 0, Matches = 6, Nationality = "Egypt", MarketValue = 120000000, TeamId = liverpool.Id };
                players[i++] = new Player { Id = 24, Name = "Xherdan Shaqiri", DateOfBirth = new DateTime(1991, 10, 10), Position = "Right Winger", Number = 23, Goals = 0, Assists = 0, Matches = 0, Nationality = "Switzerland", MarketValue = 16000000, TeamId = liverpool.Id };
                players[i++] = new Player { Id = 25, Name = "Roberto Firmino", DateOfBirth = new DateTime(1991, 10, 2), Position = "Centre-Forward", Number = 9, Goals = 1, Assists = 2, Matches = 6, Nationality = "Brazil", MarketValue = 72000000, TeamId = liverpool.Id };
                players[i++] = new Player { Id = 26, Name = "Divock Origi", DateOfBirth = new DateTime(1995, 4, 18), Position = "Centre-Forward", Number = 27, Goals = 0, Assists = 0, Matches = 0, Nationality = "Belgium", MarketValue = 16000000, TeamId = liverpool.Id };

                players[i++] = new Player { Id = 27, Name = "Ederson", DateOfBirth = new DateTime(1993, 8, 17), Position = "Goalkeeper", Number = 31, Goals = 0, Assists = 0, Matches = 5, Nationality = "Brazil", MarketValue = 56000000, TeamId = manCity.Id };
                players[i++] = new Player { Id = 28, Name = "Zack Steffen", DateOfBirth = new DateTime(1995, 4, 2), Position = "Goalkeeper", Number = 13, Goals = 0, Assists = 0, Matches = 0, Nationality = "United States", MarketValue = 6000000, TeamId = manCity.Id };
                players[i++] = new Player { Id = 29, Name = "Scott Carson", DateOfBirth = new DateTime(1985, 9, 3), Position = "Goalkeeper", Number = 33, Goals = 0, Assists = 0, Matches = 0, Nationality = "England", MarketValue = 800000, TeamId = manCity.Id };
                players[i++] = new Player { Id = 30, Name = "Aymeric Laporte", DateOfBirth = new DateTime(1994, 5, 27), Position = "Centre-Back", Number = 14, Goals = 0, Assists = 0, Matches = 1, Nationality = "France", MarketValue = 60000000, TeamId = manCity.Id };
                players[i++] = new Player { Id = 31, Name = "Rúben Dias", DateOfBirth = new DateTime(1997, 5, 14), Position = "Centre-Back", Number = 3, Goals = 0, Assists = 0, Matches = 3, Nationality = "Portugal", MarketValue = 50000000, TeamId = manCity.Id };
                players[i++] = new Player { Id = 32, Name = "Nathan Aké", DateOfBirth = new DateTime(1995, 2, 18), Position = "Centre-Back", Number = 6, Goals = 1, Assists = 0, Matches = 4, Nationality = "Netherlands", MarketValue = 40000000, TeamId = manCity.Id };
                players[i++] = new Player { Id = 33, Name = "John Stones", DateOfBirth = new DateTime(1994, 5, 28), Position = "Centre-Back", Number = 5, Goals = 0, Assists = 0, Matches = 1, Nationality = "England", MarketValue = 25000000, TeamId = manCity.Id };
                players[i++] = new Player { Id = 34, Name = "Eric García", DateOfBirth = new DateTime(2001, 1, 9), Position = "Centre-Back", Number = 50, Goals = 0, Assists = 0, Matches = 2, Nationality = "Spain", MarketValue = 20000000, TeamId = manCity.Id };
                players[i++] = new Player { Id = 35, Name = "Philippe Sandler", DateOfBirth = new DateTime(1997, 2, 10), Position = "Centre-Back", Number = 34, Goals = 0, Assists = 0, Matches = 0, Nationality = "Netherlands", MarketValue = 3500000, TeamId = manCity.Id };
                players[i++] = new Player { Id = 36, Name = "Taylor Harwood-Bellis", DateOfBirth = new DateTime(2002, 1, 30), Position = "Centre-Back", Number = 78, Goals = 0, Assists = 0, Matches = 0, Nationality = "England", MarketValue = 2300000, TeamId = manCity.Id };
                players[i++] = new Player { Id = 37, Name = "Benjamin Mendy", DateOfBirth = new DateTime(1994, 7, 17), Position = "Left-Back", Number = 22, Goals = 0, Assists = 0, Matches = 3, Nationality = "France", MarketValue = 28000000, TeamId = manCity.Id };
                players[i++] = new Player { Id = 38, Name = "Oleksandr Zinchenko", DateOfBirth = new DateTime(1996, 12, 15), Position = "Left-Back", Number = 11, Goals = 0, Assists = 0, Matches = 1, Nationality = "Ukraine", MarketValue = 20000000, TeamId = manCity.Id };
                players[i++] = new Player { Id = 39, Name = "Kyle Walker", DateOfBirth = new DateTime(1990, 5, 28), Position = "Right-Back", Number = 2, Goals = 0, Assists = 0, Matches = 5, Nationality = "England", MarketValue = 40000000, TeamId = manCity.Id };
                players[i++] = new Player { Id = 40, Name = "João Cancelo", DateOfBirth = new DateTime(1994, 5, 27), Position = "Right_Back", Number = 27, Goals = 0, Assists = 1, Matches = 2, Nationality = "Portugal", MarketValue = 36000000, TeamId = manCity.Id };
                players[i++] = new Player { Id = 41, Name = "Rodri", DateOfBirth = new DateTime(1996, 6, 22), Position = "Defensive Midfield", Number = 16, Goals = 0, Assists = 0, Matches = 5, Nationality = "Spain", MarketValue = 64000000, TeamId = manCity.Id };
                players[i++] = new Player { Id = 42, Name = "Fernandinho", DateOfBirth = new DateTime(1985, 5, 4), Position = "Defensive Midfield", Number = 25, Goals = 0, Assists = 0, Matches = 4, Nationality = "Brazil", MarketValue = 4000000, TeamId = manCity.Id };
                players[i++] = new Player { Id = 43, Name = "Phil Foden", DateOfBirth = new DateTime(2000, 5, 28), Position = "Central Midfield", Number = 47, Goals = 2, Assists = 0, Matches = 5, Nationality = "England", MarketValue = 60000000, TeamId = manCity.Id };
                players[i++] = new Player { Id = 44, Name = "Ilkay Gündogan", DateOfBirth = new DateTime(1990, 10, 24), Position = "Central Midfield", Number = 8, Goals = 0, Assists = 0, Matches = 2, Nationality = "Germany", MarketValue = 40000000, TeamId = manCity.Id };
                players[i++] = new Player { Id = 45, Name = "Tommy Doyle", DateOfBirth = new DateTime(2001, 10, 17), Position = "Central Midfield", Number = 69, Goals = 0, Assists = 0, Matches = 0, Nationality = "England", MarketValue = 1000000, TeamId = manCity.Id };
                players[i++] = new Player { Id = 46, Name = "Kevin De Bruyne", DateOfBirth = new DateTime(1991, 6, 28), Position = "Attacking Midfield", Number = 17, Goals = 1, Assists = 1, Matches = 4, Nationality = "Belgium", MarketValue = 120000000, TeamId = manCity.Id };
                players[i++] = new Player { Id = 47, Name = "Raheem Sterling", DateOfBirth = new DateTime(1994, 12, 8), Position = "Left Winger", Number = 7, Goals = 2, Assists = 1, Matches = 5, Nationality = "England", MarketValue = 128000000, TeamId = manCity.Id };
                players[i++] = new Player { Id = 48, Name = "Bernardo Silva", DateOfBirth = new DateTime(1994, 8, 10), Position = "Right Winger", Number = 20, Goals = 0, Assists = 0, Matches = 3, Nationality = "Portugal", MarketValue = 80000000, TeamId = manCity.Id };
                players[i++] = new Player { Id = 49, Name = "Riyad Mahrez", DateOfBirth = new DateTime(1991, 2, 21), Position = "Right Winger", Number = 26, Goals = 1, Assists = 1, Matches = 4, Nationality = "Algeria", MarketValue = 48000000, TeamId = manCity.Id };
                players[i++] = new Player { Id = 50, Name = "Ferran Torres", DateOfBirth = new DateTime(2000, 2, 29), Position = "Right Winger", Number = 21, Goals = 0, Assists = 1, Matches = 3, Nationality = "Spain", MarketValue = 45000000, TeamId = manCity.Id };
                players[i++] = new Player { Id = 51, Name = "Gabriel Jesus", DateOfBirth = new DateTime(1997, 4, 3), Position = "Centre-Forward", Number = 9, Goals = 1, Assists = 0, Matches = 1, Nationality = "Brazil", MarketValue = 60000000, TeamId = manCity.Id };
                players[i++] = new Player { Id = 52, Name = "Sergio Agüero", DateOfBirth = new DateTime(1988, 6, 2), Position = "Centre-Forward", Number = 10, Goals = 0, Assists = 0, Matches = 2, Nationality = "Argentina", MarketValue = 42000000, TeamId = manCity.Id };

                players[i++] = new Player { Id = 53, Name = "Edouard Mendy", DateOfBirth = new DateTime(1992, 3, 1), Position = "Goalkeeper", Number = 16, Goals = 0, Assists = 0, Matches = 10, Nationality = "Senegal", MarketValue = 20000000, TeamId = chelsea.Id };
                players[i++] = new Player { Id = 54, Name = "Kepa", DateOfBirth = new DateTime(1994, 10, 3), Position = "Goalkeeper", Number = 1, Goals = 0, Assists = 0, Matches = 3, Nationality = "Spain", MarketValue = 18000000, TeamId = chelsea.Id };
                players[i++] = new Player { Id = 55, Name = "Willy Caballero", DateOfBirth = new DateTime(1981, 9, 28), Position = "Goalkeeper", Number = 13, Goals = 0, Assists = 0, Matches = 2, Nationality = "Argentina", MarketValue = 500000, TeamId = chelsea.Id };
                players[i++] = new Player { Id = 56, Name = "Antonio Rüdiger", DateOfBirth = new DateTime(1993, 3, 3), Position = "Centre-Back", Number = 2, Goals = 0, Assists = 0, Matches = 3, Nationality = "Germany", MarketValue = 30000000, TeamId = chelsea.Id };
                players[i++] = new Player { Id = 57, Name = "Kurt Zouma", DateOfBirth = new DateTime(1994, 10, 27), Position = "Centre-Back", Number = 15, Goals = 3, Assists = 0, Matches = 14, Nationality = "France", MarketValue = 30000000, TeamId = chelsea.Id };
                players[i++] = new Player { Id = 58, Name = "Andreas Christensen", DateOfBirth = new DateTime(1996, 4, 10), Position = "Centre-Back", Number = 4, Goals = 0, Assists = 0, Matches = 4, Nationality = "Denmark", MarketValue = 24000000, TeamId = chelsea.Id };
                players[i++] = new Player { Id = 59, Name = "Fikayo Tomori", DateOfBirth = new DateTime(1997, 12, 19), Position = "Centre-Back", Number = 14, Goals = 0, Assists = 0, Matches = 3, Nationality = "England", MarketValue = 20000000, TeamId = chelsea.Id };
                players[i++] = new Player { Id = 60, Name = "Thiago Silva", DateOfBirth = new DateTime(1984, 9, 11), Position = "Centre-Back", Number = 6, Goals = 1, Assists = 0, Matches = 9, Nationality = "Brazil", MarketValue = 4500000, TeamId = chelsea.Id };
                players[i++] = new Player { Id = 61, Name = "Ben Chillwell", DateOfBirth = new DateTime(1996, 12, 21), Position = "Left-Back", Number = 21, Goals = 2, Assists = 3, Matches = 12, Nationality = "England", MarketValue = 50000000, TeamId = chelsea.Id };
                players[i++] = new Player { Id = 62, Name = "Marcos Alonso", DateOfBirth = new DateTime(1990, 12, 28), Position = "Left-Back", Number = 3, Goals = 0, Assists = 0, Matches = 3, Nationality = "Spain", MarketValue = 18000000, TeamId = chelsea.Id };
                players[i++] = new Player { Id = 63, Name = "Emerson", DateOfBirth = new DateTime(1994, 8, 3), Position = "Left-Back", Number = 33, Goals = 0, Assists = 0, Matches = 5, Nationality = "Italian", MarketValue = 18000000, TeamId = chelsea.Id };
                players[i++] = new Player { Id = 64, Name = "Reece James", DateOfBirth = new DateTime(1990, 12, 8), Position = "Right-Back", Number = 24, Goals = 1, Assists = 2, Matches = 11, Nationality = "England", MarketValue = 30000000, TeamId = chelsea.Id };
                players[i++] = new Player { Id = 65, Name = "César Azpilicueta", DateOfBirth = new DateTime(1989, 8, 28), Position = "Right-Back", Number = 28, Goals = 0, Assists = 1, Matches = 9, Nationality = "Spain", MarketValue = 20000000, TeamId = chelsea.Id };
                players[i++] = new Player { Id = 66, Name = "Jorginho", DateOfBirth = new DateTime(1991, 12, 20), Position = "Defensive Midfield", Number = 5, Goals = 3, Assists = 1, Matches = 12, Nationality = "Italian", MarketValue = 50000000, TeamId = chelsea.Id };
                players[i++] = new Player { Id = 67, Name = "N'Golo Kanté", DateOfBirth = new DateTime(1991, 3, 29), Position = "Central Midfield", Number = 7, Goals = 0, Assists = 1, Matches = 14, Nationality = "France", MarketValue = 70000000, TeamId = chelsea.Id };
                players[i++] = new Player { Id = 68, Name = "Mateo Kovacic", DateOfBirth = new DateTime(1994, 5, 6), Position = "Central Midfield", Number = 17, Goals = 0, Assists = 1, Matches = 11, Nationality = "Croatia", MarketValue = 45000000, TeamId = chelsea.Id };
                players[i++] = new Player { Id = 69, Name = "Billy Gilmour", DateOfBirth = new DateTime(2001, 6, 11), Position = "Central Midfield", Number = 23, Goals = 0, Assists = 0, Matches = 0, Nationality = "Scotland", MarketValue = 7500000, TeamId = chelsea.Id };
                players[i++] = new Player { Id = 70, Name = "Kai Havertz", DateOfBirth = new DateTime(1999, 6, 11), Position = "Attacking Midfield", Number = 29, Goals = 4, Assists = 3, Matches = 11, Nationality = "Germany", MarketValue = 81000000, TeamId = chelsea.Id };
                players[i++] = new Player { Id = 71, Name = "Mason Mount", DateOfBirth = new DateTime(1999, 1, 10), Position = "Attacking Midfield", Number = 19, Goals = 1, Assists = 4, Matches = 14, Nationality = "England", MarketValue = 45000000, TeamId = chelsea.Id };
                players[i++] = new Player { Id = 72, Name = "Christian Pulisic", DateOfBirth = new DateTime(1998, 9, 18), Position = "Left Winger", Number = 10, Goals = 1, Assists = 0, Matches = 5, Nationality = "United States", MarketValue = 60000000, TeamId = chelsea.Id };
                players[i++] = new Player { Id = 73, Name = "Callum Hudson-Odoi", DateOfBirth = new DateTime(2000, 11, 7), Position = "Left Winger", Number = 20, Goals = 3, Assists = 0, Matches = 11, Nationality = "England", MarketValue = 35000000, TeamId = chelsea.Id };
                players[i++] = new Player { Id = 74, Name = "Hakim Ziyech", DateOfBirth = new DateTime(1993, 3, 19), Position = "Right Winger", Number = 22, Goals = 2, Assists = 3, Matches = 9, Nationality = "Morocco", MarketValue = 40000000, TeamId = chelsea.Id };
                players[i++] = new Player { Id = 75, Name = "Timo Werner", DateOfBirth = new DateTime(1996, 3, 6), Position = "Centre-Forward", Number = 11, Goals = 8, Assists = 4, Matches = 14, Nationality = "Germany", MarketValue = 70000000, TeamId = chelsea.Id };
                players[i++] = new Player { Id = 76, Name = "Tammy Abraham", DateOfBirth = new DateTime(1997, 10, 2), Position = "Centre-Forward", Number = 9, Goals = 5, Assists = 5, Matches = 14, Nationality = "England", MarketValue = 40000000, TeamId = chelsea.Id };
                players[i++] = new Player { Id = 77, Name = "Oliver Giroud", DateOfBirth = new DateTime(1986, 9, 30), Position = "Centre-Forward", Number = 18, Goals = 2, Assists = 0, Matches = 8, Nationality = "France", MarketValue = 5000000, TeamId = chelsea.Id };

                players[i++] = new Player { Id = 78, Name = "David de Gea", DateOfBirth = new DateTime(1990, 11, 7), Position = "Goalkeeper", Number = 1, Goals = 0, Assists = 0, Matches = 11, Nationality = "Spain", MarketValue = 35000000, TeamId = manUtd.Id };
                players[i++] = new Player { Id = 79, Name = "Dean Henderson", DateOfBirth = new DateTime(1997, 3, 12), Position = "Goalkeeper", Number = 26, Goals = 0, Assists = 0, Matches = 3, Nationality = "England", MarketValue = 20000000, TeamId = manUtd.Id };
                players[i++] = new Player { Id = 80, Name = "Sergio Romero", DateOfBirth = new DateTime(1987, 2, 22), Position = "Goalkeeper", Number = 22, Goals = 0, Assists = 0, Matches = 0, Nationality = "Argentina", MarketValue = 2000000, TeamId = manUtd.Id };
                players[i++] = new Player { Id = 81, Name = "Lee Grant", DateOfBirth = new DateTime(1983, 1, 27), Position = "Goalkeeper", Number = 13, Goals = 0, Assists = 0, Matches = 0, Nationality = "England", MarketValue = 350000, TeamId = manUtd.Id };
                players[i++] = new Player { Id = 82, Name = "Harry Maguire", DateOfBirth = new DateTime(1993, 3, 5), Position = "Centre-Back", Number = 5, Goals = 1, Assists = 0, Matches = 12, Nationality = "England", MarketValue = 50000000, TeamId = manUtd.Id };
                players[i++] = new Player { Id = 83, Name = "Victor Lindelöf", DateOfBirth = new DateTime(1994, 7, 17), Position = "Centre-Back", Number = 2, Goals = 0, Assists = 0, Matches = 11, Nationality = "Sweden", MarketValue = 24000000, TeamId = manUtd.Id };
                players[i++] = new Player { Id = 84, Name = "Eric Bailly", DateOfBirth = new DateTime(1994, 4, 12), Position = "Centre-Back", Number = 3, Goals = 0, Assists = 0, Matches = 4, Nationality = "Cote d'Ivorie", MarketValue = 17500000, TeamId = manUtd.Id };
                players[i++] = new Player { Id = 85, Name = "Axel Tuanzebe", DateOfBirth = new DateTime(1997, 11, 14), Position = "Centre-Back", Number = 38, Goals = 0, Assists = 0, Matches = 5, Nationality = "England", MarketValue = 8000000, TeamId = manUtd.Id };
                players[i++] = new Player { Id = 86, Name = "Phil Jones", DateOfBirth = new DateTime(1992, 2, 21), Position = "Centre-Back", Number = 4, Goals = 0, Assists = 0, Matches = 0, Nationality = "England", MarketValue = 6000000, TeamId = manUtd.Id };
                players[i++] = new Player { Id = 87, Name = "Marcos Rojo", DateOfBirth = new DateTime(1990, 3, 20), Position = "Centre-Back", Number = 16, Goals = 0, Assists = 0, Matches = 0, Nationality = "Argentina", MarketValue = 6000000, TeamId = manUtd.Id };
                players[i++] = new Player { Id = 88, Name = "Alex Telles", DateOfBirth = new DateTime(1992, 12, 15), Position = "Left-Back", Number = 27, Goals = 0, Assists = 0, Matches = 3, Nationality = "Brazil", MarketValue = 40000000, TeamId = manUtd.Id };
                players[i++] = new Player { Id = 89, Name = "Luke Shaw", DateOfBirth = new DateTime(1995, 7, 12), Position = "Left-Back", Number = 23, Goals = 0, Assists = 2, Matches = 10, Nationality = "England", MarketValue = 22000000, TeamId = manUtd.Id };
                players[i++] = new Player { Id = 90, Name = "Brandon Williams", DateOfBirth = new DateTime(2000, 9, 3), Position = "Left-Back", Number = 33, Goals = 0, Assists = 1, Matches = 3, Nationality = "England", MarketValue = 11000000, TeamId = manUtd.Id };
                players[i++] = new Player { Id = 91, Name = "Aaron Wan-Bissaka", DateOfBirth = new DateTime(1997, 11, 26), Position = "Right-Back", Number = 29, Goals = 1, Assists = 0, Matches = 12, Nationality = "England", MarketValue = 40000000, TeamId = manUtd.Id };
                players[i++] = new Player { Id = 92, Name = "Timothy Fosu-Mensah", DateOfBirth = new DateTime(1998, 1, 2), Position = "Right-Back", Number = 24, Goals = 0, Assists = 0, Matches = 2, Nationality = "Netherlands", MarketValue = 6300000, TeamId = manUtd.Id };
                players[i++] = new Player { Id = 93, Name = "Nemanja Matic", DateOfBirth = new DateTime(1988, 8, 1), Position = "Defensive Midfield", Number = 31, Goals = 0, Assists = 0, Matches = 9, Nationality = "Serbia", MarketValue = 16000000, TeamId = manUtd.Id };
                players[i++] = new Player { Id = 94, Name = "Paul Pogba", DateOfBirth = new DateTime(1993, 3, 15), Position = "Central Midfield", Number = 6, Goals = 1, Assists = 2, Matches = 11, Nationality = "France", MarketValue = 80000000, TeamId = manUtd.Id };
                players[i++] = new Player { Id = 95, Name = "Donny van de Beek", DateOfBirth = new DateTime(1997, 4, 18), Position = "Central Midfield", Number = 34, Goals = 1, Assists = 1, Matches = 12, Nationality = "Netherlands", MarketValue = 44000000, TeamId = manUtd.Id };
                players[i++] = new Player { Id = 96, Name = "Scott Mctominay", DateOfBirth = new DateTime(1996, 12, 8), Position = "Central Midfield", Number = 39, Goals = 1, Assists = 0, Matches = 11, Nationality = "Scotland", MarketValue = 22500000, TeamId = manUtd.Id };
                players[i++] = new Player { Id = 97, Name = "Fred", DateOfBirth = new DateTime(1993, 3, 5), Position = "Central Midfield", Number = 17, Goals = 0, Assists = 0, Matches = 12, Nationality = "Brazil", MarketValue = 17500000, TeamId = manUtd.Id };
                players[i++] = new Player { Id = 98, Name = "Bruno Fernandes", DateOfBirth = new DateTime(1994, 9, 8), Position = "Attacking Midfield", Number = 18, Goals = 9, Assists = 5, Matches = 13, Nationality = "Portugal", MarketValue = 80000000, TeamId = manUtd.Id };
                players[i++] = new Player { Id = 99, Name = "Jesse Lingard", DateOfBirth = new DateTime(1992, 12, 15), Position = "Attacking Midfield", Number = 14, Goals = 0, Assists = 0, Matches = 2, Nationality = "England", MarketValue = 14000000, TeamId = manUtd.Id };
                players[i++] = new Player { Id = 100, Name = "Juan Mata", DateOfBirth = new DateTime(1988, 4, 28), Position = "Attacking Midfield", Number = 8, Goals = 2, Assists = 2, Matches = 7, Nationality = "Spain", MarketValue = 7500000, TeamId = manUtd.Id };
                players[i++] = new Player { Id = 101, Name = "Marcus Rashford", DateOfBirth = new DateTime(1997, 10, 31), Position = "Left Winger", Number = 10, Goals = 8, Assists = 3, Matches = 14, Nationality = "England", MarketValue = 80000000, TeamId = manUtd.Id };
                players[i++] = new Player { Id = 102, Name = "Daniel James", DateOfBirth = new DateTime(1997, 11, 10), Position = "Left Winger", Number = 21, Goals = 1, Assists = 0, Matches = 6, Nationality = "Wales", MarketValue = 16000000, TeamId = manUtd.Id };
                players[i++] = new Player { Id = 103, Name = "Mason Greenwood", DateOfBirth = new DateTime(2001, 10, 1), Position = "Right Winger", Number = 11, Goals = 2, Assists = 2, Matches = 9, Nationality = "England", MarketValue = 50000000, TeamId = manUtd.Id };
                players[i++] = new Player { Id = 104, Name = "Facundo Pellistri", DateOfBirth = new DateTime(2001, 12, 20), Position = "Right Winger", Number = 28, Goals = 0, Assists = 0, Matches = 0, Nationality = "Uruguay", MarketValue = 7000000, TeamId = manUtd.Id };
                players[i++] = new Player { Id = 105, Name = "Anthony Martial", DateOfBirth = new DateTime(1995, 12, 5), Position = "Centre-Forward", Number = 9, Goals = 2, Assists = 3, Matches = 9, Nationality = "France", MarketValue = 60000000, TeamId = manUtd.Id };
                players[i++] = new Player { Id = 106, Name = "Edinson Cavani", DateOfBirth = new DateTime(1987, 2, 14), Position = "Centre-Forward", Number = 7, Goals = 1, Assists = 0, Matches = 7, Nationality = "Uruguay", MarketValue = 12000000, TeamId = manUtd.Id };
                players[i++] = new Player { Id = 107, Name = "Odion Ighalo", DateOfBirth = new DateTime(1989, 6, 16), Position = "Centre-Forward", Number = 25, Goals = 0, Assists = 0, Matches = 3, Nationality = "Nigeria", MarketValue = 5200000, TeamId = manUtd.Id };

                players[i++] = new Player { Id = 108, Name = "Hugo Lloris", DateOfBirth = new DateTime(1986, 12, 26), Position = "Goalkeeper", Number = 1, Goals = 0, Assists = 0, Matches = 12, Nationality = "France", MarketValue = 12000000, TeamId = tottenham.Id };
                players[i++] = new Player { Id = 109, Name = "Paulo Gazzaniga", DateOfBirth = new DateTime(1992, 1, 2), Position = "Goalkeeper", Number = 22, Goals = 0, Assists = 0, Matches = 0, Nationality = "Argentina", MarketValue = 4000000, TeamId = tottenham.Id };
                players[i++] = new Player { Id = 110, Name = "Joe Hart", DateOfBirth = new DateTime(1987, 4, 19), Position = "Goalkeeper", Number = 12, Goals = 0, Assists = 0, Matches = 5, Nationality = "England", MarketValue = 2400000, TeamId = tottenham.Id };
                players[i++] = new Player { Id = 111, Name = "Alfie Whiteman", DateOfBirth = new DateTime(1998, 10, 2), Position = "Goalkeeper", Number = 41, Goals = 0, Assists = 0, Matches = 1, Nationality = "England", MarketValue = 750000, TeamId = tottenham.Id };
                players[i++] = new Player { Id = 112, Name = "Davinson Sánchez", DateOfBirth = new DateTime(1996, 6, 12), Position = "Centre-Back", Number = 6, Goals = 0, Assists = 0, Matches = 10, Nationality = "Colombia", MarketValue = 45000000, TeamId = tottenham.Id };
                players[i++] = new Player { Id = 113, Name = "Eric Dier", DateOfBirth = new DateTime(1994, 1, 15), Position = "Centre-Back", Number = 15, Goals = 0, Assists = 0, Matches = 11, Nationality = "England", MarketValue = 22000000, TeamId = tottenham.Id };
                players[i++] = new Player { Id = 114, Name = "Toby Alderweireld", DateOfBirth = new DateTime(1989, 3, 2), Position = "Centre-Back", Number = 4, Goals = 0, Assists = 0, Matches = 10, Nationality = "Belgium", MarketValue = 20000000, TeamId = tottenham.Id };
                players[i++] = new Player { Id = 115, Name = "Japhet Tangana", DateOfBirth = new DateTime(1999, 3, 31), Position = "Centre-Back", Number = 25, Goals = 0, Assists = 0, Matches = 2, Nationality = "England", MarketValue = 8000000, TeamId = tottenham.Id };
                players[i++] = new Player { Id = 116, Name = "Joe Rodon", DateOfBirth = new DateTime(1997, 10, 22), Position = "Centre-Back", Number = 14, Goals = 0, Assists = 0, Matches = 2, Nationality = "Wales", MarketValue = 7000000, TeamId = tottenham.Id };
                players[i++] = new Player { Id = 117, Name = "Sergio Reguilón", DateOfBirth = new DateTime(1996, 12, 16), Position = "Left-Back", Number = 3, Goals = 0, Assists = 3, Matches = 9, Nationality = "Spain", MarketValue = 28000000, TeamId = tottenham.Id };
                players[i++] = new Player { Id = 118, Name = "Ben Davies", DateOfBirth = new DateTime(1993, 4, 24), Position = "Left-Back", Number = 33, Goals = 0, Assists = 3, Matches = 13, Nationality = "Wales", MarketValue = 17500000, TeamId = tottenham.Id };
                players[i++] = new Player { Id = 119, Name = "Matt Doherty", DateOfBirth = new DateTime(1992, 1, 16), Position = "Right-Back", Number = 2, Goals = 0, Assists = 1, Matches = 11, Nationality = "Ireland", MarketValue = 20000000, TeamId = tottenham.Id };
                players[i++] = new Player { Id = 120, Name = "Serge Aurier", DateOfBirth = new DateTime(1992, 12, 24), Position = "Right-Back", Number = 24, Goals = 1, Assists = 1, Matches = 6, Nationality = "Cote d'Ivoire", MarketValue = 16000000, TeamId = tottenham.Id };
                players[i++] = new Player { Id = 121, Name = "Harry Winks", DateOfBirth = new DateTime(1996, 2, 2), Position = "Defensive Midfield", Number = 8, Goals = 1, Assists = 0, Matches = 10, Nationality = "England", MarketValue = 28000000, TeamId = tottenham.Id };
                players[i++] = new Player { Id = 122, Name = "Tanguy Ndombélé", DateOfBirth = new DateTime(1996, 12, 28), Position = "Central Midfield", Number = 28, Goals = 2, Assists = 1, Matches = 14, Nationality = "France", MarketValue = 40000000, TeamId = tottenham.Id };
                players[i++] = new Player { Id = 123, Name = "Giovani Lo Celso", DateOfBirth = new DateTime(1996, 4, 9), Position = "Central Midfield", Number = 18, Goals = 4, Assists = 0, Matches = 12, Nationality = "Argentina", MarketValue = 35000000, TeamId = tottenham.Id };
                players[i++] = new Player { Id = 124, Name = "Pierre-Emile Höjbjerg", DateOfBirth = new DateTime(1995, 8, 5), Position = "Central Midfield", Number = 5, Goals = 0, Assists = 1, Matches = 16, Nationality = "Denmark", MarketValue = 25000000, TeamId = tottenham.Id };
                players[i++] = new Player { Id = 125, Name = "Moussa Sissoko", DateOfBirth = new DateTime(1989, 8, 16), Position = "Central Midfield", Number = 17, Goals = 0, Assists = 0, Matches = 12, Nationality = "France", MarketValue = 18000000, TeamId = tottenham.Id };
                players[i++] = new Player { Id = 126, Name = "Gedson Fernandes", DateOfBirth = new DateTime(1999, 1, 9), Position = "Central Midfield", Number = 30, Goals = 0, Assists = 0, Matches = 1, Nationality = "Portugal", MarketValue = 13500000, TeamId = tottenham.Id };
                players[i++] = new Player { Id = 127, Name = "Dele Alli", DateOfBirth = new DateTime(1996, 4, 11), Position = "Attacking Midfield", Number = 20, Goals = 1, Assists = 1, Matches = 7, Nationality = "England", MarketValue = 52000000, TeamId = tottenham.Id };
                players[i++] = new Player { Id = 128, Name = "Heung-min Son", DateOfBirth = new DateTime(1992, 7, 8), Position = "Left Winger", Number = 7, Goals = 11, Assists = 5, Matches = 14, Nationality = "Korea, South", MarketValue = 75000000, TeamId = tottenham.Id };
                players[i++] = new Player { Id = 129, Name = "Steven Bergwijn", DateOfBirth = new DateTime(1997, 10, 8), Position = "Left Winger", Number = 23, Goals = 0, Assists = 2, Matches = 11, Nationality = "Netherlands", MarketValue = 28000000, TeamId = tottenham.Id };
                players[i++] = new Player { Id = 130, Name = "Gareth Bale", DateOfBirth = new DateTime(1989, 7, 16), Position = "Right Winger", Number = 9, Goals = 1, Assists = 1, Matches = 7, Nationality = "Wales", MarketValue = 25000000, TeamId = tottenham.Id };
                players[i++] = new Player { Id = 131, Name = "Erik Lamela", DateOfBirth = new DateTime(1992, 3, 4), Position = "Right Winger", Number = 11, Goals = 2, Assists = 0, Matches = 10, Nationality = "Argentina", MarketValue = 16000000, TeamId = tottenham.Id };
                players[i++] = new Player { Id = 132, Name = "Jack Clarke", DateOfBirth = new DateTime(2000, 11, 23), Position = "Right Winger", Number = 47, Goals = 0, Assists = 0, Matches = 2, Nationality = "England", MarketValue = 5400000, TeamId = tottenham.Id };
                players[i++] = new Player { Id = 133, Name = "Harry Kane", DateOfBirth = new DateTime(1993, 7, 28), Position = "Centre-Forward", Number = 10, Goals = 13, Assists = 11, Matches = 15, Nationality = "England", MarketValue = 120000000, TeamId = tottenham.Id };
                players[i++] = new Player { Id = 134, Name = "Carlos Vinícius", DateOfBirth = new DateTime(1995, 3, 25), Position = "Centre-Forward", Number = 45, Goals = 2, Assists = 3, Matches = 5, Nationality = "Brazil", MarketValue = 20000000, TeamId = tottenham.Id };

                players[i++] = new Player { Id = 135, Name = "Manuel Neuer", DateOfBirth = new DateTime(1986, 3, 27), Position = "Goalkeeper", Number = 1, Goals = 0, Assists = 0, Matches = 15, Nationality = "Germany", MarketValue = 18000000, TeamId = bayern.Id };
                players[i++] = new Player { Id = 136, Name = "David Alaba", DateOfBirth = new DateTime(1992, 6, 24), Position = "Centre-Back", Number = 27, Goals = 1, Assists = 1, Matches = 13, Nationality = "Austria", MarketValue = 65000000, TeamId = bayern.Id };
                players[i++] = new Player { Id = 137, Name = "Joshua Kimmich", DateOfBirth = new DateTime(1995, 2, 8), Position = "Defensive Midfield", Number = 6, Goals = 3, Assists = 7, Matches = 11, Nationality = "Germany", MarketValue = 85000000, TeamId = bayern.Id };
                players[i++] = new Player { Id = 138, Name = "Serge Gnabry", DateOfBirth = new DateTime(1995, 7, 14), Position = "Right Winger", Number = 7, Goals = 4, Assists = 1, Matches = 13, Nationality = "Germany", MarketValue = 90000000, TeamId = bayern.Id };

                players[i++] = new Player { Id = 139, Name = "Roman Bürki", DateOfBirth = new DateTime(1990, 11, 14), Position = "Goalkeeper", Number = 1, Goals = 0, Assists = 0, Matches = 10, Nationality = "Switzerland", MarketValue = 11000000, TeamId = dortmund.Id };
                players[i++] = new Player { Id = 140, Name = "Manuel Akanji", DateOfBirth = new DateTime(1995, 7, 19), Position = "Centre-Back", Number = 16, Goals = 1, Assists = 0, Matches = 13, Nationality = "Switzerland", MarketValue = 25000000, TeamId = dortmund.Id };
                players[i++] = new Player { Id = 141, Name = "Julian Brandt", DateOfBirth = new DateTime(1996, 5, 2), Position = "Attacking Midfield", Number = 19, Goals = 1, Assists = 1, Matches = 15, Nationality = "Germany", MarketValue = 35000000, TeamId = dortmund.Id };
                players[i++] = new Player { Id = 142, Name = "Erling Haaland", DateOfBirth = new DateTime(2000, 7, 21), Position = "Centre-Forward", Number = 9, Goals = 17, Assists = 3, Matches = 14, Nationality = "Norway", MarketValue = 100000000, TeamId = dortmund.Id };

                players[i++] = new Player { Id = 143, Name = "Péter Gulácsi", DateOfBirth = new DateTime(1990, 5, 6), Position = "Goalkeeper", Number = 1, Goals = 0, Assists = 0, Matches = 14, Nationality = "Hungary", MarketValue = 11000000, TeamId = leipzig.Id };
                players[i++] = new Player { Id = 144, Name = "Dayot Upamecano", DateOfBirth = new DateTime(1998, 10, 27), Position = "Centre-Back", Number = 5, Goals = 1, Assists = 0, Matches = 13, Nationality = "France", MarketValue = 60000000, TeamId = leipzig.Id };
                players[i++] = new Player { Id = 145, Name = "Dani Olmo", DateOfBirth = new DateTime(1998, 5, 7), Position = "Attacking Midfield", Number = 25, Goals = 0, Assists = 5, Matches = 14, Nationality = "Spain", MarketValue = 33000000, TeamId = leipzig.Id };
                players[i++] = new Player { Id = 146, Name = "Marcel Sabitzer", DateOfBirth = new DateTime(1994, 3, 17), Position = "Right Winger", Number = 7, Goals = 2, Assists = 1, Matches = 9, Nationality = "Austria", MarketValue = 35000000, TeamId = leipzig.Id };

                players[i++] = new Player { Id = 147, Name = "Lukas Hradecky", DateOfBirth = new DateTime(1989, 11, 24), Position = "Goalkeeper", Number = 1, Goals = 0, Assists = 0, Matches = 14, Nationality = "Finland", MarketValue = 8000000, TeamId = leverkusen.Id };
                players[i++] = new Player { Id = 148, Name = "Edmond Tapsoba", DateOfBirth = new DateTime(1999, 2, 2), Position = "Centre-Back", Number = 12, Goals = 0, Assists = 0, Matches = 9, Nationality = "Burkina Faso", MarketValue = 30000000, TeamId = leverkusen.Id };
                players[i++] = new Player { Id = 149, Name = "Florian Wirtz", DateOfBirth = new DateTime(2003, 5, 3), Position = "Attacking Midfield", Number = 27, Goals = 3, Assists = 5, Matches = 13, Nationality = "Germany", MarketValue = 24000000, TeamId = leverkusen.Id };
                players[i++] = new Player { Id = 150, Name = "Mouassa Diaby", DateOfBirth = new DateTime(1999, 7, 7), Position = "Left Winger", Number = 19, Goals = 2, Assists = 4, Matches = 14, Nationality = "France", MarketValue = 36000000, TeamId = leverkusen.Id };

                players[i++] = new Player { Id = 151, Name = "Yann Sommer", DateOfBirth = new DateTime(1988, 12, 17), Position = "Goalkeeper", Number = 1, Goals = 0, Assists = 0, Matches = 13, Nationality = "Switzerland", MarketValue = 9500000, TeamId = gladbach.Id };
                players[i++] = new Player { Id = 152, Name = "Matthias Ginter", DateOfBirth = new DateTime(1994, 1, 19), Position = "Centre-Back", Number = 28, Goals = 1, Assists = 0, Matches = 14, Nationality = "Germany", MarketValue = 35000000, TeamId = gladbach.Id };
                players[i++] = new Player { Id = 153, Name = "Denis Zakaria", DateOfBirth = new DateTime(1996, 11, 20), Position = "Defensive Midfield", Number = 8, Goals = 0, Assists = 0, Matches = 3, Nationality = "Switzerland", MarketValue = 40000000, TeamId = gladbach.Id };
                players[i++] = new Player { Id = 154, Name = "Marcus Thuram", DateOfBirth = new DateTime(1997, 8, 6), Position = "Left Winger", Number = 10, Goals = 4, Assists = 6, Matches = 14, Nationality = "France", MarketValue = 40000000, TeamId = gladbach.Id };

                players[i++] = new Player { Id = 155, Name = "Marc-André ter Stegen", DateOfBirth = new DateTime(1992, 4, 30), Position = "Goalkeeper", Number = 1, Goals = 0, Assists = 0, Matches = 5, Nationality = "Germany", MarketValue = 75000000, TeamId = barcelona.Id };
                players[i++] = new Player { Id = 156, Name = "Clément Lenglet", DateOfBirth = new DateTime(1995, 6, 17), Position = "Centre-Back", Number = 15, Goals = 0, Assists = 0, Matches = 13, Nationality = "France", MarketValue = 50000000, TeamId = barcelona.Id };
                players[i++] = new Player { Id = 157, Name = "Frenkie de Jong", DateOfBirth = new DateTime(1997, 5, 12), Position = "Central Midfield", Number = 21, Goals = 0, Assists = 1, Matches = 13, Nationality = "Netherlands", MarketValue = 70000000, TeamId = barcelona.Id };
                players[i++] = new Player { Id = 158, Name = "Lionel Messi", DateOfBirth = new DateTime(1987, 6, 24), Position = "Right Winger", Number = 10, Goals = 7, Assists = 4, Matches = 12, Nationality = "Argentina", MarketValue = 100000000, TeamId = barcelona.Id };

                players[i++] = new Player { Id = 159, Name = "Thibaut Courtois", DateOfBirth = new DateTime(1992, 5, 11), Position = "Goalkeeper", Number = 1, Goals = 0, Assists = 0, Matches = 15, Nationality = "Belgium", MarketValue = 75000000, TeamId = realMadrid.Id };
                players[i++] = new Player { Id = 160, Name = "Raphaël Varane", DateOfBirth = new DateTime(1993, 4, 25), Position = "Centre-Back", Number = 5, Goals = 0, Assists = 0, Matches = 14, Nationality = "France", MarketValue = 70000000, TeamId = realMadrid.Id };
                players[i++] = new Player { Id = 161, Name = "Federico Valverde", DateOfBirth = new DateTime(1998, 7, 22), Position = "Central Midfield", Number = 15, Goals = 3, Assists = 1, Matches = 11, Nationality = "Uruguay", MarketValue = 70000000, TeamId = realMadrid.Id };
                players[i++] = new Player { Id = 162, Name = "Eden Hazard", DateOfBirth = new DateTime(1991, 1, 7), Position = "Left Winger", Number = 7, Goals = 2, Assists = 0, Matches = 6, Nationality = "Belgium", MarketValue = 60000000, TeamId = realMadrid.Id };

                players[i++] = new Player { Id = 163, Name = "Jan Oblak", DateOfBirth = new DateTime(1993, 1, 7), Position = "Goalkeeper", Number = 13, Goals = 0, Assists = 0, Matches = 14, Nationality = "Slovenia", MarketValue = 90000000, TeamId = atleticoMadrid.Id };
                players[i++] = new Player { Id = 164, Name = "José Giménez", DateOfBirth = new DateTime(1995, 1, 20), Position = "Centre-Back", Number = 2, Goals = 1, Assists = 0, Matches = 7, Nationality = "Uruguay", MarketValue = 70000000, TeamId = atleticoMadrid.Id };
                players[i++] = new Player { Id = 165, Name = "Saúl Ñíguez", DateOfBirth = new DateTime(1994, 11, 21), Position = "Central Midfield", Number = 8, Goals = 0, Assists = 0, Matches = 9, Nationality = "Spain", MarketValue = 70000000, TeamId = atleticoMadrid.Id };
                players[i++] = new Player { Id = 166, Name = "João Félix", DateOfBirth = new DateTime(1999, 11, 10), Position = "Second Striker", Number = 7, Goals = 8, Assists = 3, Matches = 14, Nationality = "Portugal", MarketValue = 90000000, TeamId = atleticoMadrid.Id };

                players[i++] = new Player { Id = 167, Name = "Bono", DateOfBirth = new DateTime(1991, 4, 5), Position = "Goalkeeper", Number = 13, Goals = 0, Assists = 0, Matches = 9, Nationality = "Morocco", MarketValue = 15000000, TeamId = sevilla.Id };
                players[i++] = new Player { Id = 168, Name = "Diego Carlos", DateOfBirth = new DateTime(1993, 3, 15), Position = "Centre-Back", Number = 20, Goals = 0, Assists = 0, Matches = 15, Nationality = "Brazil", MarketValue = 50000000, TeamId = sevilla.Id };
                players[i++] = new Player { Id = 169, Name = "Joan Jordán", DateOfBirth = new DateTime(1994, 7, 6), Position = "Central Midfield", Number = 8, Goals = 0, Assists = 2, Matches = 14, Nationality = "Spain", MarketValue = 20000000, TeamId = sevilla.Id };
                players[i++] = new Player { Id = 170, Name = "Lucas Ocampos", DateOfBirth = new DateTime(1994, 7, 11), Position = "Right Winger", Number = 5, Goals = 2, Assists = 2, Matches = 15, Nationality = "Argentina", MarketValue = 50000000, TeamId = sevilla.Id };

                players[i++] = new Player { Id = 171, Name = "Álex Remiro", DateOfBirth = new DateTime(1995, 3, 24), Position = "Goalkeeper", Number = 1, Goals = 0, Assists = 0, Matches = 15, Nationality = "Spain", MarketValue = 20000000, TeamId = realSociedad.Id };
                players[i++] = new Player { Id = 172, Name = "Aritz Elustundo", DateOfBirth = new DateTime(1994, 3, 28), Position = "Centre-Back", Number = 6, Goals = 0, Assists = 0, Matches = 13, Nationality = "Spain", MarketValue = 15000000, TeamId = realSociedad.Id };
                players[i++] = new Player { Id = 173, Name = "Mikel Merino", DateOfBirth = new DateTime(1996, 6, 22), Position = "Central Midfield", Number = 8, Goals = 1, Assists = 4, Matches = 15, Nationality = "Spain", MarketValue = 35000000, TeamId = realSociedad.Id };
                players[i++] = new Player { Id = 174, Name = "Mikel Oyarzabal", DateOfBirth = new DateTime(1997, 4, 21), Position = "Left Winger", Number = 10, Goals = 7, Assists = 3, Matches = 16, Nationality = "Spain", MarketValue = 60000000, TeamId = realSociedad.Id };

                modelBuilder.Entity<League>().HasData(leagues);
                modelBuilder.Entity<Stadium>().HasData(stadiums);
                modelBuilder.Entity<Team>().HasData(teams);
                modelBuilder.Entity<Player>().HasData(players);
            }
        }
    }
}
