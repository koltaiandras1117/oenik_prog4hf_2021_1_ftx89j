﻿// <copyright file="League.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Globalization;
    using System.Text;

    /// <summary>
    /// A class that contains information of a league.
    /// </summary>
    [Table("leagues")]
    public class League
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="League"/> class.
        /// </summary>
        public League()
        {
            this.Teams = new HashSet<Team>();
        }

        /// <summary>
        /// Gets or sets the primary key of a League.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the official name of a leauge.
        /// </summary>
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the current season of a league.
        /// </summary>
        [Required]
        [MaxLength(9)]
        public string Season { get; set; }

        /// <summary>
        /// Gets or sets the average value of the players playing in this league.
        /// </summary>
        [MaxLength(9)]
        public int AverageValue { get; set; }

        /// <summary>
        /// Gets or sets the country where the league is located.
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string Country { get; set; }

        /// <summary>
        /// Gets or sets the division of a league.
        /// </summary>
        [Required]
        [MaxLength(2)]
        public int Division { get; set; }

        /// <summary>
        /// Gets which teams are playing in the league.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Team> Teams { get; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>True if the specified object is equal to the current object; otherwise, false.</returns>
        public override bool Equals(object obj)
        {
            return obj is League league &&
                   this.Id == league.Id &&
                   this.Name == league.Name &&
                   this.Season == league.Season &&
                   this.AverageValue == league.AverageValue &&
                   this.Country == league.Country &&
                   this.Division == league.Division;
        }

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            return this.Id;
        }

        /// <summary>
        /// Returns a string that represents the current League object.
        /// </summary>
        /// <returns>A string that represents the current League object. </returns>
        public override string ToString()
        {
            return this.Id.ToString("D", CultureInfo.CurrentCulture).PadRight(4) + "|" + this.Name.PadRight(30) + "|" + this.Season + "|" + this.Country.PadRight(30) + "|" + this.Division.ToString("D", CultureInfo.CurrentCulture).PadRight(8) + "|" + ((double)this.AverageValue / 1000000).ToString("F2", CultureInfo.CurrentCulture) + " Million Euro";
        }
    }
}
