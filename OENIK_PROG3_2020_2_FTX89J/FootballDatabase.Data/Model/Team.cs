﻿// <copyright file="Team.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Globalization;
    using System.Text;

    /// <summary>
    /// A class that contain information about a professional football team.
    /// </summary>
    [Table("teams")]
    public class Team
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Team"/> class.
        /// </summary>
        public Team()
        {
            this.Players = new HashSet<Player>();
        }

        /// <summary>
        /// Gets or sets the primary key of a team.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets a team's name.
        /// </summary>
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the color of a team's home shirt.
        /// </summary>
        [Required]
        [MaxLength(30)]
        public string HomeShirt { get; set; }

        /// <summary>
        /// Gets or sets the color of a team's aways shirt.
        /// </summary>
        [Required]
        [MaxLength(30)]
        public string AwayShirt { get; set; }

        /// <summary>
        /// Gets or sets the year when the team was established.
        /// </summary>
        [MaxLength(4)]
        public int? YearOfEstablishment { get; set; }

        /// <summary>
        /// Gets or sets how many leagues a team won so far.
        /// </summary>
        [Required]
        [MaxLength(3)]
        public int LeaguesWon { get; set; }

        /// <summary>
        /// Gets or sets the foreign key from the leagues table.
        /// </summary>
        [ForeignKey(nameof(League))]
        public int LeagueId { get; set; }

        /// <summary>
        /// Gets or sets the foreign key from the stadiums table.
        /// </summary>
        [ForeignKey(nameof(Stadium))]
        public int StadiumId { get; set; }

        /// <summary>
        /// Gets or sets the stadium where a team plays its home matches.
        /// </summary>
        [NotMapped]
        public virtual Stadium Stadium { get; set; }

        /// <summary>
        /// Gets or sets the league where the team is currently playing in.
        /// </summary>
        [NotMapped]
        public virtual League League { get; set; }

        /// <summary>
        /// Gets all the players that are playing for the team currently.
        /// </summary>
        public virtual ICollection<Player> Players { get; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>True if the specified object is equal to the current object; otherwise, false.</returns>
        public override bool Equals(object obj)
        {
            return obj is Team team &&
                   this.Id == team.Id &&
                   this.Name == team.Name &&
                   this.HomeShirt == team.HomeShirt &&
                   this.AwayShirt == team.AwayShirt &&
                   this.YearOfEstablishment == team.YearOfEstablishment &&
                   this.LeaguesWon == team.LeaguesWon &&
                   this.LeagueId == team.LeagueId &&
                   this.StadiumId == team.StadiumId;
        }

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            return this.Id;
        }

        /// <summary>
        /// Returns a string that represents the current Team object.
        /// </summary>
        /// <returns>A string that represents the current Team object. </returns>
        public override string ToString()
        {
            return this.Id.ToString("D", CultureInfo.CurrentCulture).PadRight(4) + "|" + this.Name.PadRight(30) + "|" + this.HomeShirt.PadRight(30) + "|" + this.AwayShirt.PadRight(30) + "|" + this.YearOfEstablishment.ToString().PadRight(11) + "|" + this.LeaguesWon.ToString("D", CultureInfo.CurrentCulture).PadRight(11) + "|" + this.Stadium.Name;
        }
    }
}
