﻿// <copyright file="Stadium.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballDatabase.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Globalization;
    using System.Text;

    /// <summary>
    /// A class that contains information of a football stadium.
    /// </summary>
    [Table("stadiums")]
    public class Stadium
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Stadium"/> class.
        /// </summary>
        public Stadium()
        {
            this.Teams = new HashSet<Team>();
        }

        /// <summary>
        /// Gets or sets the primary key of a stadium.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name of a stadium.
        /// </summary>
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the adress of a stadium.
        /// </summary>
        [Required]
        [MaxLength(100)]
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets the maximum capacity of a stadium.
        /// </summary>
        [Required]
        [MaxLength(6)]
        public int Capacity { get; set; }

        /// <summary>
        /// Gets or sets a phone number, where the stadium can officially be contacted.
        /// </summary>
        [MaxLength(20)]
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets the year when the stadium was built.
        /// </summary>
        [MaxLength(4)]
        public int? YearOfBuilt { get; set; }

        /// <summary>
        /// Gets which teams are currently playing at home in the stadium.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Team> Teams { get; }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>True if the specified object is equal to the current object; otherwise, false.</returns>
        public override bool Equals(object obj)
        {
            return obj is Stadium stadium &&
                   this.Id == stadium.Id &&
                   this.Name == stadium.Name &&
                   this.Address == stadium.Address &&
                   this.Capacity == stadium.Capacity &&
                   this.Phone == stadium.Phone &&
                   this.YearOfBuilt == stadium.YearOfBuilt;
        }

        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            return this.Id;
        }

        /// <summary>
        /// Adds a team to the Teams collection.
        /// </summary>
        /// <param name="team">The team which will be added to the collection.</param>
        public void SetTeams(Team team)
        {
            this.Teams.Add(team);
        }

        /// <summary>
        /// Returns a string that represents the current Stadium object.
        /// </summary>
        /// <returns>A string that represents the current Stadium object. </returns>
        public override string ToString()
        {
            return this.Id.ToString("D", CultureInfo.CurrentCulture).PadRight(4) + "|" + this.Name.PadRight(30) + "|" + this.Capacity.ToString(CultureInfo.CurrentCulture).PadRight(8) + "|" + this.YearOfBuilt.ToString().PadRight(5) + "|" + this.Address.PadRight(80) + "|" + this.Phone;
        }
    }
}
