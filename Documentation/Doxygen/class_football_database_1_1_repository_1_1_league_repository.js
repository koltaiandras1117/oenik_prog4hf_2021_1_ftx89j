var class_football_database_1_1_repository_1_1_league_repository =
[
    [ "LeagueRepository", "class_football_database_1_1_repository_1_1_league_repository.html#aadec057f5681f2bb8e9660c538db93e4", null ],
    [ "ChangeAverageValue", "class_football_database_1_1_repository_1_1_league_repository.html#a100bf0da9b946f76b4c416c90dc25686", null ],
    [ "ChangeCountry", "class_football_database_1_1_repository_1_1_league_repository.html#aeeb7143ea82eaa8e183e3daabaa66675", null ],
    [ "ChangeDivision", "class_football_database_1_1_repository_1_1_league_repository.html#a9a0ff888fe3fcead8be78049656243c8", null ],
    [ "ChangeName", "class_football_database_1_1_repository_1_1_league_repository.html#ada55ebb02959cbefefc5dbb7faf40c05", null ],
    [ "ChangeSeason", "class_football_database_1_1_repository_1_1_league_repository.html#a88f4bfd926a9de9928f05d38495b78ab", null ],
    [ "GetOne", "class_football_database_1_1_repository_1_1_league_repository.html#af584c7622d1e3a3ade500b02558074c6", null ]
];