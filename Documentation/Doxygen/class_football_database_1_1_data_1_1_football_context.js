var class_football_database_1_1_data_1_1_football_context =
[
    [ "FootballContext", "class_football_database_1_1_data_1_1_football_context.html#a55aa21fd673cee05a632f8f17feabef6", null ],
    [ "OnConfiguring", "class_football_database_1_1_data_1_1_football_context.html#a562d0f7df268a46a01fbffa4003b1990", null ],
    [ "OnModelCreating", "class_football_database_1_1_data_1_1_football_context.html#ab26ff4f93ac15626fcf9256db2b0d6eb", null ],
    [ "Leagues", "class_football_database_1_1_data_1_1_football_context.html#aef9d0e6a08f04835e3ecc25d65e1d1f9", null ],
    [ "Players", "class_football_database_1_1_data_1_1_football_context.html#a8ebbb652df9c2976a1cc22da12f3ad40", null ],
    [ "Stadiums", "class_football_database_1_1_data_1_1_football_context.html#a3d4eca5535fa1e6a2f461e7a43733c25", null ],
    [ "Teams", "class_football_database_1_1_data_1_1_football_context.html#a2f822e61bfc3bec50b1b29bee82be2de", null ]
];