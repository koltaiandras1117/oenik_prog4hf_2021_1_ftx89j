var namespace_football_database_1_1_repository =
[
    [ "ILeagueRepository", "interface_football_database_1_1_repository_1_1_i_league_repository.html", "interface_football_database_1_1_repository_1_1_i_league_repository" ],
    [ "IPlayerRepository", "interface_football_database_1_1_repository_1_1_i_player_repository.html", "interface_football_database_1_1_repository_1_1_i_player_repository" ],
    [ "IRepository", "interface_football_database_1_1_repository_1_1_i_repository.html", "interface_football_database_1_1_repository_1_1_i_repository" ],
    [ "IStadiumRepository", "interface_football_database_1_1_repository_1_1_i_stadium_repository.html", "interface_football_database_1_1_repository_1_1_i_stadium_repository" ],
    [ "ITeamRepository", "interface_football_database_1_1_repository_1_1_i_team_repository.html", "interface_football_database_1_1_repository_1_1_i_team_repository" ],
    [ "LeagueRepository", "class_football_database_1_1_repository_1_1_league_repository.html", "class_football_database_1_1_repository_1_1_league_repository" ],
    [ "PlayerRepository", "class_football_database_1_1_repository_1_1_player_repository.html", "class_football_database_1_1_repository_1_1_player_repository" ],
    [ "RepositoryClass", "class_football_database_1_1_repository_1_1_repository_class.html", "class_football_database_1_1_repository_1_1_repository_class" ],
    [ "StadiumRepository", "class_football_database_1_1_repository_1_1_stadium_repository.html", "class_football_database_1_1_repository_1_1_stadium_repository" ],
    [ "TeamRepository", "class_football_database_1_1_repository_1_1_team_repository.html", "class_football_database_1_1_repository_1_1_team_repository" ]
];