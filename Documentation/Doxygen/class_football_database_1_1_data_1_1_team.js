var class_football_database_1_1_data_1_1_team =
[
    [ "Team", "class_football_database_1_1_data_1_1_team.html#ae9a261f69f8cc414e43bfcbf5a0b6e74", null ],
    [ "Equals", "class_football_database_1_1_data_1_1_team.html#a6405baf9ed6f1889a3e7476c97d4ef65", null ],
    [ "GetHashCode", "class_football_database_1_1_data_1_1_team.html#a48e887acdf8746fa56b48011e82d50f1", null ],
    [ "ToString", "class_football_database_1_1_data_1_1_team.html#a49c4aa6520511ae5406fc8567c75022a", null ],
    [ "AwayShirt", "class_football_database_1_1_data_1_1_team.html#a6106d9a72b7b3a5089416711d1057aa8", null ],
    [ "HomeShirt", "class_football_database_1_1_data_1_1_team.html#a88f68162a48e8fbe502fa4036d127035", null ],
    [ "Id", "class_football_database_1_1_data_1_1_team.html#a47bad8397a40f47bbd1653917348dfcf", null ],
    [ "League", "class_football_database_1_1_data_1_1_team.html#af1a94a5117e76fcc4495309f2677cc1a", null ],
    [ "LeagueId", "class_football_database_1_1_data_1_1_team.html#add66161747756fc2e73e26bc7bad140a", null ],
    [ "LeaguesWon", "class_football_database_1_1_data_1_1_team.html#ae492c13b77c236a54cf90d2e763a1bb4", null ],
    [ "Name", "class_football_database_1_1_data_1_1_team.html#af42e7555b2f7dbdff35ca5bd49711140", null ],
    [ "Players", "class_football_database_1_1_data_1_1_team.html#a5b391cac3c66de37d1d6d1035e780fff", null ],
    [ "Stadium", "class_football_database_1_1_data_1_1_team.html#a5421a6fa939b7a43378a3d5344e21e17", null ],
    [ "StadiumId", "class_football_database_1_1_data_1_1_team.html#a5108fbbfac302f8998f932ceab8d6aca", null ],
    [ "YearOfEstablishment", "class_football_database_1_1_data_1_1_team.html#a58baff0a040eb01e7a255cee6a7048b1", null ]
];