var class_football_database_1_1_logic_1_1_below_average =
[
    [ "Equals", "class_football_database_1_1_logic_1_1_below_average.html#ae0ffbe7b04dee8d0dbcf9d098b6f38e0", null ],
    [ "GetHashCode", "class_football_database_1_1_logic_1_1_below_average.html#ad97e09dc235a4dd74d1f13e40bee372f", null ],
    [ "ToString", "class_football_database_1_1_logic_1_1_below_average.html#a76305570db66a9e3e356314630acab48", null ],
    [ "LeagueId", "class_football_database_1_1_logic_1_1_below_average.html#a740f3c06b5a1dbfb64267a15fd0d4f5b", null ],
    [ "LeagueName", "class_football_database_1_1_logic_1_1_below_average.html#abe5288b5ea71524cbc914cc0ec83e640", null ],
    [ "TeamId", "class_football_database_1_1_logic_1_1_below_average.html#adb758db6d5fe9135b58efd854dd5ca87", null ],
    [ "TeamName", "class_football_database_1_1_logic_1_1_below_average.html#af3cb5568ec12dcaf815e1746a41e2e05", null ]
];