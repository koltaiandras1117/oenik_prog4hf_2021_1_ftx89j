var class_football_database_1_1_wpf_client_1_1_player_v_m =
[
    [ "CopyFrom", "class_football_database_1_1_wpf_client_1_1_player_v_m.html#a90cefb2524f0364d118c46d80281b719", null ],
    [ "Assists", "class_football_database_1_1_wpf_client_1_1_player_v_m.html#a33d4d797790acd89de223002232e566d", null ],
    [ "DateOfBirth", "class_football_database_1_1_wpf_client_1_1_player_v_m.html#a0925e915d06759934ca054ae3b15d849", null ],
    [ "Goals", "class_football_database_1_1_wpf_client_1_1_player_v_m.html#a9b28f0259fcb8188b43650f3491159d1", null ],
    [ "Id", "class_football_database_1_1_wpf_client_1_1_player_v_m.html#ae23a09e8d4793ca434dac31a3550657c", null ],
    [ "MarketValue", "class_football_database_1_1_wpf_client_1_1_player_v_m.html#a2be1fe3e225f1727968ba4dc0b95c02e", null ],
    [ "Matches", "class_football_database_1_1_wpf_client_1_1_player_v_m.html#aefbc081009ec811a777734bb7e7a0249", null ],
    [ "Name", "class_football_database_1_1_wpf_client_1_1_player_v_m.html#a32a55979c9c83043dc62be832a27c918", null ],
    [ "Nationality", "class_football_database_1_1_wpf_client_1_1_player_v_m.html#a19f4ca43ba89ba7f97e210cd1a230286", null ],
    [ "Number", "class_football_database_1_1_wpf_client_1_1_player_v_m.html#a3cd35b63844e056a6c97d76d849faf9e", null ],
    [ "Position", "class_football_database_1_1_wpf_client_1_1_player_v_m.html#af15e677ae0ed87e2aa4e295663b936da", null ],
    [ "Team", "class_football_database_1_1_wpf_client_1_1_player_v_m.html#ab887ccab6cb6e0eef67d730d35c835c9", null ]
];