var namespace_football_database_1_1_logic =
[
    [ "Tests", "namespace_football_database_1_1_logic_1_1_tests.html", "namespace_football_database_1_1_logic_1_1_tests" ],
    [ "BelowAverage", "class_football_database_1_1_logic_1_1_below_average.html", "class_football_database_1_1_logic_1_1_below_average" ],
    [ "FinancialLogic", "class_football_database_1_1_logic_1_1_financial_logic.html", "class_football_database_1_1_logic_1_1_financial_logic" ],
    [ "IFinancialLogic", "interface_football_database_1_1_logic_1_1_i_financial_logic.html", "interface_football_database_1_1_logic_1_1_i_financial_logic" ],
    [ "IInfrastructuralLogic", "interface_football_database_1_1_logic_1_1_i_infrastructural_logic.html", "interface_football_database_1_1_logic_1_1_i_infrastructural_logic" ],
    [ "InfrastructuralLogic", "class_football_database_1_1_logic_1_1_infrastructural_logic.html", "class_football_database_1_1_logic_1_1_infrastructural_logic" ],
    [ "IStatisticalLogic", "interface_football_database_1_1_logic_1_1_i_statistical_logic.html", "interface_football_database_1_1_logic_1_1_i_statistical_logic" ],
    [ "ITransferLogic", "interface_football_database_1_1_logic_1_1_i_transfer_logic.html", "interface_football_database_1_1_logic_1_1_i_transfer_logic" ],
    [ "LeagueMaximumCapacity", "class_football_database_1_1_logic_1_1_league_maximum_capacity.html", "class_football_database_1_1_logic_1_1_league_maximum_capacity" ],
    [ "NoTeamException", "class_football_database_1_1_logic_1_1_no_team_exception.html", "class_football_database_1_1_logic_1_1_no_team_exception" ],
    [ "StatisticalLogic", "class_football_database_1_1_logic_1_1_statistical_logic.html", "class_football_database_1_1_logic_1_1_statistical_logic" ],
    [ "TransferLogic", "class_football_database_1_1_logic_1_1_transfer_logic.html", "class_football_database_1_1_logic_1_1_transfer_logic" ],
    [ "TransferResult", "class_football_database_1_1_logic_1_1_transfer_result.html", "class_football_database_1_1_logic_1_1_transfer_result" ]
];