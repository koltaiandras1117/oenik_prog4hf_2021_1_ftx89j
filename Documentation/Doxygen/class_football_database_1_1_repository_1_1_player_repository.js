var class_football_database_1_1_repository_1_1_player_repository =
[
    [ "PlayerRepository", "class_football_database_1_1_repository_1_1_player_repository.html#a972a2310423a427390b4e33f0f2b477a", null ],
    [ "ChangeAssists", "class_football_database_1_1_repository_1_1_player_repository.html#a639c8b4cc29514fd81fb604833a66099", null ],
    [ "ChangeBirthday", "class_football_database_1_1_repository_1_1_player_repository.html#ae9414e8b91d5cf4a06bfe0e9c6b61839", null ],
    [ "ChangeGoals", "class_football_database_1_1_repository_1_1_player_repository.html#a9b379fcc8974dc65b0a9be5b7f944037", null ],
    [ "ChangeMarketValue", "class_football_database_1_1_repository_1_1_player_repository.html#a6f08358b8193d062d4122482be97b595", null ],
    [ "ChangeMatches", "class_football_database_1_1_repository_1_1_player_repository.html#a7c0381903d452bb544405f65484c3706", null ],
    [ "ChangeName", "class_football_database_1_1_repository_1_1_player_repository.html#a73447c1091936b5a54c78628fe7ed202", null ],
    [ "ChangeNationality", "class_football_database_1_1_repository_1_1_player_repository.html#ac38d9e08599227f4e555f4bc2bcef003", null ],
    [ "ChangeNumber", "class_football_database_1_1_repository_1_1_player_repository.html#a77bbbf061d858222d26015738cb9dce9", null ],
    [ "ChangePosition", "class_football_database_1_1_repository_1_1_player_repository.html#a491dfffa69e0a7e68c489e23c4588af8", null ],
    [ "ChangeTeamId", "class_football_database_1_1_repository_1_1_player_repository.html#ae8d1ec671ca2a100af6895c040bda27f", null ],
    [ "GetOne", "class_football_database_1_1_repository_1_1_player_repository.html#a4e02530a5a07f922810ef218e179df09", null ]
];