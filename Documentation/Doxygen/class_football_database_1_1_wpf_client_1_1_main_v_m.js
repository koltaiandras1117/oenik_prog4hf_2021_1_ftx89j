var class_football_database_1_1_wpf_client_1_1_main_v_m =
[
    [ "MainVM", "class_football_database_1_1_wpf_client_1_1_main_v_m.html#a28731c222c994cb4259c336115c5df93", null ],
    [ "AddCmd", "class_football_database_1_1_wpf_client_1_1_main_v_m.html#abf37bd60889f9d176a721745b36d0b60", null ],
    [ "AllPlayers", "class_football_database_1_1_wpf_client_1_1_main_v_m.html#a3d9bd6f866709045ba8943d5830f15da", null ],
    [ "DelCmd", "class_football_database_1_1_wpf_client_1_1_main_v_m.html#adec4b9a5693bdf5b3ca8b9714dcc3a4a", null ],
    [ "EditorFunc", "class_football_database_1_1_wpf_client_1_1_main_v_m.html#a907f53485cd5f598bf409b9425125701", null ],
    [ "LoadCmd", "class_football_database_1_1_wpf_client_1_1_main_v_m.html#a5c78b372ce46060bf4ba6c2b7e3821cd", null ],
    [ "ModCmd", "class_football_database_1_1_wpf_client_1_1_main_v_m.html#ab31b72feeb2b62e8ea010545f1884bb7", null ],
    [ "SelectedPlayer", "class_football_database_1_1_wpf_client_1_1_main_v_m.html#ae26124c31251692ca3f1d7bed7409555", null ]
];