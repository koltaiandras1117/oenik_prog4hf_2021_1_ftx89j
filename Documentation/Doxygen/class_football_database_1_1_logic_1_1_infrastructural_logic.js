var class_football_database_1_1_logic_1_1_infrastructural_logic =
[
    [ "InfrastructuralLogic", "class_football_database_1_1_logic_1_1_infrastructural_logic.html#a4f24f2829a313b1158719746552859ae", null ],
    [ "ChangeStadiumCapacity", "class_football_database_1_1_logic_1_1_infrastructural_logic.html#af1e50b1edfe68941df261b1535d30f11", null ],
    [ "ChangeStadiumName", "class_football_database_1_1_logic_1_1_infrastructural_logic.html#af11f72c9c041914bd046ba7cd9a1e4c3", null ],
    [ "ChangeStadiumPhone", "class_football_database_1_1_logic_1_1_infrastructural_logic.html#a584c049d99bcd5e274a073175746b6fa", null ],
    [ "InsertStadium", "class_football_database_1_1_logic_1_1_infrastructural_logic.html#a41b0c03d76fcd0a94b8edc24d3edc24f", null ],
    [ "MaximumCapacity", "class_football_database_1_1_logic_1_1_infrastructural_logic.html#a4b0fa8190c63c04ad18700d9e3da2387", null ],
    [ "MaximumCapacityAsync", "class_football_database_1_1_logic_1_1_infrastructural_logic.html#aef10ee81bc3b8c8e6728b589bfcc0c50", null ],
    [ "RemoveStadium", "class_football_database_1_1_logic_1_1_infrastructural_logic.html#a369aab992aeac2da98f316515be90237", null ]
];