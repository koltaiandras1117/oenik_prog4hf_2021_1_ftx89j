var namespace_football_database_1_1_logic_1_1_tests =
[
    [ "FinancialLogicTests", "class_football_database_1_1_logic_1_1_tests_1_1_financial_logic_tests.html", "class_football_database_1_1_logic_1_1_tests_1_1_financial_logic_tests" ],
    [ "InfrastructuralLogicTests", "class_football_database_1_1_logic_1_1_tests_1_1_infrastructural_logic_tests.html", "class_football_database_1_1_logic_1_1_tests_1_1_infrastructural_logic_tests" ],
    [ "StatisticalLogicTests", "class_football_database_1_1_logic_1_1_tests_1_1_statistical_logic_tests.html", "class_football_database_1_1_logic_1_1_tests_1_1_statistical_logic_tests" ],
    [ "TransferLogicTests", "class_football_database_1_1_logic_1_1_tests_1_1_transfer_logic_tests.html", "class_football_database_1_1_logic_1_1_tests_1_1_transfer_logic_tests" ]
];