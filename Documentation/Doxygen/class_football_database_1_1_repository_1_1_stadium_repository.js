var class_football_database_1_1_repository_1_1_stadium_repository =
[
    [ "StadiumRepository", "class_football_database_1_1_repository_1_1_stadium_repository.html#a3d6f2fef2a98528b97c901442a9f6a68", null ],
    [ "ChangeCapacity", "class_football_database_1_1_repository_1_1_stadium_repository.html#aa66f4ea69e3e02b15b96a1115e35d20a", null ],
    [ "ChangeName", "class_football_database_1_1_repository_1_1_stadium_repository.html#a4fb8d5961a84fbbbd953e6df00ace7bd", null ],
    [ "ChangePhone", "class_football_database_1_1_repository_1_1_stadium_repository.html#a9ac13b8e261e48921f7ef3b7317eaa45", null ],
    [ "GetOne", "class_football_database_1_1_repository_1_1_stadium_repository.html#a5300d8616b6d4ee845a18ee89a329f78", null ]
];