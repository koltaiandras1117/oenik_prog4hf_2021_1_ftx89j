var class_football_database_1_1_u_i_1_1_b_l_1_1_league_w_p_f_logic =
[
    [ "LeagueWPFLogic", "class_football_database_1_1_u_i_1_1_b_l_1_1_league_w_p_f_logic.html#a5d4b9d4796065f637f66daf851c87d3e", null ],
    [ "AddLeague", "class_football_database_1_1_u_i_1_1_b_l_1_1_league_w_p_f_logic.html#ab3423ba7dad0adb599f19b54acede96f", null ],
    [ "DeleteLeague", "class_football_database_1_1_u_i_1_1_b_l_1_1_league_w_p_f_logic.html#aadf3a16766b9858173f480c9c540d4a0", null ],
    [ "GetAllLeagues", "class_football_database_1_1_u_i_1_1_b_l_1_1_league_w_p_f_logic.html#a449145fa0cc1eb6e35ec165a079341f3", null ],
    [ "ModifyLeague", "class_football_database_1_1_u_i_1_1_b_l_1_1_league_w_p_f_logic.html#a963f384746185f8eb683fbe5ba0d71a1", null ]
];