var interface_football_database_1_1_logic_1_1_i_infrastructural_logic =
[
    [ "ChangeStadiumCapacity", "interface_football_database_1_1_logic_1_1_i_infrastructural_logic.html#a75c889de16bd71e2cf4dbfc4cc342efb", null ],
    [ "ChangeStadiumName", "interface_football_database_1_1_logic_1_1_i_infrastructural_logic.html#ae3d998cd379f6f2151b99a6d095bce7f", null ],
    [ "ChangeStadiumPhone", "interface_football_database_1_1_logic_1_1_i_infrastructural_logic.html#a46594f3dcb20407656737ca3b1214942", null ],
    [ "InsertStadium", "interface_football_database_1_1_logic_1_1_i_infrastructural_logic.html#a0fef1907aa54c23cd5412d76c8690d83", null ],
    [ "MaximumCapacity", "interface_football_database_1_1_logic_1_1_i_infrastructural_logic.html#a67a5ddc460559b6aab54b11981cac42b", null ],
    [ "MaximumCapacityAsync", "interface_football_database_1_1_logic_1_1_i_infrastructural_logic.html#ac0717c2ff04b922cca17e4cbaff18ce5", null ],
    [ "RemoveStadium", "interface_football_database_1_1_logic_1_1_i_infrastructural_logic.html#aeba515396549a03e6002f9499150fa28", null ]
];