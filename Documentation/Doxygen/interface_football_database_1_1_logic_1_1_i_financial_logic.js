var interface_football_database_1_1_logic_1_1_i_financial_logic =
[
    [ "BelowLeagueAverage", "interface_football_database_1_1_logic_1_1_i_financial_logic.html#a4440ee01863b79df71be1250cb2dc546", null ],
    [ "BelowLeagueAverageAsync", "interface_football_database_1_1_logic_1_1_i_financial_logic.html#a9606d375ae3f696eec620f16ec8d7076", null ],
    [ "ChangeLeagueAverageValue", "interface_football_database_1_1_logic_1_1_i_financial_logic.html#a18e21b6b8eda4600601000810a49c16c", null ],
    [ "ChangePlayerMarketValue", "interface_football_database_1_1_logic_1_1_i_financial_logic.html#abd8f41b3b9d0bfdd74a3d6b369aa8302", null ],
    [ "FixLeagueAverageMarketValue", "interface_football_database_1_1_logic_1_1_i_financial_logic.html#ad4fa5a9a4fcbbfb175b36a16f13d9509", null ]
];