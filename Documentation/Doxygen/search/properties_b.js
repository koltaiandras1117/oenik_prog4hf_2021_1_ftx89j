var searchData=
[
  ['phone_504',['Phone',['../class_football_database_1_1_data_1_1_stadium.html#ae3dfce3f9a8cb532a17f83ee1714f231',1,'FootballDatabase::Data::Stadium']]],
  ['playerid_505',['PlayerId',['../class_football_database_1_1_logic_1_1_transfer_result.html#a259ee8bf9ad2a4aa8ab15eabd08db9a2',1,'FootballDatabase::Logic::TransferResult']]],
  ['playername_506',['PlayerName',['../class_football_database_1_1_logic_1_1_transfer_result.html#a54bba6448fb4a7232f9568d911e8d008',1,'FootballDatabase::Logic::TransferResult']]],
  ['players_507',['Players',['../class_football_database_1_1_data_1_1_football_context.html#a8ebbb652df9c2976a1cc22da12f3ad40',1,'FootballDatabase.Data.FootballContext.Players()'],['../class_football_database_1_1_data_1_1_team.html#a5b391cac3c66de37d1d6d1035e780fff',1,'FootballDatabase.Data.Team.Players()']]],
  ['position_508',['Position',['../class_football_database_1_1_data_1_1_player.html#a75d390d31da91d96c9fcf37a5645043d',1,'FootballDatabase.Data.Player.Position()'],['../class_football_database_1_1_web_1_1_models_1_1_player.html#aa360f24715d2154932f44812104282a8',1,'FootballDatabase.Web.Models.Player.Position()'],['../class_football_database_1_1_wpf_client_1_1_player_v_m.html#af15e677ae0ed87e2aa4e295663b936da',1,'FootballDatabase.WpfClient.PlayerVM.Position()']]]
];
