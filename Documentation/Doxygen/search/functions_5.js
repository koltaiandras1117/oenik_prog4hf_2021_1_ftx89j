var searchData=
[
  ['financiallogic_396',['FinancialLogic',['../class_football_database_1_1_logic_1_1_financial_logic.html#af9b41ee71394ecdb27d73c024be31364',1,'FootballDatabase::Logic::FinancialLogic']]],
  ['financiallogic_5fwhenbelowleagueaverageiscalledwithrandomvalues_5fitreturnseveryteambelowtheleaguesaveragevalue_397',['FinancialLogic_WhenBelowLeagueAverageIsCalledWithRandomValues_ItReturnsEveryTeamBelowTheLeaguesAverageValue',['../class_football_database_1_1_logic_1_1_tests_1_1_financial_logic_tests.html#aaaa98a598cb191007de283bb13726423',1,'FootballDatabase::Logic::Tests::FinancialLogicTests']]],
  ['financiallogic_5fwhenchangeleagueaveragevalueiscalled_5fitcallschangeaveragevalueonce_398',['FinancialLogic_WhenChangeLeagueAverageValueIsCalled_ItCallsChangeAverageValueOnce',['../class_football_database_1_1_logic_1_1_tests_1_1_financial_logic_tests.html#a99174c0d76b067992a8d6493cf201d0e',1,'FootballDatabase::Logic::Tests::FinancialLogicTests']]],
  ['fixleagueaveragemarketvalue_399',['FixLeagueAverageMarketValue',['../class_football_database_1_1_logic_1_1_financial_logic.html#a183c4d707a3d356d272056a5a9f2aac2',1,'FootballDatabase.Logic.FinancialLogic.FixLeagueAverageMarketValue()'],['../interface_football_database_1_1_logic_1_1_i_financial_logic.html#ad4fa5a9a4fcbbfb175b36a16f13d9509',1,'FootballDatabase.Logic.IFinancialLogic.FixLeagueAverageMarketValue()']]],
  ['footballcontext_400',['FootballContext',['../class_football_database_1_1_data_1_1_football_context.html#a55aa21fd673cee05a632f8f17feabef6',1,'FootballDatabase::Data::FootballContext']]]
];
