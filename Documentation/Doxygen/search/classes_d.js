var searchData=
[
  ['team_305',['Team',['../class_football_database_1_1_data_1_1_team.html',1,'FootballDatabase::Data']]],
  ['teamrepository_306',['TeamRepository',['../class_football_database_1_1_repository_1_1_team_repository.html',1,'FootballDatabase::Repository']]],
  ['transferlogic_307',['TransferLogic',['../class_football_database_1_1_logic_1_1_transfer_logic.html',1,'FootballDatabase::Logic']]],
  ['transferlogictests_308',['TransferLogicTests',['../class_football_database_1_1_logic_1_1_tests_1_1_transfer_logic_tests.html',1,'FootballDatabase::Logic::Tests']]],
  ['transferresult_309',['TransferResult',['../class_football_database_1_1_logic_1_1_transfer_result.html',1,'FootballDatabase::Logic']]]
];
