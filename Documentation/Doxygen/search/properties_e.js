var searchData=
[
  ['team_515',['Team',['../class_football_database_1_1_data_1_1_player.html#a646bd05641a0e6c8230be7d1618c1f4a',1,'FootballDatabase.Data.Player.Team()'],['../class_football_database_1_1_web_1_1_models_1_1_player.html#ada27aff9325539d65bd525ddbed9a2a9',1,'FootballDatabase.Web.Models.Player.Team()'],['../class_football_database_1_1_wpf_client_1_1_player_v_m.html#ab887ccab6cb6e0eef67d730d35c835c9',1,'FootballDatabase.WpfClient.PlayerVM.Team()']]],
  ['teamid_516',['TeamId',['../class_football_database_1_1_data_1_1_player.html#ad280f3cc506f97f155373d07c07d3ac9',1,'FootballDatabase.Data.Player.TeamId()'],['../class_football_database_1_1_logic_1_1_below_average.html#adb758db6d5fe9135b58efd854dd5ca87',1,'FootballDatabase.Logic.BelowAverage.TeamId()']]],
  ['teamname_517',['TeamName',['../class_football_database_1_1_logic_1_1_below_average.html#af3cb5568ec12dcaf815e1746a41e2e05',1,'FootballDatabase::Logic::BelowAverage']]],
  ['teams_518',['Teams',['../class_football_database_1_1_data_1_1_football_context.html#a2f822e61bfc3bec50b1b29bee82be2de',1,'FootballDatabase.Data.FootballContext.Teams()'],['../class_football_database_1_1_data_1_1_league.html#ab02d3809838c1ab2cd2503c5c5d7975d',1,'FootballDatabase.Data.League.Teams()'],['../class_football_database_1_1_data_1_1_stadium.html#acdb55f921d8ce866ac7577450c17865b',1,'FootballDatabase.Data.Stadium.Teams()']]],
  ['transferfee_519',['TransferFee',['../class_football_database_1_1_logic_1_1_transfer_result.html#aaaa58fcab0caeb945de84593fe50b464',1,'FootballDatabase::Logic::TransferResult']]]
];
