var searchData=
[
  ['dateofbirth_478',['DateOfBirth',['../class_football_database_1_1_data_1_1_player.html#a0442f9a8e012ab3b976c5916647bd569',1,'FootballDatabase.Data.Player.DateOfBirth()'],['../class_football_database_1_1_web_1_1_models_1_1_player.html#ac2008d83d9f6a9c75ff1243a1aced205',1,'FootballDatabase.Web.Models.Player.DateOfBirth()'],['../class_football_database_1_1_wpf_client_1_1_player_v_m.html#a0925e915d06759934ca054ae3b15d849',1,'FootballDatabase.WpfClient.PlayerVM.DateOfBirth()']]],
  ['delcmd_479',['DelCmd',['../class_football_database_1_1_u_i_1_1_v_m_1_1_main_view_model.html#a3e0fb8ef2d99fef745361cf961861ab4',1,'FootballDatabase.UI.VM.MainViewModel.DelCmd()'],['../class_football_database_1_1_wpf_client_1_1_main_v_m.html#adec4b9a5693bdf5b3ca8b9714dcc3a4a',1,'FootballDatabase.WpfClient.MainVM.DelCmd()']]],
  ['division_480',['Division',['../class_football_database_1_1_data_1_1_league.html#acc055a067105c9e4bb429f9786a919fe',1,'FootballDatabase.Data.League.Division()'],['../class_football_database_1_1_u_i_1_1_data_1_1_league_w_p_f.html#a7b720ca706c9bbe26035a5bc2021401f',1,'FootballDatabase.UI.Data.LeagueWPF.Division()']]]
];
