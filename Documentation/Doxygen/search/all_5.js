var searchData=
[
  ['bl_79',['BL',['../namespace_football_database_1_1_u_i_1_1_b_l.html',1,'FootballDatabase::UI']]],
  ['controllers_80',['Controllers',['../namespace_football_database_1_1_web_1_1_controllers.html',1,'FootballDatabase::Web']]],
  ['data_81',['Data',['../namespace_football_database_1_1_data.html',1,'FootballDatabase.Data'],['../namespace_football_database_1_1_u_i_1_1_data.html',1,'FootballDatabase.UI.Data']]],
  ['financiallogic_82',['FinancialLogic',['../class_football_database_1_1_logic_1_1_financial_logic.html',1,'FootballDatabase.Logic.FinancialLogic'],['../class_football_database_1_1_logic_1_1_financial_logic.html#af9b41ee71394ecdb27d73c024be31364',1,'FootballDatabase.Logic.FinancialLogic.FinancialLogic()']]],
  ['financiallogic_5fwhenbelowleagueaverageiscalledwithrandomvalues_5fitreturnseveryteambelowtheleaguesaveragevalue_83',['FinancialLogic_WhenBelowLeagueAverageIsCalledWithRandomValues_ItReturnsEveryTeamBelowTheLeaguesAverageValue',['../class_football_database_1_1_logic_1_1_tests_1_1_financial_logic_tests.html#aaaa98a598cb191007de283bb13726423',1,'FootballDatabase::Logic::Tests::FinancialLogicTests']]],
  ['financiallogic_5fwhenchangeleagueaveragevalueiscalled_5fitcallschangeaveragevalueonce_84',['FinancialLogic_WhenChangeLeagueAverageValueIsCalled_ItCallsChangeAverageValueOnce',['../class_football_database_1_1_logic_1_1_tests_1_1_financial_logic_tests.html#a99174c0d76b067992a8d6493cf201d0e',1,'FootballDatabase::Logic::Tests::FinancialLogicTests']]],
  ['financiallogictests_85',['FinancialLogicTests',['../class_football_database_1_1_logic_1_1_tests_1_1_financial_logic_tests.html',1,'FootballDatabase::Logic::Tests']]],
  ['fixleagueaveragemarketvalue_86',['FixLeagueAverageMarketValue',['../class_football_database_1_1_logic_1_1_financial_logic.html#a183c4d707a3d356d272056a5a9f2aac2',1,'FootballDatabase.Logic.FinancialLogic.FixLeagueAverageMarketValue()'],['../interface_football_database_1_1_logic_1_1_i_financial_logic.html#ad4fa5a9a4fcbbfb175b36a16f13d9509',1,'FootballDatabase.Logic.IFinancialLogic.FixLeagueAverageMarketValue()']]],
  ['footballcontext_87',['FootballContext',['../class_football_database_1_1_data_1_1_football_context.html',1,'FootballDatabase.Data.FootballContext'],['../class_football_database_1_1_data_1_1_football_context.html#a55aa21fd673cee05a632f8f17feabef6',1,'FootballDatabase.Data.FootballContext.FootballContext()']]],
  ['footballdatabase_88',['FootballDatabase',['../namespace_football_database.html',1,'']]],
  ['logic_89',['Logic',['../namespace_football_database_1_1_logic.html',1,'FootballDatabase']]],
  ['models_90',['Models',['../namespace_football_database_1_1_web_1_1_models.html',1,'FootballDatabase::Web']]],
  ['program_91',['Program',['../namespace_football_database_1_1_program.html',1,'FootballDatabase']]],
  ['repository_92',['Repository',['../namespace_football_database_1_1_repository.html',1,'FootballDatabase']]],
  ['tests_93',['Tests',['../namespace_football_database_1_1_logic_1_1_tests.html',1,'FootballDatabase::Logic']]],
  ['ui_94',['UI',['../namespace_football_database_1_1_u_i.html',1,'FootballDatabase.UI'],['../namespace_football_database_1_1_u_i_1_1_u_i.html',1,'FootballDatabase.UI.UI']]],
  ['vm_95',['VM',['../namespace_football_database_1_1_u_i_1_1_v_m.html',1,'FootballDatabase::UI']]],
  ['web_96',['Web',['../namespace_football_database_1_1_web.html',1,'FootballDatabase']]],
  ['wpfclient_97',['WpfClient',['../namespace_football_database_1_1_wpf_client.html',1,'FootballDatabase']]]
];
