var searchData=
[
  ['addcmd_469',['AddCmd',['../class_football_database_1_1_u_i_1_1_v_m_1_1_main_view_model.html#a2bef803d29bfc328b8e8e14a79415364',1,'FootballDatabase.UI.VM.MainViewModel.AddCmd()'],['../class_football_database_1_1_wpf_client_1_1_main_v_m.html#abf37bd60889f9d176a721745b36d0b60',1,'FootballDatabase.WpfClient.MainVM.AddCmd()']]],
  ['address_470',['Address',['../class_football_database_1_1_data_1_1_stadium.html#ac1232e571d199bd0e76b32c2e9e9e260',1,'FootballDatabase::Data::Stadium']]],
  ['allplayers_471',['AllPlayers',['../class_football_database_1_1_wpf_client_1_1_main_v_m.html#a3d9bd6f866709045ba8943d5830f15da',1,'FootballDatabase::WpfClient::MainVM']]],
  ['assists_472',['Assists',['../class_football_database_1_1_data_1_1_player.html#ace5563fea98becc363f511bf95b63b69',1,'FootballDatabase.Data.Player.Assists()'],['../class_football_database_1_1_web_1_1_models_1_1_player.html#a9af9d82d766e266f179360dce31da677',1,'FootballDatabase.Web.Models.Player.Assists()'],['../class_football_database_1_1_wpf_client_1_1_player_v_m.html#a33d4d797790acd89de223002232e566d',1,'FootballDatabase.WpfClient.PlayerVM.Assists()']]],
  ['averagevalue_473',['AverageValue',['../class_football_database_1_1_data_1_1_league.html#adce651fd2413b1f287d978519bed9835',1,'FootballDatabase.Data.League.AverageValue()'],['../class_football_database_1_1_u_i_1_1_data_1_1_league_w_p_f.html#a9b878d8c3cc50dd05362b3796e172d2c',1,'FootballDatabase.UI.Data.LeagueWPF.AverageValue()']]],
  ['awayshirt_474',['AwayShirt',['../class_football_database_1_1_data_1_1_team.html#a6106d9a72b7b3a5089416711d1057aa8',1,'FootballDatabase::Data::Team']]]
];
