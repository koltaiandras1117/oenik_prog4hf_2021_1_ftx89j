var searchData=
[
  ['season_510',['Season',['../class_football_database_1_1_data_1_1_league.html#a63971ed90b02e50d1a1d739f1663b81d',1,'FootballDatabase.Data.League.Season()'],['../class_football_database_1_1_u_i_1_1_data_1_1_league_w_p_f.html#a83cf34f5221d3ae04604296a7556a20a',1,'FootballDatabase.UI.Data.LeagueWPF.Season()']]],
  ['selectedplayer_511',['SelectedPlayer',['../class_football_database_1_1_wpf_client_1_1_main_v_m.html#ae26124c31251692ca3f1d7bed7409555',1,'FootballDatabase::WpfClient::MainVM']]],
  ['stadium_512',['Stadium',['../class_football_database_1_1_data_1_1_team.html#a5421a6fa939b7a43378a3d5344e21e17',1,'FootballDatabase::Data::Team']]],
  ['stadiumid_513',['StadiumId',['../class_football_database_1_1_data_1_1_team.html#a5108fbbfac302f8998f932ceab8d6aca',1,'FootballDatabase::Data::Team']]],
  ['stadiums_514',['Stadiums',['../class_football_database_1_1_data_1_1_football_context.html#a3d4eca5535fa1e6a2f461e7a43733c25',1,'FootballDatabase::Data::FootballContext']]]
];
