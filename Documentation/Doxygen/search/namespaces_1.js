var searchData=
[
  ['bl_322',['BL',['../namespace_football_database_1_1_u_i_1_1_b_l.html',1,'FootballDatabase::UI']]],
  ['controllers_323',['Controllers',['../namespace_football_database_1_1_web_1_1_controllers.html',1,'FootballDatabase::Web']]],
  ['data_324',['Data',['../namespace_football_database_1_1_data.html',1,'FootballDatabase.Data'],['../namespace_football_database_1_1_u_i_1_1_data.html',1,'FootballDatabase.UI.Data']]],
  ['footballdatabase_325',['FootballDatabase',['../namespace_football_database.html',1,'']]],
  ['logic_326',['Logic',['../namespace_football_database_1_1_logic.html',1,'FootballDatabase']]],
  ['models_327',['Models',['../namespace_football_database_1_1_web_1_1_models.html',1,'FootballDatabase::Web']]],
  ['program_328',['Program',['../namespace_football_database_1_1_program.html',1,'FootballDatabase']]],
  ['repository_329',['Repository',['../namespace_football_database_1_1_repository.html',1,'FootballDatabase']]],
  ['tests_330',['Tests',['../namespace_football_database_1_1_logic_1_1_tests.html',1,'FootballDatabase::Logic']]],
  ['ui_331',['UI',['../namespace_football_database_1_1_u_i.html',1,'FootballDatabase.UI'],['../namespace_football_database_1_1_u_i_1_1_u_i.html',1,'FootballDatabase.UI.UI']]],
  ['vm_332',['VM',['../namespace_football_database_1_1_u_i_1_1_v_m.html',1,'FootballDatabase::UI']]],
  ['web_333',['Web',['../namespace_football_database_1_1_web.html',1,'FootballDatabase']]],
  ['wpfclient_334',['WpfClient',['../namespace_football_database_1_1_wpf_client.html',1,'FootballDatabase']]]
];
