var class_football_database_1_1_repository_1_1_repository_class =
[
    [ "RepositoryClass", "class_football_database_1_1_repository_1_1_repository_class.html#a5ab0d8ae2d4ebc11bf6a72c41260b49d", null ],
    [ "GetAll", "class_football_database_1_1_repository_1_1_repository_class.html#a1bc1efbe589fcd849dd1dd0a91f0111f", null ],
    [ "GetCtx", "class_football_database_1_1_repository_1_1_repository_class.html#aedaa3ce52bc3570d98539ab7cd7709bb", null ],
    [ "GetOne", "class_football_database_1_1_repository_1_1_repository_class.html#a97c055d5936ed36bdea7ee28f5e6f0b1", null ],
    [ "Insert", "class_football_database_1_1_repository_1_1_repository_class.html#ae2bcdf9fe5a81f9762076a6801e5d3cd", null ],
    [ "Remove", "class_football_database_1_1_repository_1_1_repository_class.html#aee1f94ad66db31f3adbf9bf64105b17c", null ],
    [ "SetCtx", "class_football_database_1_1_repository_1_1_repository_class.html#ae361573557f8f8e88827a81a4f916449", null ]
];