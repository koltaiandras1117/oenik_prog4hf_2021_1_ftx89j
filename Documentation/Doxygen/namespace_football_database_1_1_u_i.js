var namespace_football_database_1_1_u_i =
[
    [ "BL", "namespace_football_database_1_1_u_i_1_1_b_l.html", "namespace_football_database_1_1_u_i_1_1_b_l" ],
    [ "Data", "namespace_football_database_1_1_u_i_1_1_data.html", "namespace_football_database_1_1_u_i_1_1_data" ],
    [ "UI", "namespace_football_database_1_1_u_i_1_1_u_i.html", "namespace_football_database_1_1_u_i_1_1_u_i" ],
    [ "VM", "namespace_football_database_1_1_u_i_1_1_v_m.html", "namespace_football_database_1_1_u_i_1_1_v_m" ],
    [ "App", "class_football_database_1_1_u_i_1_1_app.html", "class_football_database_1_1_u_i_1_1_app" ],
    [ "MainWindow", "class_football_database_1_1_u_i_1_1_main_window.html", "class_football_database_1_1_u_i_1_1_main_window" ],
    [ "MyIoC", "class_football_database_1_1_u_i_1_1_my_io_c.html", "class_football_database_1_1_u_i_1_1_my_io_c" ]
];