var interface_football_database_1_1_logic_1_1_i_transfer_logic =
[
    [ "ChangePlayerTeamId", "interface_football_database_1_1_logic_1_1_i_transfer_logic.html#aad1ed7af66a3bcb949bcbf4c4c3c3e2d", null ],
    [ "InsertPlayer", "interface_football_database_1_1_logic_1_1_i_transfer_logic.html#a65de55974184cad0def46aa2f5f93cda", null ],
    [ "RemovePlayer", "interface_football_database_1_1_logic_1_1_i_transfer_logic.html#a1005f2cd391cfd5963154768a0573e6e", null ],
    [ "TransferHighestValue", "interface_football_database_1_1_logic_1_1_i_transfer_logic.html#a405b56c290cbe78bf9eb19e8fc8f96fe", null ],
    [ "TransferHighestValueAsync", "interface_football_database_1_1_logic_1_1_i_transfer_logic.html#a2bc57f043a06509073d9f0392f2e49f5", null ]
];