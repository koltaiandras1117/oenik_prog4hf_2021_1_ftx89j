var class_football_database_1_1_logic_1_1_transfer_result =
[
    [ "Equals", "class_football_database_1_1_logic_1_1_transfer_result.html#a1295d5af5aacc56012d5b9b3e2b7da44", null ],
    [ "GetHashCode", "class_football_database_1_1_logic_1_1_transfer_result.html#ad4c4acf433af7d452c7f2769f13e4532", null ],
    [ "ToString", "class_football_database_1_1_logic_1_1_transfer_result.html#a69242be0ee206fec4436b7374eea980a", null ],
    [ "LeagueName", "class_football_database_1_1_logic_1_1_transfer_result.html#aac2eb107c2c335e4396fd0bca06d41af", null ],
    [ "NewTeamName", "class_football_database_1_1_logic_1_1_transfer_result.html#af6d8fea9b08162ada02b0500f416a140", null ],
    [ "PlayerId", "class_football_database_1_1_logic_1_1_transfer_result.html#a259ee8bf9ad2a4aa8ab15eabd08db9a2", null ],
    [ "PlayerName", "class_football_database_1_1_logic_1_1_transfer_result.html#a54bba6448fb4a7232f9568d911e8d008", null ],
    [ "TransferFee", "class_football_database_1_1_logic_1_1_transfer_result.html#aaaa58fcab0caeb945de84593fe50b464", null ]
];