var class_football_database_1_1_logic_1_1_league_maximum_capacity =
[
    [ "Equals", "class_football_database_1_1_logic_1_1_league_maximum_capacity.html#a857771195293054ed6572c8b30830b4c", null ],
    [ "GetHashCode", "class_football_database_1_1_logic_1_1_league_maximum_capacity.html#a0ca6f95e65978b7562fe78bd8c0af7c9", null ],
    [ "ToString", "class_football_database_1_1_logic_1_1_league_maximum_capacity.html#a32c88c955923f265d56f26de551fbcdf", null ],
    [ "LeagueId", "class_football_database_1_1_logic_1_1_league_maximum_capacity.html#a057eaa6be9032c90e74ceea680abe204", null ],
    [ "LeagueName", "class_football_database_1_1_logic_1_1_league_maximum_capacity.html#a50b147bdbea8a587fbea647d40d34bfc", null ],
    [ "MaximumCapacity", "class_football_database_1_1_logic_1_1_league_maximum_capacity.html#a520fec4ed330d42c1d916b8a21fee327", null ]
];