var class_football_database_1_1_web_1_1_models_1_1_player =
[
    [ "Assists", "class_football_database_1_1_web_1_1_models_1_1_player.html#a9af9d82d766e266f179360dce31da677", null ],
    [ "DateOfBirth", "class_football_database_1_1_web_1_1_models_1_1_player.html#ac2008d83d9f6a9c75ff1243a1aced205", null ],
    [ "Goals", "class_football_database_1_1_web_1_1_models_1_1_player.html#a2591e5f2aaee7eedfdae4148a8280acd", null ],
    [ "Id", "class_football_database_1_1_web_1_1_models_1_1_player.html#a70aa5b59f2246637184bd374c21a4609", null ],
    [ "MarketValue", "class_football_database_1_1_web_1_1_models_1_1_player.html#a4abef73f8fa9ff59ca201e491a3b0e57", null ],
    [ "Matches", "class_football_database_1_1_web_1_1_models_1_1_player.html#a9382ec640b30bb3f81c534ba13007490", null ],
    [ "Name", "class_football_database_1_1_web_1_1_models_1_1_player.html#a007da0112ff563e9f09b0a331a3bfc7c", null ],
    [ "Nationality", "class_football_database_1_1_web_1_1_models_1_1_player.html#a1f8a85f6b90b5cbf2f5cfb62bb97f602", null ],
    [ "Number", "class_football_database_1_1_web_1_1_models_1_1_player.html#ab15e50fd0ca7765f934cc6b00bca938b", null ],
    [ "Position", "class_football_database_1_1_web_1_1_models_1_1_player.html#aa360f24715d2154932f44812104282a8", null ],
    [ "Team", "class_football_database_1_1_web_1_1_models_1_1_player.html#ada27aff9325539d65bd525ddbed9a2a9", null ]
];