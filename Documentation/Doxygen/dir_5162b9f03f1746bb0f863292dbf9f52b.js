var dir_5162b9f03f1746bb0f863292dbf9f52b =
[
    [ "FootballDatabase.Data", "dir_ead0552b5d446ad1f5d1d351d7393032.html", "dir_ead0552b5d446ad1f5d1d351d7393032" ],
    [ "FootballDatabase.Logic", "dir_99e7f56c588761424b514d173ce62ee6.html", "dir_99e7f56c588761424b514d173ce62ee6" ],
    [ "FootballDatabase.Logic.Tests", "dir_60b0d9ccd0607a23650060f74276f8e2.html", "dir_60b0d9ccd0607a23650060f74276f8e2" ],
    [ "FootballDatabase.Program", "dir_3550c4a8bdf704e940ba3119261dfa07.html", "dir_3550c4a8bdf704e940ba3119261dfa07" ],
    [ "FootballDatabase.Repository", "dir_c8d112439a6e4224eba94d2233d82ba8.html", "dir_c8d112439a6e4224eba94d2233d82ba8" ],
    [ "FootballDatabase.UI", "dir_2712b62b7ab1fa7fdd20b35db8d1faf1.html", "dir_2712b62b7ab1fa7fdd20b35db8d1faf1" ],
    [ "FootballDatabase.Web", "dir_9bb4905fffd000271e7138af03f0be3c.html", "dir_9bb4905fffd000271e7138af03f0be3c" ],
    [ "FootballDatabase.WpfClient", "dir_2ba0d9824193de610f310e9c7b38aa3c.html", "dir_2ba0d9824193de610f310e9c7b38aa3c" ]
];