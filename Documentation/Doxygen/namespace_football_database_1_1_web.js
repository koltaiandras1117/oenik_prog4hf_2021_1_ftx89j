var namespace_football_database_1_1_web =
[
    [ "Controllers", "namespace_football_database_1_1_web_1_1_controllers.html", "namespace_football_database_1_1_web_1_1_controllers" ],
    [ "Models", "namespace_football_database_1_1_web_1_1_models.html", "namespace_football_database_1_1_web_1_1_models" ],
    [ "ApiResult", "class_football_database_1_1_web_1_1_api_result.html", "class_football_database_1_1_web_1_1_api_result" ],
    [ "Program", "class_football_database_1_1_web_1_1_program.html", "class_football_database_1_1_web_1_1_program" ],
    [ "Startup", "class_football_database_1_1_web_1_1_startup.html", "class_football_database_1_1_web_1_1_startup" ]
];