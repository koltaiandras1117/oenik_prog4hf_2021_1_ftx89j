var class_football_database_1_1_logic_1_1_financial_logic =
[
    [ "FinancialLogic", "class_football_database_1_1_logic_1_1_financial_logic.html#af9b41ee71394ecdb27d73c024be31364", null ],
    [ "BelowLeagueAverage", "class_football_database_1_1_logic_1_1_financial_logic.html#a06806e877c17ccb6dca799897617a0e3", null ],
    [ "BelowLeagueAverageAsync", "class_football_database_1_1_logic_1_1_financial_logic.html#a7a57b6185c137fb07cf94e6eecb85e73", null ],
    [ "ChangeLeagueAverageValue", "class_football_database_1_1_logic_1_1_financial_logic.html#a47e2e157df62f2900abd94fb662dd561", null ],
    [ "ChangePlayerMarketValue", "class_football_database_1_1_logic_1_1_financial_logic.html#a9fa5648db903042ac67a15d990c304f1", null ],
    [ "FixLeagueAverageMarketValue", "class_football_database_1_1_logic_1_1_financial_logic.html#a183c4d707a3d356d272056a5a9f2aac2", null ]
];