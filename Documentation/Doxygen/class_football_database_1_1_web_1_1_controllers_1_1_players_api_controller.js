var class_football_database_1_1_web_1_1_controllers_1_1_players_api_controller =
[
    [ "PlayersApiController", "class_football_database_1_1_web_1_1_controllers_1_1_players_api_controller.html#a1b45e3874eb2ea44fe8fff87920881d8", null ],
    [ "AddOnePlayer", "class_football_database_1_1_web_1_1_controllers_1_1_players_api_controller.html#ace842688d2a0dadbc180a59d07ce947a", null ],
    [ "DeleteOnePlayer", "class_football_database_1_1_web_1_1_controllers_1_1_players_api_controller.html#aed7648f8ef190d0642bac32b959edc73", null ],
    [ "GetAll", "class_football_database_1_1_web_1_1_controllers_1_1_players_api_controller.html#a8319eaf130126e75351706264a391ab7", null ],
    [ "ModifyOnePlayer", "class_football_database_1_1_web_1_1_controllers_1_1_players_api_controller.html#af8b181bf86e92ba7d6c8934e85cb8b06", null ]
];