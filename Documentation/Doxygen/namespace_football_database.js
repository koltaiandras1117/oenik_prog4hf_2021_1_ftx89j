var namespace_football_database =
[
    [ "Data", "namespace_football_database_1_1_data.html", "namespace_football_database_1_1_data" ],
    [ "Logic", "namespace_football_database_1_1_logic.html", "namespace_football_database_1_1_logic" ],
    [ "Program", "namespace_football_database_1_1_program.html", "namespace_football_database_1_1_program" ],
    [ "Repository", "namespace_football_database_1_1_repository.html", "namespace_football_database_1_1_repository" ],
    [ "UI", "namespace_football_database_1_1_u_i.html", "namespace_football_database_1_1_u_i" ],
    [ "Web", "namespace_football_database_1_1_web.html", "namespace_football_database_1_1_web" ],
    [ "WpfClient", "namespace_football_database_1_1_wpf_client.html", "namespace_football_database_1_1_wpf_client" ]
];