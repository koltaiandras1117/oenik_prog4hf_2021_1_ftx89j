var hierarchy =
[
    [ "FootballDatabase.Web.ApiResult", "class_football_database_1_1_web_1_1_api_result.html", null ],
    [ "Application", null, [
      [ "FootballDatabase.UI.App", "class_football_database_1_1_u_i_1_1_app.html", null ],
      [ "FootballDatabase.WpfClient.App", "class_football_database_1_1_wpf_client_1_1_app.html", null ]
    ] ],
    [ "FootballDatabase.Logic.BelowAverage", "class_football_database_1_1_logic_1_1_below_average.html", null ],
    [ "Controller", null, [
      [ "FootballDatabase.Web.Controllers.HomeController", "class_football_database_1_1_web_1_1_controllers_1_1_home_controller.html", null ],
      [ "FootballDatabase.Web.Controllers.PlayersApiController", "class_football_database_1_1_web_1_1_controllers_1_1_players_api_controller.html", null ],
      [ "FootballDatabase.Web.Controllers.PlayersController", "class_football_database_1_1_web_1_1_controllers_1_1_players_controller.html", null ]
    ] ],
    [ "DbContext", null, [
      [ "FootballDatabase.Data.FootballContext", "class_football_database_1_1_data_1_1_football_context.html", null ]
    ] ],
    [ "FootballDatabase.Web.Models.ErrorViewModel", "class_football_database_1_1_web_1_1_models_1_1_error_view_model.html", null ],
    [ "Exception", null, [
      [ "FootballDatabase.Logic.NoTeamException", "class_football_database_1_1_logic_1_1_no_team_exception.html", null ]
    ] ],
    [ "FootballDatabase.Logic.Tests.FinancialLogicTests", "class_football_database_1_1_logic_1_1_tests_1_1_financial_logic_tests.html", null ],
    [ "IComponentConnector", null, [
      [ "FootballDatabase.UI.UI.EditorWindow", "class_football_database_1_1_u_i_1_1_u_i_1_1_editor_window.html", null ]
    ] ],
    [ "FootballDatabase.UI.BL.IEditorService", "interface_football_database_1_1_u_i_1_1_b_l_1_1_i_editor_service.html", [
      [ "FootballDatabase.UI.UI.EditorServiceViaWindow", "class_football_database_1_1_u_i_1_1_u_i_1_1_editor_service_via_window.html", null ]
    ] ],
    [ "FootballDatabase.Logic.IFinancialLogic", "interface_football_database_1_1_logic_1_1_i_financial_logic.html", [
      [ "FootballDatabase.Logic.FinancialLogic", "class_football_database_1_1_logic_1_1_financial_logic.html", null ]
    ] ],
    [ "FootballDatabase.Logic.IInfrastructuralLogic", "interface_football_database_1_1_logic_1_1_i_infrastructural_logic.html", [
      [ "FootballDatabase.Logic.InfrastructuralLogic", "class_football_database_1_1_logic_1_1_infrastructural_logic.html", null ]
    ] ],
    [ "FootballDatabase.UI.BL.ILeagueWPFLogic", "interface_football_database_1_1_u_i_1_1_b_l_1_1_i_league_w_p_f_logic.html", [
      [ "FootballDatabase.UI.BL.LeagueWPFLogic", "class_football_database_1_1_u_i_1_1_b_l_1_1_league_w_p_f_logic.html", null ]
    ] ],
    [ "FootballDatabase.Logic.Tests.InfrastructuralLogicTests", "class_football_database_1_1_logic_1_1_tests_1_1_infrastructural_logic_tests.html", null ],
    [ "InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "FootballDatabase.Repository.IRepository< T >", "interface_football_database_1_1_repository_1_1_i_repository.html", [
      [ "FootballDatabase.Repository.RepositoryClass< T >", "class_football_database_1_1_repository_1_1_repository_class.html", null ]
    ] ],
    [ "FootballDatabase.Repository.IRepository< League >", "interface_football_database_1_1_repository_1_1_i_repository.html", [
      [ "FootballDatabase.Repository.ILeagueRepository", "interface_football_database_1_1_repository_1_1_i_league_repository.html", [
        [ "FootballDatabase.Repository.LeagueRepository", "class_football_database_1_1_repository_1_1_league_repository.html", null ]
      ] ]
    ] ],
    [ "FootballDatabase.Repository.IRepository< Player >", "interface_football_database_1_1_repository_1_1_i_repository.html", [
      [ "FootballDatabase.Repository.IPlayerRepository", "interface_football_database_1_1_repository_1_1_i_player_repository.html", [
        [ "FootballDatabase.Repository.PlayerRepository", "class_football_database_1_1_repository_1_1_player_repository.html", null ]
      ] ]
    ] ],
    [ "FootballDatabase.Repository.IRepository< Stadium >", "interface_football_database_1_1_repository_1_1_i_repository.html", [
      [ "FootballDatabase.Repository.IStadiumRepository", "interface_football_database_1_1_repository_1_1_i_stadium_repository.html", [
        [ "FootballDatabase.Repository.StadiumRepository", "class_football_database_1_1_repository_1_1_stadium_repository.html", null ]
      ] ]
    ] ],
    [ "FootballDatabase.Repository.IRepository< Team >", "interface_football_database_1_1_repository_1_1_i_repository.html", [
      [ "FootballDatabase.Repository.ITeamRepository", "interface_football_database_1_1_repository_1_1_i_team_repository.html", [
        [ "FootballDatabase.Repository.TeamRepository", "class_football_database_1_1_repository_1_1_team_repository.html", null ]
      ] ]
    ] ],
    [ "IServiceLocator", null, [
      [ "FootballDatabase.UI.MyIoC", "class_football_database_1_1_u_i_1_1_my_io_c.html", null ]
    ] ],
    [ "FootballDatabase.Logic.IStatisticalLogic", "interface_football_database_1_1_logic_1_1_i_statistical_logic.html", [
      [ "FootballDatabase.Logic.StatisticalLogic", "class_football_database_1_1_logic_1_1_statistical_logic.html", null ]
    ] ],
    [ "FootballDatabase.Logic.ITransferLogic", "interface_football_database_1_1_logic_1_1_i_transfer_logic.html", [
      [ "FootballDatabase.Logic.TransferLogic", "class_football_database_1_1_logic_1_1_transfer_logic.html", null ]
    ] ],
    [ "FootballDatabase.Data.League", "class_football_database_1_1_data_1_1_league.html", null ],
    [ "FootballDatabase.Logic.LeagueMaximumCapacity", "class_football_database_1_1_logic_1_1_league_maximum_capacity.html", null ],
    [ "FootballDatabase.WpfClient.MainLogic", "class_football_database_1_1_wpf_client_1_1_main_logic.html", null ],
    [ "FootballDatabase.Web.Models.MapperFactory", "class_football_database_1_1_web_1_1_models_1_1_mapper_factory.html", null ],
    [ "ObservableObject", null, [
      [ "FootballDatabase.UI.Data.LeagueWPF", "class_football_database_1_1_u_i_1_1_data_1_1_league_w_p_f.html", null ],
      [ "FootballDatabase.WpfClient.PlayerVM", "class_football_database_1_1_wpf_client_1_1_player_v_m.html", null ]
    ] ],
    [ "FootballDatabase.Data.Player", "class_football_database_1_1_data_1_1_player.html", null ],
    [ "FootballDatabase.Web.Models.Player", "class_football_database_1_1_web_1_1_models_1_1_player.html", null ],
    [ "FootballDatabase.Web.Models.PlayerListViewModel", "class_football_database_1_1_web_1_1_models_1_1_player_list_view_model.html", null ],
    [ "FootballDatabase.Web.Program", "class_football_database_1_1_web_1_1_program.html", null ],
    [ "FootballDatabase.Program.Program", "class_football_database_1_1_program_1_1_program.html", null ],
    [ "RazorPage", null, [
      [ "AspNetCore.Views__ViewImports", "class_asp_net_core_1_1_views_____view_imports.html", null ],
      [ "AspNetCore.Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", null ],
      [ "AspNetCore.Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", null ],
      [ "AspNetCore.Views_Home_Privacy", "class_asp_net_core_1_1_views___home___privacy.html", null ],
      [ "AspNetCore.Views_Players_PlayersDetails", "class_asp_net_core_1_1_views___players___players_details.html", null ],
      [ "AspNetCore.Views_Players_PlayersEdit", "class_asp_net_core_1_1_views___players___players_edit.html", null ],
      [ "AspNetCore.Views_Players_PlayersIndex", "class_asp_net_core_1_1_views___players___players_index.html", null ],
      [ "AspNetCore.Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", null ],
      [ "AspNetCore.Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", null ],
      [ "AspNetCore.Views_Shared_Error", "class_asp_net_core_1_1_views___shared___error.html", null ]
    ] ],
    [ "RazorPage< IEnumerable< FootballDatabase.Web.Models.Player >>", null, [
      [ "AspNetCore.Views_Players_PlayersList", "class_asp_net_core_1_1_views___players___players_list.html", null ]
    ] ],
    [ "FootballDatabase.Repository.RepositoryClass< League >", "class_football_database_1_1_repository_1_1_repository_class.html", [
      [ "FootballDatabase.Repository.LeagueRepository", "class_football_database_1_1_repository_1_1_league_repository.html", null ]
    ] ],
    [ "FootballDatabase.Repository.RepositoryClass< Player >", "class_football_database_1_1_repository_1_1_repository_class.html", [
      [ "FootballDatabase.Repository.PlayerRepository", "class_football_database_1_1_repository_1_1_player_repository.html", null ]
    ] ],
    [ "FootballDatabase.Repository.RepositoryClass< Stadium >", "class_football_database_1_1_repository_1_1_repository_class.html", [
      [ "FootballDatabase.Repository.StadiumRepository", "class_football_database_1_1_repository_1_1_stadium_repository.html", null ]
    ] ],
    [ "FootballDatabase.Repository.RepositoryClass< Team >", "class_football_database_1_1_repository_1_1_repository_class.html", [
      [ "FootballDatabase.Repository.TeamRepository", "class_football_database_1_1_repository_1_1_team_repository.html", null ]
    ] ],
    [ "SimpleIoc", null, [
      [ "FootballDatabase.UI.MyIoC", "class_football_database_1_1_u_i_1_1_my_io_c.html", null ]
    ] ],
    [ "FootballDatabase.Data.Stadium", "class_football_database_1_1_data_1_1_stadium.html", null ],
    [ "FootballDatabase.Web.Startup", "class_football_database_1_1_web_1_1_startup.html", null ],
    [ "FootballDatabase.Logic.Tests.StatisticalLogicTests", "class_football_database_1_1_logic_1_1_tests_1_1_statistical_logic_tests.html", null ],
    [ "FootballDatabase.Data.Team", "class_football_database_1_1_data_1_1_team.html", null ],
    [ "FootballDatabase.Logic.Tests.TransferLogicTests", "class_football_database_1_1_logic_1_1_tests_1_1_transfer_logic_tests.html", null ],
    [ "FootballDatabase.Logic.TransferResult", "class_football_database_1_1_logic_1_1_transfer_result.html", null ],
    [ "ViewModelBase", null, [
      [ "FootballDatabase.UI.VM.EditorViewModel", "class_football_database_1_1_u_i_1_1_v_m_1_1_editor_view_model.html", null ],
      [ "FootballDatabase.UI.VM.MainViewModel", "class_football_database_1_1_u_i_1_1_v_m_1_1_main_view_model.html", null ],
      [ "FootballDatabase.WpfClient.MainVM", "class_football_database_1_1_wpf_client_1_1_main_v_m.html", null ]
    ] ],
    [ "Window", null, [
      [ "FootballDatabase.UI.MainWindow", "class_football_database_1_1_u_i_1_1_main_window.html", null ],
      [ "FootballDatabase.UI.UI.EditorWindow", "class_football_database_1_1_u_i_1_1_u_i_1_1_editor_window.html", null ],
      [ "FootballDatabase.WpfClient.EditorWindow", "class_football_database_1_1_wpf_client_1_1_editor_window.html", null ],
      [ "FootballDatabase.WpfClient.MainWindow", "class_football_database_1_1_wpf_client_1_1_main_window.html", null ]
    ] ]
];