var namespace_football_database_1_1_wpf_client =
[
    [ "App", "class_football_database_1_1_wpf_client_1_1_app.html", "class_football_database_1_1_wpf_client_1_1_app" ],
    [ "EditorWindow", "class_football_database_1_1_wpf_client_1_1_editor_window.html", "class_football_database_1_1_wpf_client_1_1_editor_window" ],
    [ "MainLogic", "class_football_database_1_1_wpf_client_1_1_main_logic.html", "class_football_database_1_1_wpf_client_1_1_main_logic" ],
    [ "MainVM", "class_football_database_1_1_wpf_client_1_1_main_v_m.html", "class_football_database_1_1_wpf_client_1_1_main_v_m" ],
    [ "MainWindow", "class_football_database_1_1_wpf_client_1_1_main_window.html", "class_football_database_1_1_wpf_client_1_1_main_window" ],
    [ "PlayerVM", "class_football_database_1_1_wpf_client_1_1_player_v_m.html", "class_football_database_1_1_wpf_client_1_1_player_v_m" ]
];