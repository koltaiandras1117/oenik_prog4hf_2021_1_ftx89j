var class_football_database_1_1_data_1_1_stadium =
[
    [ "Stadium", "class_football_database_1_1_data_1_1_stadium.html#a86f64266eb1484c4384a72d158c5d2d4", null ],
    [ "Equals", "class_football_database_1_1_data_1_1_stadium.html#a537c7919306063ac80ba0078fcb3f76a", null ],
    [ "GetHashCode", "class_football_database_1_1_data_1_1_stadium.html#a78b4ef582a91792b23c370a917c2f6ac", null ],
    [ "SetTeams", "class_football_database_1_1_data_1_1_stadium.html#ab064a7dc505030363283864b6ccfa10e", null ],
    [ "ToString", "class_football_database_1_1_data_1_1_stadium.html#ab6504415643ffed94f95784188aee594", null ],
    [ "Address", "class_football_database_1_1_data_1_1_stadium.html#ac1232e571d199bd0e76b32c2e9e9e260", null ],
    [ "Capacity", "class_football_database_1_1_data_1_1_stadium.html#acb80a99ed3882e03b36312d3ff1a84c7", null ],
    [ "Id", "class_football_database_1_1_data_1_1_stadium.html#a0d15137a3750145be3f10c75d3497619", null ],
    [ "Name", "class_football_database_1_1_data_1_1_stadium.html#acf721974cecbcf0847d4f9b76af4e966", null ],
    [ "Phone", "class_football_database_1_1_data_1_1_stadium.html#ae3dfce3f9a8cb532a17f83ee1714f231", null ],
    [ "Teams", "class_football_database_1_1_data_1_1_stadium.html#acdb55f921d8ce866ac7577450c17865b", null ],
    [ "YearOfBuilt", "class_football_database_1_1_data_1_1_stadium.html#a4434aa1ea27356cd5be849075cf64e17", null ]
];