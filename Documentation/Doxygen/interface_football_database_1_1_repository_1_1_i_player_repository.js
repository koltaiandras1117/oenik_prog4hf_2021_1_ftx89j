var interface_football_database_1_1_repository_1_1_i_player_repository =
[
    [ "ChangeAssists", "interface_football_database_1_1_repository_1_1_i_player_repository.html#a6f58f370a582da372180a4f267f94281", null ],
    [ "ChangeBirthday", "interface_football_database_1_1_repository_1_1_i_player_repository.html#abd8fd8a4a2ddd02d33649c9d381161bb", null ],
    [ "ChangeGoals", "interface_football_database_1_1_repository_1_1_i_player_repository.html#a75713a0216996eaaf8c40e24d77a361c", null ],
    [ "ChangeMarketValue", "interface_football_database_1_1_repository_1_1_i_player_repository.html#a17cf7d6ed7fdf4a86a496b30f96e1650", null ],
    [ "ChangeMatches", "interface_football_database_1_1_repository_1_1_i_player_repository.html#afcb197f3a251f6a26ff38b1522db75ad", null ],
    [ "ChangeName", "interface_football_database_1_1_repository_1_1_i_player_repository.html#a0476775fb4bd59d6d848bb76f86345df", null ],
    [ "ChangeNationality", "interface_football_database_1_1_repository_1_1_i_player_repository.html#aecbbd5901b8370c71c0133b8ee52c103", null ],
    [ "ChangeNumber", "interface_football_database_1_1_repository_1_1_i_player_repository.html#a3768535ca9c2284bee1a534f4dddbe6b", null ],
    [ "ChangePosition", "interface_football_database_1_1_repository_1_1_i_player_repository.html#a7e58d9b73bd9e089553031b6f043f843", null ],
    [ "ChangeTeamId", "interface_football_database_1_1_repository_1_1_i_player_repository.html#adb17b2e0033669dbb79eb46eed79b3da", null ]
];