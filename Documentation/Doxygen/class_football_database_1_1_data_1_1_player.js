var class_football_database_1_1_data_1_1_player =
[
    [ "Equals", "class_football_database_1_1_data_1_1_player.html#ac7d3db67720277da198138fa7e663d50", null ],
    [ "GetHashCode", "class_football_database_1_1_data_1_1_player.html#a9be7c5f664d595f85df257bf89a0d342", null ],
    [ "ToString", "class_football_database_1_1_data_1_1_player.html#a9dbbba936565af970427e768baf8e667", null ],
    [ "Assists", "class_football_database_1_1_data_1_1_player.html#ace5563fea98becc363f511bf95b63b69", null ],
    [ "DateOfBirth", "class_football_database_1_1_data_1_1_player.html#a0442f9a8e012ab3b976c5916647bd569", null ],
    [ "Goals", "class_football_database_1_1_data_1_1_player.html#a6a36ce2409feea3b977ec23201148b82", null ],
    [ "Id", "class_football_database_1_1_data_1_1_player.html#adfa296d79ffe084c06516ef7129b803e", null ],
    [ "MarketValue", "class_football_database_1_1_data_1_1_player.html#ac8e5ed3813b2b17d39d0ccf0d470ddec", null ],
    [ "Matches", "class_football_database_1_1_data_1_1_player.html#a645ccd3bc2423dd12b1820298aa5c078", null ],
    [ "Name", "class_football_database_1_1_data_1_1_player.html#aef9b32f2a9715e1067622536de4405c4", null ],
    [ "Nationality", "class_football_database_1_1_data_1_1_player.html#a89244f405881d2edf21e7f710f033ad8", null ],
    [ "Number", "class_football_database_1_1_data_1_1_player.html#a2626f34a3d9acd2fa975112999d0d465", null ],
    [ "Position", "class_football_database_1_1_data_1_1_player.html#a75d390d31da91d96c9fcf37a5645043d", null ],
    [ "Team", "class_football_database_1_1_data_1_1_player.html#a646bd05641a0e6c8230be7d1618c1f4a", null ],
    [ "TeamId", "class_football_database_1_1_data_1_1_player.html#ad280f3cc506f97f155373d07c07d3ac9", null ]
];