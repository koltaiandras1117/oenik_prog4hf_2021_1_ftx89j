var class_football_database_1_1_logic_1_1_transfer_logic =
[
    [ "TransferLogic", "class_football_database_1_1_logic_1_1_transfer_logic.html#a69d809fdd9e5f84745a98d3c10356be4", null ],
    [ "ChangePlayerTeamId", "class_football_database_1_1_logic_1_1_transfer_logic.html#ae4372f229063085983ca770e177e6935", null ],
    [ "InsertPlayer", "class_football_database_1_1_logic_1_1_transfer_logic.html#a796f47096e4bd7f903bd9e383012c2c7", null ],
    [ "RemovePlayer", "class_football_database_1_1_logic_1_1_transfer_logic.html#aeca46f27802de171b3903f496730df5e", null ],
    [ "TransferHighestValue", "class_football_database_1_1_logic_1_1_transfer_logic.html#a2781e082bada8bd9c5a0f90dbffbc418", null ],
    [ "TransferHighestValueAsync", "class_football_database_1_1_logic_1_1_transfer_logic.html#a6c2c01091a39aec88fb98a6e6180f0f9", null ]
];