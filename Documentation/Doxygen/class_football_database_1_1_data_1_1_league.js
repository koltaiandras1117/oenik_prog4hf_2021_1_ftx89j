var class_football_database_1_1_data_1_1_league =
[
    [ "League", "class_football_database_1_1_data_1_1_league.html#ac595e85374266695271a70f3b390191e", null ],
    [ "Equals", "class_football_database_1_1_data_1_1_league.html#a5526321c7b68d069726e35bd9ea6f6fa", null ],
    [ "GetHashCode", "class_football_database_1_1_data_1_1_league.html#afe54c8da2eec32865865a8d818eadd76", null ],
    [ "ToString", "class_football_database_1_1_data_1_1_league.html#a12a1b4c76287032e60efd3de5dbb2e51", null ],
    [ "AverageValue", "class_football_database_1_1_data_1_1_league.html#adce651fd2413b1f287d978519bed9835", null ],
    [ "Country", "class_football_database_1_1_data_1_1_league.html#a818e88387e0de17c023008978ebef33c", null ],
    [ "Division", "class_football_database_1_1_data_1_1_league.html#acc055a067105c9e4bb429f9786a919fe", null ],
    [ "Id", "class_football_database_1_1_data_1_1_league.html#aaf6a57968daff7a6e36979c633e9e6c9", null ],
    [ "Name", "class_football_database_1_1_data_1_1_league.html#a7a8be643b3de4fd5135ac065dc04b489", null ],
    [ "Season", "class_football_database_1_1_data_1_1_league.html#a63971ed90b02e50d1a1d739f1663b81d", null ],
    [ "Teams", "class_football_database_1_1_data_1_1_league.html#ab02d3809838c1ab2cd2503c5c5d7975d", null ]
];